# Deploy Instructions on Server Bank

1. Add the root path for js files in application.js using the variable urls

    ```
    const urls ={
      root:'/santander_budget'
    }
    ```

2. Run the command to precompile assets in production environment

    ```(bash)
    RAILS_RELATIVE_URL_ROOT=/santander_budget RAILS_ENV=production rake assets:precompile
    ```

3. Run the rake task called non_digested

    ```(bash)
    $ RAILS_ENV=production rake nondigest:precompile
    ```


4. Modify the layout to use all the assets with sub_path santander, change the original (development environment) to subpath domain (production environment)
 
```
Original
= stylesheet_link_tag    "application" ,media: 'all'
= javascript_include_tag "application"

Must be
= stylesheet_link_tag    "/santander_budget/assets/application" ,media: 'all'
= javascript_include_tag "/santander_budget/assets/application"
```


5. The file must "config/environments/production.rb" to have the following settings in order to allow to run the app in production mode.

  1. config.assets.compile = false
  2. config.assets.digest = false
  3. config.serve_static_files = true

6. Make sure the "config.ru" file in root folder look like this:

    ```
    # This file is used by Rack-based servers to start the application.
    require ::File.expand_path('../config/environment',  __FILE__)
    
    
    puts "Production Mode : " + (Rails.env.production?).to_s
    
    if Rails.env.production?
    
      Rails.application.config.relative_url_root = '/santander_budget'
    
      map Rails.application.config.relative_url_root || "/santander_budget" do
        run Rails.application
      end
    
    else
    
      Rails.application.config.relative_url_root = '/'
    
      map Rails.application.config.relative_url_root || "/" do
        run Rails.application
      end
    
      require ::File.expand_path('../config/environment',  __FILE__)
      run Rails.application
    end
    
    ```


7. You can test the app running the app locally in production mode.

    ```
    $ rails s -e production
    http://localhost:3000/santander_budget
    ```

Note : In order to reset everything, just delete the folders public/santander_budget and public/assets.

# Deploy Instructions on Heroku

1. All the configuration have to be in default "config/environments/production.rb":

  - config.assets.compile = true
  - config.assets.digest = true
  - config.serve_static_files = false
  
2. Heroku doesn't use the relative directory to deploy, so config.ru should look like this:

```
    # This file is used by Rack-based servers to start the application.
    require ::File.expand_path('../config/environment',  __FILE__)
    
    puts "Production Mode : " + (Rails.env.production?).to_s
    
    if Rails.env.production?
    
        Rails.application.config.relative_url_root = '/'
    
        map Rails.application.config.relative_url_root || "/" do
            run Rails.application
        end
    
    else
    
        Rails.application.config.relative_url_root = '/'
    
        map Rails.application.config.relative_url_root || "/" do
            run Rails.application
        end
    
        require ::File.expand_path('../config/environment',  __FILE__)
        run Rails.application
    end
```