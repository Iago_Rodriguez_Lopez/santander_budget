      create or replace view annual_planification_items_detail
      as
      select
      r.annual_planification_id,
      r.year,
      r.pivot_month,
      r.annual_planification_name,
      r.subfield_name,
      r.item_code,
      t.item_description,
      t.q1,
      t.q2,
      t.q3,
      t.q4,
      b.january_budget,
      b.february_budget,
      b.march_budget,
      b.april_budget,
      b.may_budget,
      b.june_budget,
      b.july_budget,
      b.august_budget,
      b.september_budget,
      b.october_budget,
      b.november_budget,
      b.december_budget,
      b.year_budget,
      sum(r.amount + r.january_transactions) as "january_transactions_budgeted",
      sum(r.amount + r.february_transactions) as "february_transactions_budgeted",
      sum(r.amount + r.march_transactions) as "march_transactions_budgeted",
      sum(r.amount + r.april_transactions) as "april_transactions_budgeted",
      sum(r.amount + r.may_transactions) as "may_transactions_budgeted",
      sum(r.amount + r.june_transactions) as "june_transactions_budgeted",
      sum(r.amount + r.july_transactions) as "july_transactions_budgeted",
      sum(r.amount + r.august_transactions) as "august_transactions_budgeted",
      sum(r.amount + r.september_transactions) as "september_transactions_budgeted",
      sum(r.amount + r.october_transactions) as "october_transactions_budgeted",
      sum(r.amount + r.november_transactions) as "november_transactions_budgeted",
      sum(r.amount + r.december_transactions) as "december_transactions_budgeted"

      from annual_adjustment_details_report r

      join transactions_report t
      on t.year = r.year AND
      t.subfield_id = r.subfield_id
      AND t.item_code = r.item_code

      join budgets_report b on
      b.year = r.year AND
      b.subfield_id = r.subfield_id AND
      b.item_id = r.item_id

      group by
      r.annual_planification_id,
      r.year,
      r.pivot_month,
      r.annual_planification_name,
      r.subfield_name,
      r.item_code,
      t.item_description,
      t.q1,
      t.q2,
      t.q3,
      t.q4,
      b.january_budget,
      b.february_budget,
      b.march_budget,
      b.april_budget,
      b.may_budget,
      b.june_budget,
      b.july_budget,
      b.august_budget,
      b.september_budget,
      b.october_budget,
      b.november_budget,
      b.december_budget,
      b.year_budget
      order by
      r.annual_planification_id,
      r.year,
      r.pivot_month,
      r.annual_planification_name,
      r.subfield_name,
      r.item_code;