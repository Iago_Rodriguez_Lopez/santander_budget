﻿select 
ac.id,
ac.name,
ac.code,
ac.description,
ac.source,
ac.item_id,
i.code,
i.description
from accounts ac 
join items i on i.id = ac.item_id;