﻿select 
i.code as item_id,
i.description as item_name,
a.code as account_code,
a.name as account_name,
t.source as transaction_source,
t.expired_date as expired_date,
extract(year from t.expired_date) as transaction_year,
extract(month from t.expired_date) AS transaction_month,
t.amount as amount
from transactions t
join accounts a on a.id = t.account_id
join items i on a.item_id = i.id
order by 
i.code,
a.code,
a.source,
transaction_year,
transaction_month;



