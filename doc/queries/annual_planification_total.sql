create or replace view annual_planification_totals_report
as
select
r.annual_planification_id,
r.year,
r.pivot_month,
r.annual_planification_name,
cast('-' as varchar) as "subfield_name",
cast('-' as varchar) as "item_code",
cast('-' as varchar) as "item_description",
sum(t.q1) as "q1",
sum(t.q2) as "q2",
sum(t.q3) as "q3",
sum(t.q4) as "q4",
sum(b.january_budget) as "january_budget",
sum(b.february_budget) as "february_budget",
sum(b.march_budget) as "march_budget",
sum(b.april_budget) as "april_budget",
sum(b.may_budget) as "may_budget" ,
sum(b.june_budget) as "june_budget",
sum(b.july_budget) as "july_budget",
sum(b.august_budget) as "august_budget",
sum(b.september_budget) as "september_budget",
sum(b.october_budget) as "october_budget",
sum(b.november_budget) as "november_budget",
sum(b.december_budget) as "december_budget",
sum(b.year_budget) as "year_budget",
sum(r.amount + r.january_transactions) as "january_transactions_budgeted",
sum(r.amount + r.february_transactions) as "february_transactions_budgeted",
sum(r.amount + r.march_transactions) as "march_transactions_budgeted",
sum(r.amount + r.april_transactions) as "april_transactions_budgeted",
sum(r.amount + r.may_transactions) as "may_transactions_budgeted",
sum(r.amount + r.june_transactions) as "june_transactions_budgeted",
sum(r.amount + r.july_transactions) as "july_transactions_budgeted",
sum(r.amount + r.august_transactions) as "august_transactions_budgeted",
sum(r.amount + r.september_transactions) as "september_transactions_budgeted",
sum(r.amount + r.october_transactions) as "october_transactions_budgeted",
sum(r.amount + r.november_transactions) as "november_transactions_budgeted",
sum(r.amount + r.december_transactions) as "december_transactions_budgeted",
3 as ord

from annual_adjustment_details_report r

join transactions_report t
on t.year = r.year AND
t.subfield_id = r.subfield_id
AND t.item_code = r.item_code

join budgets_report b on
b.year = r.year AND
b.subfield_id = r.subfield_id AND
b.item_id = r.item_id

group by
r.annual_planification_id,
r.year,
r.pivot_month,
r.annual_planification_name

order by
r.annual_planification_id,
r.year,
r.pivot_month,
r.annual_planification_name;