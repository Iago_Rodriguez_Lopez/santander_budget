﻿
select 
i.id,
i.code,
i.description,
b.year,
b.month,
b.period,
b.amount
from budgets b
join items i on i.id = b.item_id
order by i.code,b.year,b.month;