CREATE OR REPLACE VIEW transactions_report
AS
SELECT
EXTRACT(YEAR FROM transactions.expired_date) as "year",
subfields.id as "subfield_id",
subfields.name as "subfield_name",
items.id as "item_id",
items.code as "item_code",
items.description as "item_description",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 1.0 THEN transactions.amount ELSE 0 END) as "january_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 2.0 THEN transactions.amount ELSE 0 END) as "february_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 3.0 THEN transactions.amount ELSE 0 END) as "march_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 4.0 THEN transactions.amount ELSE 0 END) as "april_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 5.0 THEN transactions.amount ELSE 0 END) as "may_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 6.0 THEN transactions.amount ELSE 0 END) as "june_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 7.0 THEN transactions.amount ELSE 0 END) as "july_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 8.0 THEN transactions.amount ELSE 0 END) as "august_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 9.0 THEN transactions.amount ELSE 0 END) as "september_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 10.0 THEN transactions.amount ELSE 0 END) as "october_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 11.0 THEN transactions.amount ELSE 0 END) as "november_transactions",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 12.0 THEN transactions.amount ELSE 0 END) as "december_transactions",
SUM(transactions.amount) AS "year_transaction",
SUM(CASE WHEN EXTRACT(MONTH FROM transactions.expired_date) IN (1.0, 2.0, 3.0) THEN transactions.amount ELSE 0 END) AS "q1",
SUM(CASE WHEN EXTRACT(MONTH FROM transactions.expired_date) IN (4.0, 5.0, 6.0) THEN transactions.amount ELSE 0 END) AS "q2",
SUM(CASE WHEN EXTRACT(MONTH FROM transactions.expired_date) IN (7.0, 8.0, 9.0) THEN transactions.amount ELSE 0 END) AS "q3",
SUM(CASE WHEN EXTRACT(MONTH FROM transactions.expired_date) IN (10.0, 11.0, 12.0) THEN transactions.amount ELSE 0 END) AS "q4"
FROM transactions
INNER JOIN accounts ON accounts.id = transactions.account_id
INNER JOIN items ON items.id = accounts.item_id
INNER JOIN subfields ON subfields.id = items.subfield_id
GROUP BY
EXTRACT(YEAR FROM transactions.expired_date),
subfields.id,
subfields.name,
items.id,
items.code,
items.description
ORDER BY
EXTRACT(YEAR FROM transactions.expired_date),
subfields.id,
subfields.name,
items.id,
items.code,
items.description;