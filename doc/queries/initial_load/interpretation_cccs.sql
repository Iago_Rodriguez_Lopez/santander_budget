--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: interpretation_cccs; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (1, '94', 'SEGUROS DE VIDA Y ACCIDENTES', '2017-12-05 15:30:40.426886', '2017-12-05 15:30:40.426886');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (2, '86', 'SUELDOS Y SALARIOS', '2017-12-05 15:30:40.49957', '2017-12-05 15:30:40.49957');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (3, '99', 'INCENTIVOS PREMIOS Y OTROS', '2017-12-05 15:30:40.556498', '2017-12-05 15:30:40.556498');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (4, '95', 'SEGUROS DE SALUD', '2017-12-05 15:30:40.641847', '2017-12-05 15:30:40.641847');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (5, '101', 'OTROS BENEFICIOS SOCIALES', '2017-12-05 15:30:40.687048', '2017-12-05 15:30:40.687048');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (6, '106', 'Actividades Varias del Personal', '2017-12-05 15:30:40.697521', '2017-12-05 15:30:40.697521');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (7, '52', 'Comunicaciones Internas', '2017-12-05 15:30:40.706671', '2017-12-05 15:30:40.706671');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (8, '136', 'Actividades del Personal', '2017-12-05 15:30:40.762462', '2017-12-05 15:30:40.762462');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (9, '154', 'RETRIBUCCION VARIABLE', '2017-12-05 15:30:40.773462', '2017-12-05 15:30:40.773462');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (10, '88', 'REMUNERACIONES BASADAS EN INSTRUMENTOS DE CAPITAL', '2017-12-05 15:30:40.904216', '2017-12-05 15:30:40.904216');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (11, '87', 'HORAS EXTRAS', '2017-12-05 15:30:40.972977', '2017-12-05 15:30:40.972977');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (12, '89', 'CUOTAS DE EMPRESA A LA SEGURIDAD SOCIAL', '2017-12-05 15:30:41.097705', '2017-12-05 15:30:41.097705');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (13, '102', 'INDEMNIZACIONES', '2017-12-05 15:30:41.115368', '2017-12-05 15:30:41.115368');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (14, '73', 'COMEDORES DE EMPRESA', '2017-12-05 15:30:41.179853', '2017-12-05 15:30:41.179853');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (15, '91', 'SERVICIOS DE FORMACIÓN', '2017-12-05 15:30:41.275239', '2017-12-05 15:30:41.275239');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (16, '97', 'Uniformes', '2017-12-05 15:30:41.395311', '2017-12-05 15:30:41.395311');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (17, '152', 'DOTACIONES A PLANES DE PENSIONES DE PRESTACIÓN DEFINIDA', '2017-12-05 15:30:41.439747', '2017-12-05 15:30:41.439747');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (18, '90', 'Personal Externo', '2017-12-05 15:30:41.511521', '2017-12-05 15:30:41.511521');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (19, '153', 'BECARIOS', '2017-12-05 15:30:41.521092', '2017-12-05 15:30:41.521092');
INSERT INTO interpretation_cccs (id, code, name, created_at, updated_at) VALUES (20, '115', 'ASESOR LABORAL', '2017-12-05 15:30:41.548958', '2017-12-05 15:30:41.548958');


--
-- Name: interpretation_cccs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('interpretation_cccs_id_seq', 20, true);


--
-- PostgreSQL database dump complete
--

