--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: format_banks; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO format_banks (id, name, created_at, updated_at) VALUES (1, 'Beneficios Sociales', '2017-12-05 15:30:40.467544', '2017-12-05 15:30:40.467544');
INSERT INTO format_banks (id, name, created_at, updated_at) VALUES (2, 'Sueldo fijo', '2017-12-05 15:30:40.510728', '2017-12-05 15:30:40.510728');
INSERT INTO format_banks (id, name, created_at, updated_at) VALUES (3, 'Otros', '2017-12-05 15:30:40.567019', '2017-12-05 15:30:40.567019');
INSERT INTO format_banks (id, name, created_at, updated_at) VALUES (4, 'Formación', '2017-12-05 15:30:40.672695', '2017-12-05 15:30:40.672695');
INSERT INTO format_banks (id, name, created_at, updated_at) VALUES (5, 'Retribución variable', '2017-12-05 15:30:40.78289', '2017-12-05 15:30:40.78289');
INSERT INTO format_banks (id, name, created_at, updated_at) VALUES (6, 'Seguridad Social y otras cargas sociales obligatorias', '2017-12-05 15:30:41.107501', '2017-12-05 15:30:41.107501');
INSERT INTO format_banks (id, name, created_at, updated_at) VALUES (7, 'Indemnizaciones contabilizadas en Gtos. De Personal', '2017-12-05 15:30:41.125482', '2017-12-05 15:30:41.125482');


--
-- Name: format_banks_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('format_banks_id_seq', 7, true);


--
-- PostgreSQL database dump complete
--

