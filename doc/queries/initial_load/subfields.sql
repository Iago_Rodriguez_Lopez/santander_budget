--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: subfields; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO subfields (id, name, created_at, updated_at) VALUES (1, 'Beneficios y Otros Gastos de Personal', '2017-12-05 15:30:40.417048', '2017-12-05 15:30:40.417048');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (2, 'Remuneraciones', '2017-12-05 15:30:40.496716', '2017-12-05 15:30:40.496716');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (3, 'P. Externo y Honorarios (inc. Directorio)', '2017-12-05 15:30:40.588287', '2017-12-05 15:30:40.588287');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (4, 'Capacitación y Desarrollo', '2017-12-05 15:30:40.660595', '2017-12-05 15:30:40.660595');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (5, 'Comisiones', '2017-12-05 15:30:40.771513', '2017-12-05 15:30:40.771513');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (6, 'Otras Rem. Imponib. Tribut', '2017-12-05 15:30:40.88489', '2017-12-05 15:30:40.88489');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (7, 'Cash Flow', '2017-12-05 15:30:40.902134', '2017-12-05 15:30:40.902134');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (8, 'Horas Extraordinarias', '2017-12-05 15:30:40.970945', '2017-12-05 15:30:40.970945');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (9, 'Indemnizaciones', '2017-12-05 15:30:41.113247', '2017-12-05 15:30:41.113247');
INSERT INTO subfields (id, name, created_at, updated_at) VALUES (10, 'Provisión de Vacaciones', '2017-12-05 15:30:41.529707', '2017-12-05 15:30:41.529707');


--
-- Name: subfields_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('subfields_id_seq', 10, true);


--
-- PostgreSQL database dump complete
--

