--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: month_planifications; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO month_planifications (id, name, year, month, created_at, updated_at) VALUES (1, 'version 01', 2017, 12, '2017-12-05 18:35:54.56826', '2017-12-05 18:35:54.56826');


--
-- Name: month_planifications_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('month_planifications_id_seq', 1, true);


--
-- PostgreSQL database dump complete
--

