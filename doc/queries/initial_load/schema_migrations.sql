--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO schema_migrations (version) VALUES ('20170505212912');
INSERT INTO schema_migrations (version) VALUES ('20170508183253');
INSERT INTO schema_migrations (version) VALUES ('20170508195342');
INSERT INTO schema_migrations (version) VALUES ('20170508195401');
INSERT INTO schema_migrations (version) VALUES ('20170508195435');
INSERT INTO schema_migrations (version) VALUES ('20170508195752');
INSERT INTO schema_migrations (version) VALUES ('20170508200531');
INSERT INTO schema_migrations (version) VALUES ('20170508200553');
INSERT INTO schema_migrations (version) VALUES ('20170508200641');
INSERT INTO schema_migrations (version) VALUES ('20170508201600');
INSERT INTO schema_migrations (version) VALUES ('20170508210729');
INSERT INTO schema_migrations (version) VALUES ('20170508221207');
INSERT INTO schema_migrations (version) VALUES ('20170526190149');
INSERT INTO schema_migrations (version) VALUES ('20170612200248');
INSERT INTO schema_migrations (version) VALUES ('20170612200842');
INSERT INTO schema_migrations (version) VALUES ('20170613065216');
INSERT INTO schema_migrations (version) VALUES ('20170613073158');
INSERT INTO schema_migrations (version) VALUES ('20170710215335');
INSERT INTO schema_migrations (version) VALUES ('20170711165630');
INSERT INTO schema_migrations (version) VALUES ('20170712201955');
INSERT INTO schema_migrations (version) VALUES ('20170712224532');
INSERT INTO schema_migrations (version) VALUES ('20170714202617');
INSERT INTO schema_migrations (version) VALUES ('20170718204218');
INSERT INTO schema_migrations (version) VALUES ('20170718204544');
INSERT INTO schema_migrations (version) VALUES ('20170718213906');
INSERT INTO schema_migrations (version) VALUES ('20170725223816');
INSERT INTO schema_migrations (version) VALUES ('20170813220611');
INSERT INTO schema_migrations (version) VALUES ('20170824200747');
INSERT INTO schema_migrations (version) VALUES ('20170925203859');
INSERT INTO schema_migrations (version) VALUES ('20170925204106');
INSERT INTO schema_migrations (version) VALUES ('20170925204417');
INSERT INTO schema_migrations (version) VALUES ('20171002173449');
INSERT INTO schema_migrations (version) VALUES ('20171002174030');
INSERT INTO schema_migrations (version) VALUES ('20171016212157');
INSERT INTO schema_migrations (version) VALUES ('20171016224208');
INSERT INTO schema_migrations (version) VALUES ('20171016224744');
INSERT INTO schema_migrations (version) VALUES ('20171016231252');
INSERT INTO schema_migrations (version) VALUES ('20171020204518');
INSERT INTO schema_migrations (version) VALUES ('20171023134659');
INSERT INTO schema_migrations (version) VALUES ('20171023135142');
INSERT INTO schema_migrations (version) VALUES ('20171023135634');
INSERT INTO schema_migrations (version) VALUES ('20171023144951');
INSERT INTO schema_migrations (version) VALUES ('20171023145842');
INSERT INTO schema_migrations (version) VALUES ('20171023151758');
INSERT INTO schema_migrations (version) VALUES ('20171023152353');
INSERT INTO schema_migrations (version) VALUES ('20171023201454');
INSERT INTO schema_migrations (version) VALUES ('20171026201554');


--
-- PostgreSQL database dump complete
--

