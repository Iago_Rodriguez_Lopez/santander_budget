--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: interpretation_cargabals; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (1, '43040', 'Beneficios sociales', '2017-12-05 15:30:40.443841', '2017-12-05 15:30:40.443841');
INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (2, '43000', 'Retribuciones fijas', '2017-12-05 15:30:40.504499', '2017-12-05 15:30:40.504499');
INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (3, '43049', 'Otros gastos de personal', '2017-12-05 15:30:40.561016', '2017-12-05 15:30:40.561016');
INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (4, '43041', 'Formación', '2017-12-05 15:30:40.666613', '2017-12-05 15:30:40.666613');
INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (5, '43001', 'Retribuciones variables (Bonus)', '2017-12-05 15:30:40.777233', '2017-12-05 15:30:40.777233');
INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (6, '43042', 'Remuneraciones basadas en instrumentos de capital', '2017-12-05 15:30:40.90811', '2017-12-05 15:30:40.90811');
INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (7, '4301', 'cuotas a la seguridad social', '2017-12-05 15:30:41.101725', '2017-12-05 15:30:41.101725');
INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (8, '4305', 'Indemnizaciones', '2017-12-05 15:30:41.119704', '2017-12-05 15:30:41.119704');
INSERT INTO interpretation_cargabals (id, code, name, created_at, updated_at) VALUES (9, '4306', '0', '2017-12-05 15:30:41.444624', '2017-12-05 15:30:41.444624');


--
-- Name: interpretation_cargabals_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('interpretation_cargabals_id_seq', 9, true);


--
-- PostgreSQL database dump complete
--

