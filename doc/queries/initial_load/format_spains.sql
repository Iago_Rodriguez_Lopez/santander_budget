--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: format_spains; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO format_spains (id, name, created_at, updated_at) VALUES (1, 'Beneficios Sociales', '2017-12-05 15:30:40.451745', '2017-12-05 15:30:40.451745');
INSERT INTO format_spains (id, name, created_at, updated_at) VALUES (2, 'Sueldo fijo', '2017-12-05 15:30:40.506629', '2017-12-05 15:30:40.506629');
INSERT INTO format_spains (id, name, created_at, updated_at) VALUES (3, 'Otros', '2017-12-05 15:30:40.56306', '2017-12-05 15:30:40.56306');
INSERT INTO format_spains (id, name, created_at, updated_at) VALUES (4, 'Formación', '2017-12-05 15:30:40.668726', '2017-12-05 15:30:40.668726');
INSERT INTO format_spains (id, name, created_at, updated_at) VALUES (5, 'Retribución variable', '2017-12-05 15:30:40.779097', '2017-12-05 15:30:40.779097');
INSERT INTO format_spains (id, name, created_at, updated_at) VALUES (6, 'Seguridad Social y otras cargas sociales obligatorias', '2017-12-05 15:30:41.103657', '2017-12-05 15:30:41.103657');
INSERT INTO format_spains (id, name, created_at, updated_at) VALUES (7, 'Indemnizaciones contabilizadas en Gtos. De Personal', '2017-12-05 15:30:41.121738', '2017-12-05 15:30:41.121738');


--
-- Name: format_spains_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('format_spains_id_seq', 7, true);


--
-- PostgreSQL database dump complete
--

