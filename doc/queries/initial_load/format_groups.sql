--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: format_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO format_groups (id, name, created_at, updated_at) VALUES (1, 'Beneficios Sociales', '2017-12-05 15:30:40.459751', '2017-12-05 15:30:40.459751');
INSERT INTO format_groups (id, name, created_at, updated_at) VALUES (2, 'Sueldo fijo', '2017-12-05 15:30:40.508708', '2017-12-05 15:30:40.508708');
INSERT INTO format_groups (id, name, created_at, updated_at) VALUES (3, 'Otros', '2017-12-05 15:30:40.565085', '2017-12-05 15:30:40.565085');
INSERT INTO format_groups (id, name, created_at, updated_at) VALUES (4, 'Formación', '2017-12-05 15:30:40.670704', '2017-12-05 15:30:40.670704');
INSERT INTO format_groups (id, name, created_at, updated_at) VALUES (5, 'Retribución variable', '2017-12-05 15:30:40.780995', '2017-12-05 15:30:40.780995');
INSERT INTO format_groups (id, name, created_at, updated_at) VALUES (6, 'Seguridad Social y otras cargas sociales obligatorias', '2017-12-05 15:30:41.105564', '2017-12-05 15:30:41.105564');
INSERT INTO format_groups (id, name, created_at, updated_at) VALUES (7, 'Indemnizaciones contabilizadas en Gtos. De Personal', '2017-12-05 15:30:41.123666', '2017-12-05 15:30:41.123666');


--
-- Name: format_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('format_groups_id_seq', 7, true);


--
-- PostgreSQL database dump complete
--

