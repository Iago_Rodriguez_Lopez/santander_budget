--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: base_adjustments; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (1, NULL, NULL, NULL, 1, 6, 41, '2017-12-05 18:35:54.592691', '2017-12-05 18:35:54.592691');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (2, NULL, NULL, NULL, 1, 6, 52, '2017-12-05 18:35:54.600043', '2017-12-05 18:35:54.600043');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (3, NULL, NULL, NULL, 1, 6, 55, '2017-12-05 18:35:54.60646', '2017-12-05 18:35:54.60646');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (4, NULL, NULL, NULL, 1, 6, 57, '2017-12-05 18:35:54.612431', '2017-12-05 18:35:54.612431');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (5, NULL, NULL, NULL, 1, 6, 61, '2017-12-05 18:35:54.618743', '2017-12-05 18:35:54.618743');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (6, NULL, NULL, NULL, 1, 6, 62, '2017-12-05 18:35:54.624785', '2017-12-05 18:35:54.624785');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (7, NULL, NULL, NULL, 1, 6, 63, '2017-12-05 18:35:54.630825', '2017-12-05 18:35:54.630825');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (8, NULL, NULL, NULL, 1, 6, 64, '2017-12-05 18:35:54.639805', '2017-12-05 18:35:54.639805');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (9, NULL, NULL, NULL, 1, 8, 51, '2017-12-05 18:35:54.648212', '2017-12-05 18:35:54.648212');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (10, NULL, NULL, NULL, 1, 1, 1, '2017-12-05 18:35:54.661933', '2017-12-05 18:35:54.661933');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (11, NULL, NULL, NULL, 1, 1, 7, '2017-12-05 18:35:54.668398', '2017-12-05 18:35:54.668398');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (12, NULL, NULL, NULL, 1, 1, 12, '2017-12-05 18:35:54.676944', '2017-12-05 18:35:54.676944');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (13, NULL, NULL, NULL, 1, 1, 13, '2017-12-05 18:35:54.683498', '2017-12-05 18:35:54.683498');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (14, NULL, NULL, NULL, 1, 1, 14, '2017-12-05 18:35:54.689386', '2017-12-05 18:35:54.689386');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (15, NULL, NULL, NULL, 1, 1, 15, '2017-12-05 18:35:54.695321', '2017-12-05 18:35:54.695321');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (16, NULL, NULL, NULL, 1, 1, 16, '2017-12-05 18:35:54.701894', '2017-12-05 18:35:54.701894');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (17, NULL, NULL, NULL, 1, 1, 17, '2017-12-05 18:35:54.70806', '2017-12-05 18:35:54.70806');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (18, NULL, NULL, NULL, 1, 1, 19, '2017-12-05 18:35:54.716576', '2017-12-05 18:35:54.716576');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (19, NULL, NULL, NULL, 1, 1, 20, '2017-12-05 18:35:54.742224', '2017-12-05 18:35:54.742224');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (20, NULL, NULL, NULL, 1, 1, 21, '2017-12-05 18:35:54.755582', '2017-12-05 18:35:54.755582');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (21, NULL, NULL, NULL, 1, 1, 22, '2017-12-05 18:35:54.774804', '2017-12-05 18:35:54.774804');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (22, NULL, NULL, NULL, 1, 1, 23, '2017-12-05 18:35:54.79096', '2017-12-05 18:35:54.79096');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (23, NULL, NULL, NULL, 1, 1, 24, '2017-12-05 18:35:54.804384', '2017-12-05 18:35:54.804384');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (24, NULL, NULL, NULL, 1, 1, 25, '2017-12-05 18:35:54.814742', '2017-12-05 18:35:54.814742');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (25, NULL, NULL, NULL, 1, 1, 26, '2017-12-05 18:35:54.845057', '2017-12-05 18:35:54.845057');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (26, NULL, NULL, NULL, 1, 1, 27, '2017-12-05 18:35:54.864977', '2017-12-05 18:35:54.864977');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (27, NULL, NULL, NULL, 1, 1, 28, '2017-12-05 18:35:54.878654', '2017-12-05 18:35:54.878654');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (28, NULL, NULL, NULL, 1, 1, 29, '2017-12-05 18:35:54.893489', '2017-12-05 18:35:54.893489');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (29, NULL, NULL, NULL, 1, 1, 32, '2017-12-05 18:35:54.908312', '2017-12-05 18:35:54.908312');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (30, NULL, NULL, NULL, 1, 1, 33, '2017-12-05 18:35:54.921684', '2017-12-05 18:35:54.921684');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (31, NULL, NULL, NULL, 1, 1, 35, '2017-12-05 18:35:54.961367', '2017-12-05 18:35:54.961367');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (32, NULL, NULL, NULL, 1, 1, 36, '2017-12-05 18:35:55.008599', '2017-12-05 18:35:55.008599');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (33, NULL, NULL, NULL, 1, 1, 37, '2017-12-05 18:35:55.036275', '2017-12-05 18:35:55.036275');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (34, NULL, NULL, NULL, 1, 1, 45, '2017-12-05 18:35:55.068657', '2017-12-05 18:35:55.068657');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (35, NULL, NULL, NULL, 1, 1, 46, '2017-12-05 18:35:55.086778', '2017-12-05 18:35:55.086778');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (36, NULL, NULL, NULL, 1, 1, 47, '2017-12-05 18:35:55.09883', '2017-12-05 18:35:55.09883');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (37, NULL, NULL, NULL, 1, 1, 56, '2017-12-05 18:35:55.11662', '2017-12-05 18:35:55.11662');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (38, NULL, NULL, NULL, 1, 1, 60, '2017-12-05 18:35:55.135775', '2017-12-05 18:35:55.135775');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (39, NULL, NULL, NULL, 1, 1, 69, '2017-12-05 18:35:55.148778', '2017-12-05 18:35:55.148778');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (40, NULL, NULL, NULL, 1, 1, 70, '2017-12-05 18:35:55.170701', '2017-12-05 18:35:55.170701');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (41, NULL, NULL, NULL, 1, 1, 71, '2017-12-05 18:35:55.183699', '2017-12-05 18:35:55.183699');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (42, NULL, NULL, NULL, 1, 1, 72, '2017-12-05 18:35:55.199008', '2017-12-05 18:35:55.199008');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (43, NULL, NULL, NULL, 1, 1, 73, '2017-12-05 18:35:55.219053', '2017-12-05 18:35:55.219053');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (44, NULL, NULL, NULL, 1, 1, 74, '2017-12-05 18:35:55.230758', '2017-12-05 18:35:55.230758');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (45, NULL, NULL, NULL, 1, 1, 75, '2017-12-05 18:35:55.24069', '2017-12-05 18:35:55.24069');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (46, NULL, NULL, NULL, 1, 1, 76, '2017-12-05 18:35:55.256452', '2017-12-05 18:35:55.256452');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (47, NULL, NULL, NULL, 1, 1, 77, '2017-12-05 18:35:55.272116', '2017-12-05 18:35:55.272116');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (48, NULL, NULL, NULL, 1, 1, 78, '2017-12-05 18:35:55.286153', '2017-12-05 18:35:55.286153');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (49, NULL, NULL, NULL, 1, 1, 79, '2017-12-05 18:35:55.306268', '2017-12-05 18:35:55.306268');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (50, NULL, NULL, NULL, 1, 1, 81, '2017-12-05 18:35:55.316468', '2017-12-05 18:35:55.316468');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (51, NULL, NULL, NULL, 1, 1, 82, '2017-12-05 18:35:55.327316', '2017-12-05 18:35:55.327316');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (52, NULL, NULL, NULL, 1, 1, 83, '2017-12-05 18:35:55.340391', '2017-12-05 18:35:55.340391');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (53, NULL, NULL, NULL, 1, 1, 85, '2017-12-05 18:35:55.352866', '2017-12-05 18:35:55.352866');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (54, NULL, NULL, NULL, 1, 1, 87, '2017-12-05 18:35:55.3633', '2017-12-05 18:35:55.3633');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (55, NULL, NULL, NULL, 1, 1, 88, '2017-12-05 18:35:55.37911', '2017-12-05 18:35:55.37911');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (56, NULL, NULL, NULL, 1, 1, 89, '2017-12-05 18:35:55.395525', '2017-12-05 18:35:55.395525');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (57, NULL, NULL, NULL, 1, 1, 90, '2017-12-05 18:35:55.408349', '2017-12-05 18:35:55.408349');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (58, NULL, NULL, NULL, 1, 1, 91, '2017-12-05 18:35:55.423635', '2017-12-05 18:35:55.423635');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (59, NULL, NULL, NULL, 1, 1, 92, '2017-12-05 18:35:55.434783', '2017-12-05 18:35:55.434783');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (60, NULL, NULL, NULL, 1, 1, 93, '2017-12-05 18:35:55.449282', '2017-12-05 18:35:55.449282');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (61, NULL, NULL, NULL, 1, 1, 94, '2017-12-05 18:35:55.463765', '2017-12-05 18:35:55.463765');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (62, NULL, NULL, NULL, 1, 1, 95, '2017-12-05 18:35:55.487794', '2017-12-05 18:35:55.487794');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (63, NULL, NULL, NULL, 1, 1, 96, '2017-12-05 18:35:55.502863', '2017-12-05 18:35:55.502863');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (64, NULL, NULL, NULL, 1, 1, 97, '2017-12-05 18:35:55.515087', '2017-12-05 18:35:55.515087');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (65, NULL, NULL, NULL, 1, 1, 98, '2017-12-05 18:35:55.528307', '2017-12-05 18:35:55.528307');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (66, NULL, NULL, NULL, 1, 1, 107, '2017-12-05 18:35:55.541926', '2017-12-05 18:35:55.541926');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (67, NULL, NULL, NULL, 1, 1, 112, '2017-12-05 18:35:55.557849', '2017-12-05 18:35:55.557849');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (68, NULL, NULL, NULL, 1, 1, 113, '2017-12-05 18:35:55.573587', '2017-12-05 18:35:55.573587');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (69, NULL, NULL, NULL, 1, 1, 114, '2017-12-05 18:35:55.585898', '2017-12-05 18:35:55.585898');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (70, NULL, NULL, NULL, 1, 2, 2, '2017-12-05 18:35:55.605062', '2017-12-05 18:35:55.605062');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (71, NULL, NULL, NULL, 1, 2, 3, '2017-12-05 18:35:55.620501', '2017-12-05 18:35:55.620501');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (72, NULL, NULL, NULL, 1, 2, 4, '2017-12-05 18:35:55.634936', '2017-12-05 18:35:55.634936');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (73, NULL, NULL, NULL, 1, 2, 5, '2017-12-05 18:35:55.644804', '2017-12-05 18:35:55.644804');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (76, NULL, NULL, NULL, 1, 2, 9, '2017-12-05 18:35:55.69375', '2017-12-05 18:35:55.69375');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (80, NULL, NULL, NULL, 1, 2, 40, '2017-12-05 18:35:55.747251', '2017-12-05 18:35:55.747251');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (82, NULL, NULL, NULL, 1, 2, 44, '2017-12-05 18:35:55.770197', '2017-12-05 18:35:55.770197');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (83, NULL, NULL, NULL, 1, 2, 48, '2017-12-05 18:35:55.784567', '2017-12-05 18:35:55.784567');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (89, NULL, NULL, NULL, 1, 2, 99, '2017-12-05 18:35:55.860812', '2017-12-05 18:35:55.860812');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (90, NULL, NULL, NULL, 1, 3, 10, '2017-12-05 18:35:55.884808', '2017-12-05 18:35:55.884808');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (91, NULL, NULL, NULL, 1, 3, 31, '2017-12-05 18:35:55.905554', '2017-12-05 18:35:55.905554');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (92, NULL, NULL, NULL, 1, 3, 108, '2017-12-05 18:35:55.925422', '2017-12-05 18:35:55.925422');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (93, NULL, NULL, NULL, 1, 3, 109, '2017-12-05 18:35:55.939007', '2017-12-05 18:35:55.939007');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (94, NULL, NULL, NULL, 1, 10, 110, '2017-12-05 18:35:55.959516', '2017-12-05 18:35:55.959516');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (95, NULL, NULL, NULL, 1, 10, 111, '2017-12-05 18:35:55.969337', '2017-12-05 18:35:55.969337');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (96, NULL, NULL, NULL, 1, 4, 18, '2017-12-05 18:35:55.995469', '2017-12-05 18:35:55.995469');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (97, NULL, NULL, NULL, 1, 4, 84, '2017-12-05 18:35:56.012006', '2017-12-05 18:35:56.012006');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (98, NULL, NULL, NULL, 1, 4, 101, '2017-12-05 18:35:56.023725', '2017-12-05 18:35:56.023725');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (99, NULL, NULL, NULL, 1, 4, 102, '2017-12-05 18:35:56.033672', '2017-12-05 18:35:56.033672');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (100, NULL, NULL, NULL, 1, 4, 103, '2017-12-05 18:35:56.046082', '2017-12-05 18:35:56.046082');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (101, NULL, NULL, NULL, 1, 4, 104, '2017-12-05 18:35:56.061627', '2017-12-05 18:35:56.061627');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (102, NULL, NULL, NULL, 1, 4, 105, '2017-12-05 18:35:56.079614', '2017-12-05 18:35:56.079614');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (103, NULL, NULL, NULL, 1, 4, 106, '2017-12-05 18:35:56.091777', '2017-12-05 18:35:56.091777');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (104, NULL, NULL, NULL, 1, 5, 30, '2017-12-05 18:35:56.110321', '2017-12-05 18:35:56.110321');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (105, NULL, NULL, NULL, 1, 5, 34, '2017-12-05 18:35:56.128746', '2017-12-05 18:35:56.128746');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (106, NULL, NULL, NULL, 1, 5, 58, '2017-12-05 18:35:56.14374', '2017-12-05 18:35:56.14374');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (107, NULL, NULL, NULL, 1, 5, 59, '2017-12-05 18:35:56.160322', '2017-12-05 18:35:56.160322');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (108, NULL, NULL, NULL, 1, 9, 66, '2017-12-05 18:35:56.178532', '2017-12-05 18:35:56.178532');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (109, NULL, NULL, NULL, 1, 9, 67, '2017-12-05 18:35:56.214487', '2017-12-05 18:35:56.214487');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (110, NULL, NULL, NULL, 1, 9, 100, '2017-12-05 18:35:56.231422', '2017-12-05 18:35:56.231422');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (111, NULL, NULL, NULL, 1, 7, 43, '2017-12-05 18:35:56.255879', '2017-12-05 18:35:56.255879');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (114, NULL, NULL, NULL, 1, 7, 54, '2017-12-05 18:35:56.313078', '2017-12-05 18:35:56.313078');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (113, NULL, 'Presupuesto mes anterior', 4108664995, 1, 7, 53, '2017-12-05 18:35:56.296664', '2017-12-05 18:36:38.513115');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (112, NULL, 'Presupuesto mes anterior', 151036402, 1, 7, 49, '2017-12-05 18:35:56.280125', '2017-12-05 18:36:38.523476');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (78, NULL, 'Presupuesto mes anterior', 16241650817, 1, 2, 38, '2017-12-05 18:35:55.716515', '2017-12-05 18:37:19.534884');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (79, NULL, 'Presupuesto mes anterior', 3727436235, 1, 2, 39, '2017-12-05 18:35:55.727868', '2017-12-05 18:37:19.552333');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (81, NULL, 'Presupuesto mes anterior', 24470787, 1, 2, 42, '2017-12-05 18:35:55.758378', '2017-12-05 18:37:19.565822');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (74, NULL, 'Presupuesto mes anterior', 215070, 1, 2, 6, '2017-12-05 18:35:55.665386', '2017-12-05 18:37:19.575655');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (85, NULL, 'Presupuesto mes anterior', 171003158, 1, 2, 65, '2017-12-05 18:35:55.806912', '2017-12-05 18:37:19.586526');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (86, NULL, 'Presupuesto mes anterior', 355184721, 1, 2, 68, '2017-12-05 18:35:55.825764', '2017-12-05 18:37:19.595013');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (88, NULL, 'Presupuesto mes anterior', 254721462, 1, 2, 86, '2017-12-05 18:35:55.848307', '2017-12-05 18:37:19.602516');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (87, NULL, 'Presupuesto mes anterior', 22680185, 1, 2, 80, '2017-12-05 18:35:55.837515', '2017-12-05 18:37:19.609983');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (84, NULL, 'Presupuesto mes anterior', 120273930, 1, 2, 50, '2017-12-05 18:35:55.793663', '2017-12-05 18:37:19.617294');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (75, NULL, 'Presupuesto mes anterior', 3111294, 1, 2, 8, '2017-12-05 18:35:55.679922', '2017-12-05 18:37:19.624858');
INSERT INTO base_adjustments (id, name, base_type, amount, month_planification_id, subfield_id, item_id, created_at, updated_at) VALUES (77, NULL, 'Presupuesto mes anterior', 643759183, 1, 2, 11, '2017-12-05 18:35:55.703992', '2017-12-05 18:37:19.635153');


--
-- Name: base_adjustments_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('base_adjustments_id_seq', 114, true);


--
-- PostgreSQL database dump complete
--

