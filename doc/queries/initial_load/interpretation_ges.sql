--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.9
-- Dumped by pg_dump version 9.5.9

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Data for Name: interpretation_ges; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (1, '72000', 'Beneficios Sociales', '2017-12-05 15:30:40.435294', '2017-12-05 15:30:40.435294');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (2, '72000', 'Salario', '2017-12-05 15:30:40.502067', '2017-12-05 15:30:40.502067');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (3, '73000', 'Salario', '2017-12-05 15:30:40.545662', '2017-12-05 15:30:40.545662');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (4, '78000', 'Otros gastos de personal', '2017-12-05 15:30:40.558888', '2017-12-05 15:30:40.558888');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (5, '76100', 'Beneficios Sociales', '2017-12-05 15:30:40.610064', '2017-12-05 15:30:40.610064');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (6, '78005', 'Beneficios Sociales', '2017-12-05 15:30:40.643869', '2017-12-05 15:30:40.643869');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (7, '78000', 'Formacion', '2017-12-05 15:30:40.664243', '2017-12-05 15:30:40.664243');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (8, '78000', 'Beneficios Sociales', '2017-12-05 15:30:40.689098', '2017-12-05 15:30:40.689098');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (9, '73000', 'Retribucion Variable', '2017-12-05 15:30:40.775374', '2017-12-05 15:30:40.775374');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (10, '78005', 'Otros gastos de personal', '2017-12-05 15:30:40.837626', '2017-12-05 15:30:40.837626');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (11, '78500', 'Remuneracion basada instrumen', '2017-12-05 15:30:40.906197', '2017-12-05 15:30:40.906197');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (12, '76100', 'Salario', '2017-12-05 15:30:40.963133', '2017-12-05 15:30:40.963133');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (13, '78002', 'Salario', '2017-12-05 15:30:41.059269', '2017-12-05 15:30:41.059269');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (14, '78004', 'Salario', '2017-12-05 15:30:41.068985', '2017-12-05 15:30:41.068985');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (15, '74000', 'Cuotas a la Seguridad Social', '2017-12-05 15:30:41.09972', '2017-12-05 15:30:41.09972');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (16, '76000', 'Indemnizaciones', '2017-12-05 15:30:41.117527', '2017-12-05 15:30:41.117527');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (17, '78003', 'Beneficios Sociales', '2017-12-05 15:30:41.171431', '2017-12-05 15:30:41.171431');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (18, '78003', 'Otros gastos de personal', '2017-12-05 15:30:41.181949', '2017-12-05 15:30:41.181949');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (19, '78005', 'Salario', '2017-12-05 15:30:41.24097', '2017-12-05 15:30:41.24097');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (20, '77000', 'Formacion', '2017-12-05 15:30:41.277148', '2017-12-05 15:30:41.277148');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (21, '78002', 'Beneficios Sociales', '2017-12-05 15:30:41.350637', '2017-12-05 15:30:41.350637');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (22, '78002', 'Otros gastos de personal', '2017-12-05 15:30:41.359767', '2017-12-05 15:30:41.359767');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (23, '78001', 'Beneficios Sociales', '2017-12-05 15:30:41.398241', '2017-12-05 15:30:41.398241');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (24, '75000', '0', '2017-12-05 15:30:41.442246', '2017-12-05 15:30:41.442246');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (25, '78005', 'Formacion', '2017-12-05 15:30:41.471602', '2017-12-05 15:30:41.471602');
INSERT INTO interpretation_ges (id, code, name, created_at, updated_at) VALUES (26, '73010', 'Prov de Vacaciones', '2017-12-05 15:30:41.532572', '2017-12-05 15:30:41.532572');


--
-- Name: interpretation_ges_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('interpretation_ges_id_seq', 26, true);


--
-- PostgreSQL database dump complete
--

