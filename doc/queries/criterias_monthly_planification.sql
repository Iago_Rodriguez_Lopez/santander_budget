﻿select
items.id as item_id,
items.code as item_code,
items.description as item_description,
SUM(transactions.amount) AS total_amount
from transactions
inner join accounts on accounts.id = transactions.account_id
inner join items ON items.id = accounts.item_id
where to_date('2017-01-01','YYYY-MM-DD') <= transactions.expired_date and 
transactions.expired_date <= to_date('2017-09-30','YYYY-MM-DD')
group by items.id
order by items.code;

select *
from transactions
inner join accounts on accounts.id = transactions.account_id
inner join items ON items.id = accounts.item_id
where to_date('2017-10-01','YYYY-MM-DD') <= transactions.expired_date and 
transactions.expired_date <= to_date('2017-10-31','YYYY-MM-DD');



SELECT
items.id as item_id,
items.code as item_code,
items.description as item_description,
SUM(transactions.amount) AS total_amount,
(cast(SUM(transactions.amount) as decimal)/date_part('month', to_date('2017-10-01','YYYY-MM-DD'))) as monthly_average
FROM transactions
INNER JOIN accounts ON accounts.id = transactions.account_id
INNER JOIN items ON items.id = accounts.item_id
WHERE to_date('2017-01-01','YYYY-MM-DD') <= transactions.expired_date AND 
transactions.expired_date <= to_date('2017-10-31','YYYY-MM-DD')
GROUP BY items.id
ORDER BY items.code


