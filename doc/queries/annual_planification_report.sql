create or replace view annual_planification_report
as
select
annual_report.annual_planification_id,
annual_report.year,
annual_report.pivot_month,
annual_report.annual_planification_name,
annual_report.subfield_name,
annual_report.item_code,
annual_report.item_description,
annual_report.q1,
annual_report.q2,
annual_report.q3,
annual_report.q4,
annual_report.january_budget,
annual_report.february_budget,
annual_report.march_budget,
annual_report.april_budget,
annual_report.may_budget,
annual_report.june_budget,
annual_report.july_budget,
annual_report.august_budget,
annual_report.september_budget,
annual_report.october_budget,
annual_report.november_budget,
annual_report.december_budget,
annual_report.year_budget,
annual_report.january_transactions_budgeted,
annual_report.february_transactions_budgeted,
annual_report.march_transactions_budgeted,
annual_report.april_transactions_budgeted,
annual_report.may_transactions_budgeted,
annual_report.june_transactions_budgeted,
annual_report.july_transactions_budgeted,
annual_report.august_transactions_budgeted,
annual_report.september_transactions_budgeted,
annual_report.october_transactions_budgeted,
annual_report.november_transactions_budgeted,
annual_report.december_transactions_budgeted,
annual_report.ord
from(
	select * from
	annual_planification_items_detail
	UNION ALL
	select * from
	annual_planification_subfields_detail
	UNION ALL
	select * from
	annual_planification_totals_report
) as annual_report
order by
annual_report.subfield_name,
annual_report.item_code,
annual_report.ord;