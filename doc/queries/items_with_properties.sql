﻿select 
i.code, 
i.description, 
ccc.id, 
ccc.name,
ccc.code,
iges.id,
iges.name,
iges.code,
fs.id,
fs.name,
s.id,
s.name,
cargs.id,
cargs.code,
cargs.name,
fb.id,
fb.name,
fg.id,
fg.name
from items i
join interpretation_cccs ccc on ccc.id = i.interpretation_ccc_id
join interpretation_ges iges on iges.id = i.interpretation_ge_id
join format_spains fs on fs.id = i.format_spain_id
join subfields s on s.id = i.subfield_id
join interpretation_cargabals cargs on cargs.id = i.interpretation_cargabal_id
join format_banks fb on fb.id = i.format_bank_id
join format_groups fg on fg.id = i.format_group_id;
