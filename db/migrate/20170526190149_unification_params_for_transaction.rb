class UnificationParamsForTransaction < ActiveRecord::Migration
  def change
    rename_column :transactions, :kind, :source
    add_column :transactions, :expired_date, :datetime # Is the date when the document is paid.
    add_column :transactions, :document_date, :datetime
    add_column :transactions, :comment, :text
  end
end
