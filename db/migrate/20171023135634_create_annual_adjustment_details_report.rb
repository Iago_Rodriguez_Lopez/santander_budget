class CreateAnnualAdjustmentDetailsReport < ActiveRecord::Migration
  def up
    self.connection.execute %Q(
      CREATE OR REPLACE VIEW annual_adjustment_details_report
      AS
      select
      annual_planifications.year as "year",
      annual_planifications.pivot_month as "pivot_month",
      annual_planifications.id as "annual_planification_id",
      annual_planifications.name as "annual_planification_name",
      subfields.id as "subfield_id",
      subfields.name as "subfield_name",
      items.id as "item_id",
      items.code as "item_code",
      annual_base_adjustments.id ,
      annual_base_adjustments.name ,
      annual_base_adjustments.base_type ,
      annual_base_adjustments.amount ,
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 1.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "january_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 2.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "february_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 3.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "march_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 4.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "april_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 5.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "may_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 6.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "june_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 7.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "july_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 8.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "august_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 9.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "september_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 10.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "october_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 11.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "november_transactions",
      SUM(CASE EXTRACT(MONTH FROM theoric_annual_adjustments.forescat_date) WHEN 12.0 THEN theoric_annual_adjustments.amount ELSE 0 END) as "december_transactions",
      sum(theoric_annual_adjustments.amount) AS "year_theorical_spent_budget"
      from annual_planifications
      join annual_base_adjustments on annual_base_adjustments.annual_planification_id = annual_planifications.id
      join items on items.id = annual_base_adjustments.item_id
      join subfields on subfields.id = annual_base_adjustments.subfield_id
      join theoric_annual_adjustments on theoric_annual_adjustments.annual_base_adjustment_id = annual_base_adjustments.id
      group by
      annual_planifications.year,
      annual_planifications.pivot_month,
      annual_planifications.id,
      subfields.id,
      items.id,
      annual_base_adjustments.id
      order by
      annual_planifications.year,
      annual_planifications.pivot_month,
      annual_planifications.id,
      subfields.id,
      items.id,
      annual_base_adjustments.id;
    )
  end

  def down
    self.connection.execute "DROP VIEW IF EXISTS annual_adjustment_details_report;"
  end
end
