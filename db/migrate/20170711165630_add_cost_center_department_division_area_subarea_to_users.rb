class AddCostCenterDepartmentDivisionAreaSubareaToUsers < ActiveRecord::Migration
  def change
    add_column :users, :cost_center, :string
    add_column :users, :department, :string
    add_column :users, :division, :string
    add_column :users, :area, :string
    add_column :users, :subarea, :string
  end
end
