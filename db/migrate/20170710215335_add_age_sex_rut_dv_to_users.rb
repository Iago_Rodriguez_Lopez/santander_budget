class AddAgeSexRutDvToUsers < ActiveRecord::Migration
  def change
    add_column :users, :age, :integer
    add_column :users, :sex, :string
    add_column :users, :rut, :integer
    add_column :users, :dv, :string
  end
end
