class AddItemToAccounts < ActiveRecord::Migration
  def change
    add_reference :accounts, :item, index: true, foreign_key: true
  end
end
