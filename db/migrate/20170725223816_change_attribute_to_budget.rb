class ChangeAttributeToBudget < ActiveRecord::Migration
  def change
    remove_column :budgets, :item_code, :string
    remove_column :budgets, :item_name, :string
    add_reference :budgets, :item, index: true
    add_foreign_key :budgets, :items
  end
end
