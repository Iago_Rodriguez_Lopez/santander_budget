class CreateAnnualPlanifications < ActiveRecord::Migration
  def change
    create_table :annual_planifications do |t|
      t.string :name
      t.integer :year
      t.integer :pivot_month

      t.timestamps null: false
    end
  end
end
