class ReferencesOfAccountToItem < ActiveRecord::Migration
  def change
    remove_reference :items, :account, index:true, foreign_key: true
  end
end
