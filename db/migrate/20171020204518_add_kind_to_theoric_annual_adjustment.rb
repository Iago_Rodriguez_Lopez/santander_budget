class AddKindToTheoricAnnualAdjustment < ActiveRecord::Migration
  def change
    add_column :theoric_annual_adjustments, :kind, :string
    add_column :theoric_annual_adjustments, :percentage, :decimal
  end
end
