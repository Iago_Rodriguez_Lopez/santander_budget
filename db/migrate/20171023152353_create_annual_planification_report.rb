class CreateAnnualPlanificationReport < ActiveRecord::Migration
  def up
    self.connection.execute %Q(
      create or replace view annual_planification_report
      as
      select
      annual_report.annual_planification_id,
      annual_report.year,
      annual_report.pivot_month,
      annual_report.annual_planification_name,
      annual_report.subfield_name,
      annual_report.item_code,
      annual_report.item_description,
      annual_report.january_transactions,
      annual_report.february_transactions,
      annual_report.march_transactions,
      annual_report.april_transactions,
      annual_report.may_transactions,
      annual_report.june_transactions,
      annual_report.july_transactions,
      annual_report.august_transactions,
      annual_report.september_transactions,
      annual_report.october_transactions,
      annual_report.november_transactions,
      annual_report.december_transactions,
      annual_report.year_transaction,
      annual_report.q1,
      annual_report.q1_1,
      annual_report.q1_2,
      annual_report.q1_3,
      annual_report.q2,
      annual_report.q2_4,
      annual_report.q2_5,
      annual_report.q2_6,
      annual_report.q3,
      annual_report.q3_7,
      annual_report.q3_8,
      annual_report.q3_9,
      annual_report.q4,
      annual_report.q4_10,
      annual_report.q4_11,
      annual_report.q4_12,
      annual_report.january_budget,
      annual_report.february_budget,
      annual_report.march_budget,
      annual_report.april_budget,
      annual_report.may_budget,
      annual_report.june_budget,
      annual_report.july_budget,
      annual_report.august_budget,
      annual_report.september_budget,
      annual_report.october_budget,
      annual_report.november_budget,
      annual_report.december_budget,
      annual_report.year_budget,
      annual_report.january_transactions_budgeted,
      annual_report.february_transactions_budgeted,
      annual_report.march_transactions_budgeted,
      annual_report.april_transactions_budgeted,
      annual_report.may_transactions_budgeted,
      annual_report.june_transactions_budgeted,
      annual_report.july_transactions_budgeted,
      annual_report.august_transactions_budgeted,
      annual_report.september_transactions_budgeted,
      annual_report.october_transactions_budgeted,
      annual_report.november_transactions_budgeted,
      annual_report.december_transactions_budgeted,
      annual_report.ord
      from(
        select * from
        annual_planification_items_detail
        UNION ALL
        select * from
        annual_planification_subfields_detail
        UNION ALL
        select * from
        annual_planification_totals_report
      ) as annual_report
      order by
      annual_report.subfield_name,
      annual_report.item_code,
      annual_report.ord;

    )
  end

  def down
    self.connection.execute "DROP VIEWS IF EXISTS annual_planification_report;"
  end
end
