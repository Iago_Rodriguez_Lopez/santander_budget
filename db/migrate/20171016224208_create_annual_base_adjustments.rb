class CreateAnnualBaseAdjustments < ActiveRecord::Migration
  def change
    create_table :annual_base_adjustments do |t|
      t.string :name
      t.string :base_type
      t.float :amount
      t.references :annual_planification, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
