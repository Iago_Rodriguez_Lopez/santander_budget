class AddEnterpriseCodeEnterpriseNameToTransaction < ActiveRecord::Migration
  def change
    add_column :transactions, :enterprise_code, :string
    add_column :transactions, :enterprise_name, :string
  end
end
