class CreateBaseAdjustments < ActiveRecord::Migration
  def change
    create_table :base_adjustments do |t|
      t.string :name
      t.string :base_type
      t.float :amount
      t.references :month_planification
      t.references :subfield
      t.references :item
      t.timestamps null: false
    end
  end
end
