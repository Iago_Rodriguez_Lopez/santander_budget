class CreateInterpretationGes < ActiveRecord::Migration
  def change
    create_table :interpretation_ges do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
