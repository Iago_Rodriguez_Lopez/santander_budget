class CreateCostCenters < ActiveRecord::Migration
  def change
    create_table :cost_centers do |t|
      t.string :code
      t.string :name
      t.string :enterprise
      t.string :division
      t.string :department
      t.string :area
      t.string :subarea
      t.string :responsable

      t.timestamps
    end
  end
end
