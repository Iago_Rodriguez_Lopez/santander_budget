class AddItemCodeItemNameToBudget < ActiveRecord::Migration
  def change
    add_column :budgets, :item_code, :string
    add_column :budgets, :item_name, :string
  end
end
