class AddPercentageToTheoricAdjustment < ActiveRecord::Migration
  def change
    add_column :theoric_adjustments, :kind, :string
    add_column :theoric_adjustments, :percentage, :decimal
  end
end
