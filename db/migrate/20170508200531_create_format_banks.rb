class CreateFormatBanks < ActiveRecord::Migration
  def change
    create_table :format_banks do |t|
      t.string :name

      t.timestamps
    end
  end
end
