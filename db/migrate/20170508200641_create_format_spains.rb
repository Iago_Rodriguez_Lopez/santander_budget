class CreateFormatSpains < ActiveRecord::Migration
  def change
    create_table :format_spains do |t|
      t.string :name

      t.timestamps
    end
  end
end
