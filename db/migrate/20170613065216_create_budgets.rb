class CreateBudgets < ActiveRecord::Migration
  def change
    create_table :budgets do |t|
      t.date :period
      t.integer :month
      t.integer :year

      t.timestamps
    end
  end
end
