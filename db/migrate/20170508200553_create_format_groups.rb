class CreateFormatGroups < ActiveRecord::Migration
  def change
    create_table :format_groups do |t|
      t.string :name

      t.timestamps
    end
  end
end
