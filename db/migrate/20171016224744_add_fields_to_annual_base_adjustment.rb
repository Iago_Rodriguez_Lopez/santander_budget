class AddFieldsToAnnualBaseAdjustment < ActiveRecord::Migration
  def change
    add_reference :annual_base_adjustments, :subfield, index: true, foreign_key: true
    add_reference :annual_base_adjustments, :item, index: true, foreign_key: true
  end
end
