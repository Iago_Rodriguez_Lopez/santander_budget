class AddReferenceAccountToTransaction < ActiveRecord::Migration
  def change
    change_table(:transactions) do |t|
      t.references(:account)
    end
  end
end
