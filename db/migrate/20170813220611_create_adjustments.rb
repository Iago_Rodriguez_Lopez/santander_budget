class CreateAdjustments < ActiveRecord::Migration
  def change
    create_table :adjustments do |t|
      t.string :adjustment_type
      t.string :name
      t.float :amount
      t.datetime :date
      t.references :item
      t.references :account
      t.timestamps
    end
  end
end
