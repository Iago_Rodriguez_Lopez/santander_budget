class CreateInterpretationCargabals < ActiveRecord::Migration
  def change
    create_table :interpretation_cargabals do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
