class CreateMonthPlanifications < ActiveRecord::Migration
  def change
    create_table :month_planifications do |t|
      t.string :name
      t.integer :year
      t.integer :month

      t.timestamps null: false
    end
  end
end
