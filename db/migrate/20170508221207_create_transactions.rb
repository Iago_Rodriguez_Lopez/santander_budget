class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.references :item, foreign_key: true
      t.references :cost_center, foreign_key: true
      t.float :amount
      t.string :kind

      t.timestamps
    end
  end
end
