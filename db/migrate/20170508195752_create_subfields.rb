class CreateSubfields < ActiveRecord::Migration
  def change
    create_table :subfields do |t|
      t.string :name

      t.timestamps
    end
  end
end
