class CreateTheoricAnnualAdjustments < ActiveRecord::Migration
  def change
    create_table :theoric_annual_adjustments do |t|
      t.string :name
      t.float :amount
      t.references :annual_base_adjustment, index: true, foreign_key: true
      t.datetime :forescat_date

      t.timestamps null: false
    end
  end
end
