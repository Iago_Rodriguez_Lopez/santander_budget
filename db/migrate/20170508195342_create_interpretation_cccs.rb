class CreateInterpretationCccs < ActiveRecord::Migration
  def change
    create_table :interpretation_cccs do |t|
      t.string :code
      t.string :name

      t.timestamps
    end
  end
end
