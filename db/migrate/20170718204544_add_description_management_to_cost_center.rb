class AddDescriptionManagementToCostCenter < ActiveRecord::Migration
  def change
    add_column :cost_centers, :description, :string
    add_column :cost_centers, :management, :string
  end
end
