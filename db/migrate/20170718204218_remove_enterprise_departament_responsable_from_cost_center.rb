class RemoveEnterpriseDepartamentResponsableFromCostCenter < ActiveRecord::Migration
  def change
    remove_column :cost_centers, :enterprise, :string
    remove_column :cost_centers, :department, :string
    remove_column :cost_centers, :responsable, :string
  end
end
