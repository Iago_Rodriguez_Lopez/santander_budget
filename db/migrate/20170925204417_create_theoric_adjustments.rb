class CreateTheoricAdjustments < ActiveRecord::Migration
  def change
    create_table :theoric_adjustments do |t|
      t.string :name
      t.float :amount
      t.references :base_adjustment
      t.references :item
      t.timestamps null: false
    end
  end
end
