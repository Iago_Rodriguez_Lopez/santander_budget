class CreateEstimates < ActiveRecord::Migration
  def change
    create_table :estimates do |t|
      t.string :estimates_type
      t.string :name
      t.float :amount
      t.date :date
      t.references :item
      t.references :account
      t.timestamps
    end
  end
end
