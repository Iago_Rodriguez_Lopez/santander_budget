class CreateBudgetReport < ActiveRecord::Migration
  def up
    self.connection.execute %Q(
      CREATE OR REPLACE VIEW budgets_report
      AS
      SELECT
      EXTRACT(YEAR FROM budgets.period) as "year",
      subfields.id as "subfield_id",
      subfields.name as "subfield_name",
      items.id as "item_id",
      items.code as "item_code",
      items.description as "item_description",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 1.0 THEN budgets.amount ELSE 0 END) AS "january_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 2.0 THEN budgets.amount ELSE 0 END) AS "february_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 3.0 THEN budgets.amount ELSE 0 END) AS "march_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 4.0 THEN budgets.amount ELSE 0 END) AS "april_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 5.0 THEN budgets.amount ELSE 0 END) AS "may_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 6.0 THEN budgets.amount ELSE 0 END) AS "june_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 7.0 THEN budgets.amount ELSE 0 END) AS "july_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 8.0 THEN budgets.amount ELSE 0 END) AS "august_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 9.0 THEN budgets.amount ELSE 0 END) AS "september_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 10.0 THEN budgets.amount ELSE 0 END) AS "october_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 11.0 THEN budgets.amount ELSE 0 END) AS "november_budget",
      SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 12.0 THEN budgets.amount ELSE 0 END) AS "december_budget",
      SUM(budgets.amount) AS "year_budget"
      FROM budgets
      INNER JOIN items ON items.id = budgets.item_id
      INNER JOIN subfields ON subfields.id = items.subfield_id
      GROUP BY
      EXTRACT(YEAR FROM budgets.period),
      subfields.id,
      subfields.name,
      items.id,
      items.code,
      items.description
      ORDER BY
      EXTRACT(YEAR FROM budgets.period),
      subfields.id,
      subfields.name,
      items.id,
      items.code,
      items.description;
    )
  end

  def down
    self.connection.execute "DROP VIEW IF EXISTS budgets_report;"
  end
end
