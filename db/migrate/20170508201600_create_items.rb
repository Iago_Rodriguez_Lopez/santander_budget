class CreateItems < ActiveRecord::Migration
  def change
    create_table :items do |t|
      t.string :code
      t.string :description
      t.belongs_to :account, index: { unique: true }, foreign_key: true
      t.references :interpretation_ccc, foreign_key: true
      t.references :interpretation_cargabal, foreign_key: true
      t.references :interpretation_ge, foreign_key: true
      t.references :subfield, foreign_key: true
      t.references :format_bank, foreign_key: true
      t.references :format_group, foreign_key: true
      t.references :format_spain, foreign_key: true

      t.timestamps
    end
  end
end
