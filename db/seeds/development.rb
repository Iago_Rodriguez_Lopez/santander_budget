require 'csv'

#Users
ruts_path = [Rails.root,'db','csv_data','chilean_ids.csv'].join("/")
csv_ruts = CSV.read(ruts_path,headers: true)

interpretation_ccc = []
interpretation_ges = []
format_spains = []
subfields = []
interpretation_cargabals = []
format_banks = []
format_groups = []
accounts = []
cost_centers = []
month_planification = []
annual_planifications = []
annual_base_adjustments = []


if ENV["users"] == 'true' || ENV["all"] == 'true'
  puts "Loading users..."
  50.times do |index|
    full_name = "User #{index}".split(" ")
    name = full_name[0]
    last_name = full_name[1]
    rut_data = csv_ruts[index].to_s.split("-")
    User.create!(name:name,last_name:last_name,rut:rut_data[0],dv:rut_data[1].strip)
  end
  puts "Users loaded"
end

if ENV["base_data"] == 'true' || ENV["all"] == 'true'
  puts "Loading base data..."
  interpretation_ccc << InterpretationCcc.create!(name:"SUELDOS Y SALARIOS",code:"86")
  interpretation_ccc << InterpretationCcc.create!(name:"SEGUROS DE SALUD",code:"95")

  interpretation_ges << InterpretationGe.create(name:"Salario",code:"72000")
  interpretation_ges << InterpretationGe.create(name:"Beneficios Sociales",code:"76100")

  format_spains << FormatSpain.create!(name:"Sueldo fijo")
  format_spains << FormatSpain.create!(name:"Sueldo Variable")

  subfields << Subfield.create!(name:"Remuneraciones")
  subfields << Subfield.create!(name:"Otras Rem. Imponib. Tribut")

  interpretation_cargabals << InterpretationCargabal.create!(code: "43000",name: "Retribuciones fijas")

  format_banks << FormatBank.create!(name:"remuneración permanente")
  format_banks << FormatBank.create!(name:"otras Remuneraciones")

  format_groups << FormatGroup.create!(name:"remuneración permanente")
  format_groups << FormatGroup.create!(name:"Recuperación subsidios médicos")

  items = []
  items << Item.create!(code:'T0101000000',description:'Sueldo Base',interpretation_ccc_id:interpretation_ccc[0].id,
    interpretation_cargabal_id:interpretation_cargabals[0].id,interpretation_ge_id:interpretation_ges[0].id,subfield_id:subfields[0].id,
    format_bank_id:format_banks[0].id,format_group_id: format_groups[0].id,format_spain_id:format_spains[0].id)

  items << Item.create!(code:'T0101010000',description:'Recuperación subsidios médicos',interpretation_ccc_id:interpretation_ccc[1].id,
    interpretation_cargabal_id:interpretation_cargabals[0].id,interpretation_ge_id:interpretation_ges[1].id,subfield_id:subfields[1].id,
    format_bank_id:format_banks[1].id,format_group_id: format_groups[1].id,format_spain_id:format_spains[1].id)

  items << Item.create!(code:'T0102000000',description:'Gratificación Legal',interpretation_ccc_id:interpretation_ccc[0].id,
    interpretation_cargabal_id:interpretation_cargabals[0].id,interpretation_ge_id:interpretation_ges[0].id,subfield_id:subfields[0].id,
    format_bank_id:format_banks[0].id,format_group_id: format_groups[0].id,format_spain_id:format_spains[0].id)

  items << Item.create!(code:'T0103030000',description:'Asig. Responsabilidad',interpretation_ccc_id:interpretation_ccc[1].id,
    interpretation_cargabal_id:interpretation_cargabals[0].id,interpretation_ge_id:interpretation_ges[1].id,subfield_id:subfields[1].id,
    format_bank_id:format_banks[1].id,format_group_id: format_groups[1].id,format_spain_id:format_spains[1].id)

  accounts << Account.create!(name:'REMUNERACIONES',code:'3126101010',description:'REMUNERACIONES',source:'Banco',item_id:items[0].id)
  accounts << Account.create!(name:'RECUP. SUBSIDIOS MEDICOS',code:'3126101035',description:'RECUP. SUBSIDIOS MEDICOS',source:'Banco',item_id:items[1].id)
  accounts << Account.create!(name:'GRATIFICACION LEGAL',code:'3126101020',description:'GRATIFICACION LEGAL',source:'Banco',item_id:items[2].id)
  accounts << Account.create!(name:'BONO ASIGNACION RESPONSABILIDAD',code:'3126101050',description:'BONO ASIGNACION RESPONSABILIDAD',source:'Banco',item_id:items[3].id)
  accounts << Account.create!(name:'OTRAS REMUNERACIONES IMP.',code:'3126101037',description:'OTRAS REMUNERACIONES IMP.',source:'Banco',item_id:items[0].id)
  accounts << Account.create!(name:'BONO ESCOLAR',code:'3126105070',description:'BONO ESCOLAR',source:'Banco',item_id:items[1].id)
  accounts << Account.create!(name:'ALMUERZO PERSONAL',code:'3126105030',description:'ALMUERZO PERSONAL',source:'Banco',item_id:items[2].id)
  accounts << Account.create!(name:'BONO MATRIMONIO',code:'3126105050',description:'BONO MATRIMONIO',source:'Banco',item_id:items[0].id)

  cost_centers << CostCenter.create!(code:'6205262050',name:'SUELDOS',division:"DIVISION PERSONAS Y COMUNICACION" ,management: "GESTION DE PERSONAS", area:"CONSULTORIA Y GESTION PERSONAS" )
  cost_centers << CostCenter.create!(code:'6205262051',name:'SUBSIDIOS',division:"DIVISION PERSONAS Y COMUNICACION" ,management: "GESTION DE PERSONAS", area:"CONSULTORIA Y GESTION PERSONAS" , subarea:"CENTRO DE ATENCION VIRTUAL" )
  cost_centers << CostCenter.create!(code:'6205262052',name:'REMUNERACIONES',division:"DIVISION PERSONAS Y COMUNICACION" ,management: "GESTION DE PERSONAS", area:"BP REDES Y CANALES")
  cost_centers << CostCenter.create!(code:'6220262030',name:'BONO INCENTIVOS',division:"DIVISION COMERCIAL" ,management: "RED BANCO", area:"TRAINEE BANCA PERSONAS.")

  amount_of_years = 2
  start_year = 2016
  monthly_budgets_by_item = [100,100,100,100,100,300,300,300,300,300]

  #Year 2016
  items.each_with_index do |item,index_item|
    amount_of_years.times do |year_index|
      year = start_year + year_index
      12.times do |index|
        month = index + 1
        amount = if year == start_year
                    monthly_budgets_by_item[index_item]
                  else
                    monthly_budgets_by_item[index_item+5]
                  end
        Budget.create!(year:year, month:month,period:Date.new(year,month,1),amount:amount,item_id: item.id)
      end
    end
  end

  transactions_by_month = [100,-100,100,-100,100,-100,200,-200,200,-200,200,-200]

  #Year 2017
  accounts.each do |account|
    amount_of_years.times do |year_index|
      year = start_year + year_index
      12.times do |index|
        month = index + 1
        cost_center_random = cost_centers[index%4]
        transaction_amount = transactions_by_month[index] * (year%2+1)
        Transaction.create!(source:'Banco',expired_date:Date.new(year,month,1),amount:transaction_amount,account_id:account.id,cost_center_id: cost_center_random.id)
      end
    end
  end
  puts "Base data loaded"
end

if ENV["month_planification"] == 'true' || ENV["all"] == 'true'
  puts "Loading month planification data"
  month_planification << MonthPlanification.create!(name:"Octubre",year:2017,month:10)
  month_planification << MonthPlanification.create!(name:"Noviembre",year:2017,month:11)

  puts "Month planification data loaded"
end

if ENV["annual_planification"] == 'true' || ENV["all"] == 'true'
  puts "Loading annual planification data"
  annual_planifications << AnnualPlanification.create!(year:2017,pivot_month:9,name: "Version 01")
  annual_planifications << AnnualPlanification.create!(year:2017,pivot_month:10,name: "Version 01")

  annual_planifications.each do |annual_planification|
    subfields.each_with_index do |subfield,subfield_index|
      items.each_with_index do |item,item_index|
        base_type = AnnualBaseAdjustment::BASE_ADJUSTMENTS_TYPE[:budget_previous_month]

        annual_base_adjustments << AnnualBaseAdjustment.create!(name:"base adjusment 01",base_type:base_type,amount:2000,annual_planification_id:annual_planification.id,subfield_id:subfield.id,item_id:item.id)
      end
    end
  end

  #Add the adjustment for each base after created them
  annual_base_adjustments.each do |annual_base_adjustment|

      year = annual_base_adjustment.annual_planification.year
      start_month = annual_base_adjustment.annual_planification.pivot_month

      amount__adjustments = (12 - start_month + 1)

      amount__adjustments.times do |index|

        kind = index%2 == 0 ? 'numeric' : 'percentage'
        decision_kind_value = kind == 'numeric'

        percentage = decision_kind_value ? nil : rand().round(2)
        amount = decision_kind_value ? 100 : annual_base_adjustment.amount * percentage

        month = start_month + index
        forcasted_date = DateTime.new(year,month,1)
        TheoricAnnualAdjustment.create!(name:"Ajuste 01",amount:amount, forescat_date:forcasted_date, percentage:percentage,kind:kind,annual_base_adjustment_id:annual_base_adjustment.id)
        TheoricAnnualAdjustment.create!(name:"Ajuste 02",amount:amount, forescat_date:forcasted_date, percentage:percentage,kind:kind,annual_base_adjustment_id:annual_base_adjustment.id)
      end
  end

end