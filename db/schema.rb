# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20171026201554) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "accounts", force: :cascade do |t|
    t.string   "name"
    t.string   "code"
    t.string   "description"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "source"
    t.string   "item_code"
    t.integer  "item_id"
  end

  add_index "accounts", ["item_id"], name: "index_accounts_on_item_id", using: :btree

  create_table "adjustments", force: :cascade do |t|
    t.string   "adjustment_type"
    t.string   "name"
    t.float    "amount"
    t.datetime "date"
    t.integer  "item_id"
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "annual_base_adjustments", force: :cascade do |t|
    t.string   "name"
    t.string   "base_type"
    t.float    "amount"
    t.integer  "annual_planification_id"
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.integer  "subfield_id"
    t.integer  "item_id"
  end

  add_index "annual_base_adjustments", ["annual_planification_id"], name: "index_annual_base_adjustments_on_annual_planification_id", using: :btree
  add_index "annual_base_adjustments", ["item_id"], name: "index_annual_base_adjustments_on_item_id", using: :btree
  add_index "annual_base_adjustments", ["subfield_id"], name: "index_annual_base_adjustments_on_subfield_id", using: :btree

  create_table "annual_planifications", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.integer  "pivot_month"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "base_adjustments", force: :cascade do |t|
    t.string   "name"
    t.string   "base_type"
    t.float    "amount"
    t.integer  "month_planification_id"
    t.integer  "subfield_id"
    t.integer  "item_id"
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "budgets", force: :cascade do |t|
    t.date     "period"
    t.integer  "month"
    t.integer  "year"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "amount"
    t.integer  "item_id"
  end

  add_index "budgets", ["item_id"], name: "index_budgets_on_item_id", using: :btree

  create_table "cost_centers", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.string   "division"
    t.string   "area"
    t.string   "subarea"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "description"
    t.string   "management"
  end

  create_table "estimates", force: :cascade do |t|
    t.string   "estimates_type"
    t.string   "name"
    t.float    "amount"
    t.date     "date"
    t.integer  "item_id"
    t.integer  "account_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "format_banks", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "format_groups", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "format_spains", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interpretation_cargabals", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interpretation_cccs", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "interpretation_ges", force: :cascade do |t|
    t.string   "code"
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "items", force: :cascade do |t|
    t.string   "code"
    t.string   "description"
    t.integer  "interpretation_ccc_id"
    t.integer  "interpretation_cargabal_id"
    t.integer  "interpretation_ge_id"
    t.integer  "subfield_id"
    t.integer  "format_bank_id"
    t.integer  "format_group_id"
    t.integer  "format_spain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "month_planifications", force: :cascade do |t|
    t.string   "name"
    t.integer  "year"
    t.integer  "month"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "subfields", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "theoric_adjustments", force: :cascade do |t|
    t.string   "name"
    t.float    "amount"
    t.integer  "base_adjustment_id"
    t.integer  "item_id"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.string   "kind"
    t.decimal  "percentage"
  end

  create_table "theoric_annual_adjustments", force: :cascade do |t|
    t.string   "name"
    t.float    "amount"
    t.integer  "annual_base_adjustment_id"
    t.datetime "forescat_date"
    t.datetime "created_at",                null: false
    t.datetime "updated_at",                null: false
    t.string   "kind"
    t.decimal  "percentage"
    t.integer  "group"
  end

  add_index "theoric_annual_adjustments", ["annual_base_adjustment_id"], name: "index_theoric_annual_adjustments_on_annual_base_adjustment_id", using: :btree

  create_table "transactions", force: :cascade do |t|
    t.integer  "cost_center_id"
    t.float    "amount"
    t.string   "source"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "expired_date"
    t.datetime "document_date"
    t.text     "comment"
    t.integer  "account_id"
    t.string   "enterprise_code"
    t.string   "enterprise_name"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name"
    t.string   "last_name"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "age"
    t.string   "sex"
    t.integer  "rut"
    t.string   "dv"
    t.string   "cost_center"
    t.string   "department"
    t.string   "division"
    t.string   "area"
    t.string   "subarea"
  end

  add_foreign_key "accounts", "items"
  add_foreign_key "annual_base_adjustments", "annual_planifications"
  add_foreign_key "annual_base_adjustments", "items"
  add_foreign_key "annual_base_adjustments", "subfields"
  add_foreign_key "budgets", "items"
  add_foreign_key "items", "format_banks"
  add_foreign_key "items", "format_groups"
  add_foreign_key "items", "format_spains"
  add_foreign_key "items", "interpretation_cargabals"
  add_foreign_key "items", "interpretation_cccs"
  add_foreign_key "items", "interpretation_ges"
  add_foreign_key "items", "subfields"
  add_foreign_key "theoric_annual_adjustments", "annual_base_adjustments"
  add_foreign_key "transactions", "cost_centers"
end
