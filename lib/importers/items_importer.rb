require_relative 'data_importer'

class ItemsImporter < DataImporter
  
  def import_data(args = {})
    logger_path = Rails.root.join('lib','importers','log',"#{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}_items_importer.log")
    logger = Logger.new(logger_path)

    @data.each_with_index do |row_data,index|
      Item.transaction do

        item_code = row_data[0].to_s.strip
        item_name = row_data[1].to_s.strip

        subfield_name = row_data[2].to_s.strip
        
        interpretation_ccc_code = row_data[3].to_i.to_s.strip
        interpretation_ccc_name = row_data[4].to_s.strip
        
        interpretation_ges_code = row_data[5].to_i.to_s.strip
        interpretation_ges_name = row_data[6].to_s.strip
        
        interpretation_cargabal_code = row_data[7].to_i.to_s.strip
        interpretation_cargabal_name = row_data[8].to_s.strip

        format_spain_name = row_data[9].to_s.strip

        format_group_name = row_data[10].to_s.strip

        format_bank_name = row_data[11].to_s.strip

        variables = {code:item_code,
                    name:item_name,
                    subfield_name:subfield_name,
                    interpretation_ccc_name:interpretation_ccc_name,
                    interpretation_ges_name:interpretation_ges_name,
                    interpretation_cargabal_name:interpretation_cargabal_name,
                    format_spain:format_spain_name,
                    format_group:format_group_name,
                    format_bank:format_bank_name}

        begin
          
          subfield = Subfield.where(name: subfield_name).first_or_create!
          raise Exception.new("The subfield #{subfield_name} cann't be created") unless subfield.present?
          
          interpretation_ccc = InterpretationCcc.where(code:interpretation_ccc_code,name:interpretation_ccc_name).first_or_create!
          raise Exception.new("The interpretation CCC #{interpretation_ccc_code} cann't be created") unless interpretation_ccc.present?

          interpretation_ges = InterpretationGe.where(code:interpretation_ges_code,name:interpretation_ges_name).first_or_create!
          raise Exception.new("The interpretation GES #{interpretation_ges_code} cann't be created") unless interpretation_ges.present?
          
          interpretation_cargabal = InterpretationCargabal.where(code:interpretation_cargabal_code,name:interpretation_cargabal_name).first_or_create!
          raise Exception.new("The interpretation CARGABAL #{interpretation_cargabal_code} cann't be created") unless interpretation_cargabal.present?

          format_spain = FormatSpain.where(name: format_spain_name).first_or_create!
          raise Exception.new("The format spain #{format_spain_name} cann't be created") unless format_spain.present?
          
          format_group = FormatGroup.where(name: format_spain_name).first_or_create!
          raise Exception.new("The format group #{format_group_name} cann't be created") unless format_group.present?

          format_bank = FormatBank.where(name: format_spain_name).first_or_create!
          raise Exception.new("The format group #{format_group_name} cann't be created") unless format_bank.present?

          Item.create!({
                        code:item_code,
                        description:item_name,
                        interpretation_ccc_id:interpretation_ccc.id,
                        interpretation_cargabal_id:interpretation_cargabal.id,
                        interpretation_ge_id:interpretation_ges.id,
                        subfield_id:subfield.id,
                        format_bank_id:format_bank.id,
                        format_group_id:format_group.id,
                        format_spain_id:format_spain.id,
                      })

        rescue Exception => e
          logger.error e.message
          logger.error "#{variables}"
        end

      end
    end
    
  end
end