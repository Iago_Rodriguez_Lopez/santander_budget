require_relative 'data_importer'

class CostCenterImporter < DataImporter
  
  def import_data(args = {})
    logger_path = Rails.root.join('lib','importers','log',"#{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}_cost_centers_importer.log")
    logger = Logger.new(logger_path)

    @data.each_with_index do |row_data,index|
      CostCenter.transaction do

        cost_center_params = {code: row_data[0],
                    name: row_data[1],
                    description: row_data[2],
                    management: row_data[3],
                    division: row_data[4],
                    area: row_data[5],
                    subarea: row_data[6]
                    }
        
        begin
          cost_center = CostCenter.create!(cost_center_params)
        rescue Exception => e
          logger.error e.message
          logger.error "#{cost_center_params}"
        end

      end
    end
    
  end
end