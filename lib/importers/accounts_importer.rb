require_relative 'data_importer'

class AccountsImporter < DataImporter
  
  def import_data(args = {})
    logger_path = Rails.root.join('lib','importers','log',"#{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}_accounts_importer.log")
    logger = Logger.new(logger_path)

    @data.each_with_index do |row_data,index|
      Account.transaction do

        account_params = {code:row_data[0],
                    name:row_data[1],
                    description:row_data[2],
                    source:row_data[3],
                    }

        begin
          item_code = row_data[4].to_s
          item = Item.find_by_code(item_code)
          raise Exception.new("The item #{item_code} doesn't exist") unless item.present?
          

          account = Account.create!(
                    account_params.merge({item_code:item_code,item_id:item.id})
                    )

        rescue Exception => e
          logger.error e.message
          logger.error "#{account_params}"
        end

      end
    end
    
  end
end