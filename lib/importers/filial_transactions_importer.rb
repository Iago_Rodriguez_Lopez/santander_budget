require_relative 'data_importer'

class FilialTrasanctionsImporter < DataImporter
  
  def import_data(args = {})
    logger_path = Rails.root.join('lib','importers','log',"#{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}_filial_transactions_2017.log")
    logger = Logger.new(logger_path)

    filial_transactions_failed_path = Rails.root.join('db','csv_data',"#{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}_filial_transactions_import_details.csv")
          
    CSV.open(filial_transactions_failed_path, 'w') do |csv_object|          
      csv_object << ['account_code','item_code','amount','expired_date','cost_center','description','account_exits?','consistency_account_item?','cost_center_exits?']

      @data.each_with_index do |row_data,index|
        Transaction.transaction do
          begin

            account_code = row_data[0].to_s
            item_code = row_data[1].to_s

            expired_date = DateTime.strptime(row_data[2], "%d-%m-%Y %H:%M:%S")
            
            amount = row_data[3].include?(',') ? row_data[3].to_s.gsub!(',','') : row_data[3].to_s
            
            cost_center_code = row_data[4].to_s

            comment = row_data[5].to_s

            enterprise_name = row_data[6].to_s
            enterprise_code = row_data[7].to_s

            account = Account.find_by_code(account_code)
            cost_center = CostCenter.find_by_code(cost_center_code)

            
            transaction_params = {}

            transaction_params[:cost_center_id] = cost_center.id if cost_center.present?
            transaction_params[:amount] = amount.to_f
            transaction_params[:source] = 'Filial'
            transaction_params[:expired_date] = expired_date
            transaction_params[:comment] = comment
            transaction_params[:account_id] = account.id if account.present?
            transaction_params[:enterprise_code] = enterprise_code if enterprise_code.present?
            transaction_params[:enterprise_name] = enterprise_name if enterprise_name.present?

            transaction = Transaction.create(transaction_params)
            
            #Details for the csv file report
            details = [account_code,item_code,amount,expired_date,cost_center_code,comment,account.present?.to_s, account.present? ? account.item.code == item_code : '-', cost_center.present? ]

            csv_object << details
            puts "Processing #{index} - #{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}" if index % 1000 == 0
          rescue Exception => e
            logger.error e.message
          end 
        end
      end

    end
    


  end
end