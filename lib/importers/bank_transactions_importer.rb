require_relative 'data_importer'

class BankTrasanctionsImporter < DataImporter
  
  def import_data(args = {})
    logger_path = Rails.root.join('lib','importers','log',"#{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}_bank_transactions_2017.log")
    logger = Logger.new(logger_path)
    bank_transactions_failed_path = Rails.root.join('db','csv_data',"#{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}_bank_transactions_import_details.csv")
          
    CSV.open(bank_transactions_failed_path, 'w') do |csv_object|
      csv_object << ['account_code','item_code','amount','expired_date','document_date','cost_center','description','account_exits?','consistency_account_item?','cost_center_exits?','correct_format_amount?']
            
      @data.each_with_index do |row_data,index|
        Transaction.transaction do
          begin
            account_code = row_data[0].to_s.strip
            item_code = row_data[1].to_s.strip

            amount = row_data[2].include?(',') ? row_data[2].to_s.gsub!(',','') : row_data[2].to_s

            expired_date = DateTime.strptime(row_data[3], "%d/%m/%Y")
            document_date = DateTime.strptime(row_data[4], "%d/%m/%Y")
            
            cost_center_code = row_data[5].to_s.strip

            comment = row_data[6].to_s.strip

            account = Account.find_by_code(account_code)
            cost_center = CostCenter.find_by_code(cost_center_code)
            transaction_params = {}

            transaction_params[:cost_center_id] = cost_center.id if cost_center.present?
            transaction_params[:amount] = amount.to_f #TODO: Change importer to elimanate dots and commas in numbers
            transaction_params[:source] = 'Banco'
            transaction_params[:expired_date] = expired_date
            transaction_params[:document_date] = document_date
            transaction_params[:comment] = comment
            
            transaction_params[:account_id] = account.id if account.present?
            
            transaction = Transaction.create(transaction_params)

            #Details transactions
            details = [account_code,item_code,amount,expired_date.strftime("%d/%m/%Y"),document_date.strftime("%d/%m/%Y"),cost_center_code,comment]
            details.push(account.present?.to_s)
            details.push(account.try(:item).try(:code) == item_code)
            details.push(cost_center.present?.to_s)
            details.push(row_data[2].include?(',').to_s)

            csv_object << details
              
            puts "Processing #{index} - #{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}" if index % 1000 == 0
          rescue Exception => e
            logger.error e.message
          end 
        end
      end
    end


  end
end