require_relative 'accounts_importer'
require_relative 'bank_transactions_importer'
require_relative 'budget_importer'
require_relative 'cost_center_importer'
require_relative 'filial_transactions_importer'
require_relative 'items_importer'


class ImporterManager

  def initialize(paths)
    @importers = {}

    @importers[:account_importer] = AccountsImporter.new(paths[:accounts_path]) if paths[:accounts_path].present?
    @importers[:bank_importer] = BankTrasanctionsImporter.new(paths[:bank_transactions_path]) if paths[:bank_transactions_path].present?
    @importers[:budget_importer] = BudgetImporter.new(paths[:budget_path]) if paths[:budget_path].present?
    @importers[:cost_center_importer] = CostCenterImporter.new(paths[:cost_center_path]) if paths[:cost_center_path].present?
    @importers[:filial_importer] = FilialTrasanctionsImporter.new(paths[:filial_transactions_path]) if paths[:filial_transactions_path].present?
    @importers[:item_importer] = ItemsImporter.new(paths[:items_path]) if paths[:items_path].present?
  end

  def import_all
    @importers[:item_importer].import_data
    @importers[:account_importer].import_data
    @importers[:cost_center_importer].import_data
    @importers[:bank_importer].import_data
    @importers[:filial_importer].import_data
    @importers[:budget_importer].import_data
  end

  def import_data_of(importer_key,args = {})
    puts "Import data of #{importer_key} initiated"
    @importers[importer_key.to_sym].import_data(args)
    puts "Import data of #{importer_key} finished"
  end

  def import_data_with(importer_key,args = {})
    if args.has_key?(:path)
      importer = @importers[importer_key.to_sym]
      
      new_data_path = args[:path]
      importer.set_new_data(new_data_path)

      puts "Import data of #{importer_key} initiated"
      importer.import_data(args)
      puts "Import data of #{importer_key} finished"
    end
  end
end

base_path = Rails.root.join('db','csv_data')

paths = {
  items_path: base_path.join('items.csv'),
  accounts_path: base_path.join('accounts.csv'),
  cost_center_path: base_path.join('cost_centers.csv'),
  bank_transactions_path: base_path.join('bank_transactions_2017.csv'),
  filial_transactions_path: base_path.join('filial_transactions_2017.csv'),
  budget_path: base_path.join('budget_2017.csv')
}

manager = ImporterManager.new(paths)

manager.import_data_of('item_importer')
manager.import_data_of('account_importer')
manager.import_data_of('cost_center_importer')
manager.import_data_of('bank_importer')
manager.import_data_of('filial_importer')
manager.import_data_of('budget_importer',{year:2017})

budget_2016_path = base_path.join('budget_2016.csv')
manager.import_data_with('budget_importer',{year:2016,path:budget_2016_path})

filial_2016_path = base_path.join('transactions_filiales_2016.csv')
manager.import_data_with('filial_importer',{path:filial_2016_path,year:2016})

bank_2016_path = base_path.join('transactions_bank_2016.csv')
manager.import_data_with('bank_importer',{path:bank_2016_path,year:2016})


