require_relative 'data_importer'

class BudgetImporter < DataImporter
  
  def import_data(args = {})
    
    y = args.has_key?(:year) ? args[:year].to_i : 2017

    logger_path = Rails.root.join('lib','importers','log',"#{DateTime.now.strftime('%Y%m%d%H%M%S').to_s}_budget_#{y}_importer.log")
    logger = Logger.new(logger_path)

    @data.each_with_index do |row_data,index|

      Budget.transaction do
        variables = {}
        begin
          item_code = row_data[0].to_s
          item = Item.find_by_code(item_code)
          
          raise Exception.new("The item #{item_code} doesn't exist") unless item.present?

          initial_month = 1
          final_month = 12

          (final_month - initial_month + 1).times do |index|
            
            row_index = index + 2
            month = initial_month + index
            amount = row_data[row_index].include?(',') ? row_data[row_index].to_s.gsub!(',','').to_f : row_data[row_index].to_f
            period = Date.new(y.to_i,month,1)

            variables[:period] = period
            variables[:month] = month
            variables[:year] = y
            variables[:amount] = amount 
            variables[:item_code] = item_code

            Budget.create!(period:period, month:month, year:y, amount:amount, item_id:item.id)
          end
          
        rescue Exception => e
          logger.error e.message
          logger.error "#{variables}"
        end
      end
    end
    
  end
end