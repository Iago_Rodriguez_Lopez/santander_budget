require 'csv'

class DataImporter

  def initialize(path_file='')
    if path_file != ''
      @path_file = path_file
      @data = CSV.read(path_file, { :headers => true })
    end
  end

  def import_data(args = {})
  end

  def set_files_path(pattern)
    @file_paths = Dir[pattern]
  end

  def process_multiple_files
  end

  def set_new_data(path_file='')
    if path_file != ''
      @path_file = path_file
      @data = CSV.read(path_file, { :headers => true })
    end
  end

end
