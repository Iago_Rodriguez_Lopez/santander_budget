module BaseCalculations
 def previous_spending(year,month,subfield)
   reference_date = DateTime.new(year,month,1)
   start_date = (reference_date - 1.month).beginning_of_month

   end_date = start_date.end_of_month
   sql = p %{
			SELECT
      items.id as item_id,
      items.code as item_code,
      items.description as item_description,
      SUM(transactions.amount) AS amount
			FROM transactions
			INNER JOIN accounts ON accounts.id = transactions.account_id
			INNER JOIN items ON items.id = accounts.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
      GROUP BY items.id
      ORDER BY items.code
    }.gsub(/\s+/, " ").strip

    execute_pg_query(sql)
 end

 def previous_budget(year,month,subfield)
   reference_date = Date.new(year,month,1)
   start_date = (reference_date - 1.month).beginning_of_month
   end_date = start_date.end_of_month

   sql = p %{
			SELECT
      items.id as item_id,
      items.code as item_code,
      items.description as item_description,
      SUM(budgets.amount) AS amount
			FROM budgets
			INNER JOIN items ON items.id = budgets.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
      GROUP BY items.id
      ORDER BY items.code
      }.gsub(/\s+/, " ").strip

   execute_pg_query(sql)
 end

 def monthly_average_expenditure(year,month,subfield)
   reference_date = DateTime.new(year,month,1)
   start_date = DateTime.new(year,1,1)

   end_date = reference_date.end_of_month
   sql = p %{
			SELECT
      items.id as item_id,
      items.code as item_code,
      items.description as item_description,
      SUM(transactions.amount) AS total_amount,
      (cast(SUM(transactions.amount) as decimal)/ date_part('month', to_date('#{reference_date}','YYYY-MM-DD'))) as amount
			FROM transactions
			INNER JOIN accounts ON accounts.id = transactions.account_id
			INNER JOIN items ON items.id = accounts.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
      GROUP BY items.id
      ORDER BY items.code
    }.gsub(/\s+/, " ").strip

    execute_pg_query(sql)
 end

 def monthtly_average_expenditure_for_subfield(year,month,subfield)
   reference_date = DateTime.new(year,month,1)
   start_date = DateTime.new(year,1,1)

   end_date = reference_date.end_of_month
   sql = p %{
			SELECT
      SUM(transactions.amount) AS total_amount,
      (cast(SUM(transactions.amount) as decimal)/ date_part('month', to_date('#{reference_date}','YYYY-MM-DD'))) as monthly_average
			FROM transactions
			INNER JOIN accounts ON accounts.id = transactions.account_id
			INNER JOIN items ON items.id = accounts.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
    }.gsub(/\s+/, " ").strip

   result = execute_pg_query(sql)

   return result[0].present? ? result[0]['monthly_average'].to_d.round(2) : nil
 end

 def new_values_by_item(base_type,year,month,subfield)

 end


 def execute_pg_query(query)
   #Source: https://stackoverflow.com/questions/25331778/getting-typed-results-from-activerecord-raw-sql
   #Answer by : Ramfjord
   conn = ActiveRecord::Base.connection
   @type_map ||= PG::BasicTypeMapForResults.new(conn.raw_connection)

   res = conn.execute(query)
   res.type_map = @type_map
   res
 end
end