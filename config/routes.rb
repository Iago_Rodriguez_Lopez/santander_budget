Rails.application.routes.draw do

  namespace :reports do
    get 'control'
    get 'consultation'


    post 'control'
    post 'consultation'

    post 'year_report'
    post 'month_report'

    get 'detailed_control'
    post 'detailed_control'

  end

  namespace :planification do
    #TODO: Adjust request type 
    get 'monthly'
    post 'monthly'

    get 'change_version_monthly'
    get 'change_analysis_version_name'
    get 'new_monthly_budget_modal'

    post 'new_monthly_budget_version'

    get 'open_adjustments_form_modal'
    get 'open_base_adjustments_modal'
    post 'create_budget_base'
    post 'new_monthly_base'
    get 'process_quater_details'
    get 'control_report'

    get 'adjust_monthly_base'
    patch 'process_base_adjustment'

    post 'new_item_adjustment'
    post 'delete_item_adjustment'
    get 'change_base_type'
    get 'change_adjustment_amount'
  end

  namespace :annual_planification do

    get 'new_annual_planification' 
    get 'planification' #Load the initial information
    get 'annual_budget_details' # Process all selects changes (year,month,version)

    post 'create_annual_version' # Process the modal of new annual_version

    get 'load_base' #load the information to update a given base
    get 'change_base_type' #LOAD the info about the base type
    patch 'adjust_base' # Update the base for a given item and subfield

    get 'load_adjustments' #Load the information to create or update new adjustments
    post 'process_adjustments' # Update exiting adjustments or create new ones
    get 'add_virtual_adjustment' # add new row in virtual adjustment form

    get 'change_adjustment_amount' #load the information of percentaje in virtual_adjustment

    get 'quater_details'

    get 'control_report' #Load the information for the table control report
  end

  resources :adjustments do
    collection {post :import}
    collection {get :excel_example}
    collection { get :source }
  end

  resources :estimates do
   collection {post :import}
   collection {get :excel_example}
  end

  resources :budgets do
    collection { post :import }
    collection { get :check_year }
    collection {get :excel_example}
    collection { get :source }
  end
  resources :transactions do
    collection { get :bank_excel_example }
    collection { get :filial_excel_example }
    collection { post :import }
    collection {get :all, defaults: { format: :json}}
    collection { get :source }
  end
  resources :users do
    collection {post :import}
    collection {get :excel_example}
    collection { get :source }
  end
  resources :accounts do
    collection{post :import}
    collection{get :excel_example}
    collection { get :source }
  end
  resources :items do
    collection { post :import }
    collection { get :excel_example }
    collection { get :source }
  end
  resources :cost_centers do
    collection { post :import }
    collection { get :import_excel_example }
    collection { get :source }
  end
  resources :month_planifications do
    collection {post :create}
  end
  resources :base_adjustments
  resources :theoric_adjustments

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html

  root to: 'users#index'
end
