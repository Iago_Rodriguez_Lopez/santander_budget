require 'test_helper'

class PlanificationControllerTest < ActionController::TestCase
  test "should get monthly" do
    get :monthly
    assert_response :success
  end

  test "should get anual" do
    get :anual
    assert_response :success
  end

end
