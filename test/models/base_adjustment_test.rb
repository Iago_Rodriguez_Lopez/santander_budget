require 'test_helper'

class BaseAdjustmentTest < ActiveSupport::TestCase

  def setup
    @base_types = BaseAdjustment::BASE_TYPES.values

    @correct_params = {month_planification_id:2,base_type:@base_types[1], subfield_id:1}

    @base = BaseAdjustment.find(5)

    @incorrect_month_planification = {month_planification_id:-1,base_type:@base_types[1], subfield_id:1}
    @incorrect_base_type = {month_planification_id:2 ,base_type:"no es una base", subfield_id:1}
    @incorrect_subfield_id = {month_planification_id:2,base_type:@base_types[1], subfield_id:-1}
  end

  test "not found message when for month planification" do
    assert_equal(true,BaseAdjustment.update_bases_by(@incorrect_month_planification).has_key?(:not_found))
  end

  test "subfield error message when subfield can't be found" do
    assert_equal(true, BaseAdjustment.update_bases_by(@incorrect_subfield_id).has_key?(:not_found))
  end

  test "raise exception when amount is not numerical for a base adjustment" do
    @base.amount = "asdasd"
    assert_raises(ActiveRecord::RecordInvalid) { @base.save! }
  end


  test "exception when base type is invalid" do
    assert_raises(BaseAdjustment::BaseTypeRequirement) { @base.calculate_amount(@incorrect_base_type[:base_type]) }
  end

  test "base type error message when base type is incorrect" do
    assert_equal("La base indicada no pertenece a las bases del sistema", BaseAdjustment.update_bases_by(@incorrect_base_type)[:base_type])
  end

  test "calculate amount based on base_type" do
    base_type = @base_types[0]
    assert_equal(100,@base.calculate_amount(base_type))
  end

  test "calculate spent_previous_month for item" do
    item = Item.find_by_code("T0101000000")
    subfield = item.subfield
    year = 2017
    month = 10

    results = MonthPlanification.previous_spending(year,month,subfield)
    spent_previous_month = nil

    results.each do |result|
      spent_previous_month = result['amount'] if result['item_code'] == item.code
    end

    assert_equal(1200,spent_previous_month)
  end

  test "calculate spent_previous_month to given subfield" do
    subfield = Subfield.find_by_name("Remuneraciones")
    year = 2017
    month = 10

    item_codes = subfield.items.pluck(:code)

    results = MonthPlanification.previous_spending(year,month,subfield)
    total = 0

    results.each do |result|
     total += result['amount'] if item_codes.include?(result['item_code'])
    end

    assert_equal(2000,total)
  end

  test "calculate budget_previous_month for item" do
    item = Item.find_by_code("T0101000000")
    subfield = item.subfield
    year = 2017
    month = 10

    results = MonthPlanification.previous_budget(year,month,subfield)
    spent_previous_month = nil

    results.each do |result|
      spent_previous_month = result['amount'] if result['item_code'] == item.code
    end

    assert_equal(300,spent_previous_month)
  end

  test "calculate budget_previous_to given subfield" do
    subfield = Subfield.find_by_name("Remuneraciones")
    year = 2017
    month = 10

    item_codes = subfield.items.pluck(:code)

    results = MonthPlanification.previous_budget(year,month,subfield)
    total = 0

    results.each do |result|
      total += result['amount'] if item_codes.include?(result['item_code'])
    end

    assert_equal(600,total)
  end

  test "calculate average expenditure by item" do
    item = Item.find_by_code("T0101000000")
    subfield = item.subfield
    year = 2017
    month = 10

    results = MonthPlanification.monthly_average_expenditure(year,month,subfield)
    average_expenditure = nil

    results.each do |result|
      average_expenditure = result['monthly_average'] if result['item_code'] == item.code
    end

    assert_equal(0,average_expenditure)
  end

  test "calculate average expenditure by subfield " do
    subfield = Subfield.find_by_name("Remuneraciones")
    year = 2017
    month = 10

    item_codes = subfield.items.pluck(:code)

    results = MonthPlanification.monthly_average_expenditure(year,month,subfield)
    total = 0

    results.each do |result|
      total += result['monthly_average'] if item_codes.include?(result['item_code'])
    end

    assert_equal(0,total)
  end

end
