SELECT  subfields.id,subfields.name as subfield_name,items.description as item_description, items.id as item_id,items.code AS "item_code",q1,q2,q3,q4,current_month,last_month, base_amount,
january_budget,february_budget,february_budget,march_budget,april_budget,may_budget,june_budget,july_budget,august_budget,september_budget,october_budget,november_budget,december_budget,year_budget

FROM items

LEFT JOIN(SELECT
items.code as code,
items.subfield_id as subfield_id,
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 1.0 THEN budgets.amount ELSE 0 END) AS "january_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 2.0 THEN budgets.amount ELSE 0 END) AS "february_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 3.0 THEN budgets.amount ELSE 0 END) AS "march_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 4.0 THEN budgets.amount ELSE 0 END) AS "april_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 5.0 THEN budgets.amount ELSE 0 END) AS "may_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 6.0 THEN budgets.amount ELSE 0 END) AS "june_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 7.0 THEN budgets.amount ELSE 0 END) AS "july_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 8.0 THEN budgets.amount ELSE 0 END) AS "august_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 9.0 THEN budgets.amount ELSE 0 END) AS "september_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 10.0 THEN budgets.amount ELSE 0 END) AS "october_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 11.0 THEN budgets.amount ELSE 0 END) AS "november_budget",
SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 12.0 THEN budgets.amount ELSE 0 END) AS "december_budget",
SUM(budgets.amount) AS "year_budget"
FROM budgets
INNER JOIN items ON items.id = budgets.item_id
WHERE to_date('2017-01-01','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('2017-12-31','YYYY-MM-DD')
GROUP BY items.code,subfield_id) budget_detail on (budget_detail.code = items.code)


LEFT JOIN(SELECT
items.code as item_code,
items.subfield_id as subfield_id,
SUM(CASE WHEN EXTRACT(MONTH FROM transactions.expired_date) IN (1.0, 2.0, 3.0) THEN transactions.amount ELSE 0 END) AS "q1",
SUM(CASE WHEN EXTRACT(MONTH FROM transactions.expired_date) IN (4.0, 5.0, 6.0) THEN transactions.amount ELSE 0 END) AS "q2",
SUM(CASE WHEN EXTRACT(MONTH FROM transactions.expired_date) IN (7.0, 8.0, 9.0) THEN transactions.amount ELSE 0 END) AS "q3",
SUM(CASE WHEN EXTRACT(MONTH FROM transactions.expired_date) IN (10.0, 11.0, 12.0) THEN transactions.amount ELSE 0 END) AS "q4",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 2 THEN transactions.amount ELSE 0 END) AS "current_month",
SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 1 THEN transactions.amount ELSE 0 END) AS "last_month"
FROM transactions
INNER JOIN accounts ON accounts.id = transactions.account_id
INNER JOIN items ON items.id = accounts.item_id
WHERE to_date('2017-01-01','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('2017-12-31','YYYY-MM-DD')
GROUP BY subfield_id,items.code) transaction_detail on (transaction_detail.item_code = items.code)

LEFT JOIN subfields ON subfields.id = transaction_detail.subfield_id

LEFT JOIN(SELECT
SUM(theoric_adjustments.amount) as base_amount,
base_adjustments.item_id
FROM base_adjustments
INNER JOIN theoric_adjustments ON theoric_adjustments.base_adjustment_id = base_adjustments.id
GROUP BY base_adjustments.item_id 
) base ON (base.item_id=items.id)

GROUP BY subfields.id,items.id,q1,q2,q3,q4,current_month,last_month,transaction_detail,base_amount,january_budget,february_budget,february_budget,
march_budget,april_budget,may_budget,june_budget,july_budget,august_budget,september_budget,october_budget,november_budget,december_budget,year_budget
ORDER BY subfields.name,items.code
;
