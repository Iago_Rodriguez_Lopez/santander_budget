json.extract! estimate, :id, :item_id, :account_id, :amount, :date,:estimate_type, :created_at, :updated_at
json.url estimate_url(estimate, format: :json)
