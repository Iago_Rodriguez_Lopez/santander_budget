json.extract! budget, :id, :period, :format_month, :year, :created_at, :updated_at
json.url budget_url(budget, format: :json)
