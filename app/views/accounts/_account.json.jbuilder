json.extract! account, :id, :name, :code, :description, :created_at, :updated_at
json.url account_url(account, format: :json)
