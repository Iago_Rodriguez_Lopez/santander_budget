json.extract! adjustment, :id, :item_id, :account_id, :amount, :date,:type, :created_at, :updated_at
json.url adjustment_url(adjustment, format: :json)
