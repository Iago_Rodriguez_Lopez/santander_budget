json.extract! transaction, :id, :item_id, :cost_center_id, :amount, :kind, :created_at, :updated_at
json.url transaction_url(transaction, format: :json)
