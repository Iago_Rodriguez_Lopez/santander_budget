json.extract! cost_center, :id, :created_at, :updated_at
json.url cost_center_url(cost_center, format: :json)
