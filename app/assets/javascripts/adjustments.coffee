

$(document).on 'turbolinks:load', ->
  $('#adjustments-table').dataTable
    processing: true
    serverSide: true
    ajax: $('#adjustments-table').data('source')
    pagingType: 'full_numbers'
    language: 'url': 'https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
    columns: [
      {data: 'concept'}
      {data: 'adjustment_type'}
      {data: 'amount'}
      {data: 'date'}
      {data: 'item'}
      {data: 'account'}
      {data: 'actions'}
    ]