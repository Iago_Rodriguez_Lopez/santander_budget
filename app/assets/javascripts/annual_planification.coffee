# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

new_annual_planification = ->
  $(document).on 'click', '#new_annual_planification_button', (e) ->
    e.preventDefault()
    select_year = $('#annual_plannification_year');
    select_month = $('#annual_plannification_month');
    year = select_year.val();
    month = select_month.val();
    $.ajax({
      type: "GET",
      url: urls.open_modal_new_annual_planification,
      data: {year:year,pivot_month:month}
      })

create_annual_version = ->
  $(document).on 'submit','#new_annual_planification', (e) ->
    e.preventDefault()
    analysis_period = $('#annual_period_analysis')
    select_year = $('#annual_plannification_year');
    select_month = $('#annual_plannification_month');
    year = select_year.val();
    month = select_month.val();
    version = $(this).find('input[id="new_version_name"]').val();

    $.ajax({
      type: "POST",
      url: urls.create_annual_planification,
      data: {
        annual_planification:{
          name:version,
          year:year,
          pivot_month: month
        }
      }
      error: (xhr, status, errorThrown) ->
        errors_container = $('#new_annual_planification > div.errors')
        errors = xhr.responseJSON.errors

        errors_ul = "<ul>"

        for message of errors
          errors_ul += '<li>' + errors[message] + '</li>'

        errors_ul += "</ul>"

        div_container = """
        <div class="alert alert-danger fade in">
          <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
          #{errors_ul}
        </div>
        """
        errors_container.empty()
        errors_container.append(div_container)

        return
    })

annual_budget_details = ->
  $(document).on 'change','#annual_plannification_year, #annual_plannification_month', ->
    select_year = $('#annual_plannification_year');
    select_month = $('#annual_plannification_month');
    year = select_year.val();
    month = select_month.val();
    ajax_annual_budget_details(year,month,null)

  $(document).on 'change','select.annual-version-name', ->

    select_year = $('#annual_plannification_year');
    select_month = $('#annual_plannification_month');
    select_version = $('#annual_plannification_name');

    year = select_year.val();
    month = select_month.val();
    version = select_version.val();
    ajax_annual_budget_details(year,month,version)

ajax_annual_budget_details = (year,month,version) ->
  $.ajax({
      type: "GET",
      url: urls.budget_details_annual_planification,
      data: {
        annual_planification: {year:year,pivot_month:month,name:version}
      },
      error: (xhr, status, errorThrown) ->
        error = xhr.responseJSON.error

        console.log(xhr.status);
        console.log(error);
    })


open_modal_adjust_base = ->
  $(document).on 'click', '.define-annual-base', (e) ->
    e.preventDefault()
    item_code = $(this).data('item-code')
    subfield_name = $(this).data('subfield-name')

    if item_code == "-"
      item_code = null
    if subfield_name == "-"
      subfield_name = null

    select_year = $('#annual_plannification_year')
    select_month = $('#annual_plannification_month')

    select_version = $('#annual_plannification_name')

    year = select_year.val()
    month = select_month.val()
    version = select_version.val()
    $.ajax({
        type: "GET",
        url: urls.open_modal_adjust_base_annual_planification,
      data: {year:year,pivot_month:month,name:version, item_code:item_code, subfield_name:subfield_name}
      })

change_base_type_select = ->
  $(document).on 'change', '#annual_base_adjustment_base_type',(e) ->
    selected_base_type = this.value;

    select_year = $('#annual_plannification_year')
    select_month = $('#annual_plannification_month')

    select_version = $('#annual_plannification_name')

    year = select_year.val()
    month = select_month.val()
    version = select_version.val()
    subfield_id = $("#annual_base_adjustment_subfield_id").val()

    $.ajax({
      type: "GET",
      url: urls.change_base_type_annual_planification,
      data: {selected_base_type: selected_base_type,year:year,pivot_month:month,subfield_id:subfield_id,version:version}
    })

load_adjustments = ->
  $(document).on 'click', '.annual-adjustments', (e) ->
    e.preventDefault()

    select_year = $('#annual_plannification_year')
    select_month = $('#annual_plannification_month')
    select_version = $('#annual_plannification_name')

    year = select_year.val()
    month = select_month.val()
    version = select_version.val()

    subfield_name = $(this).data('subfield-name')
    item_code = $(this).data('item-code')
    if item_code == "-"
      item_code = null
    if subfield_name == "-"
      subfield_name = null

    $.ajax({
      type: "GET",
      url: urls.open_modal_adjustments,
      dataType: 'script',
      data: {year: year,pivot_month:month,name:version,subfield_name:subfield_name,item_code:item_code},
      error: (xhr, status, errorThrown) ->
        error = xhr.responseJSON.error

        console.log(xhr.status);
        console.log(error);
    })
add_virtual_adjustment = -> 
  $(document).on 'click', '#new-annual-adjustment-button', (e) ->
    e.preventDefault()
    adjustmentCount = 0
    adjustmentRow = $('#virtual_adjustment_form').children().last().children().first().children()[0]
    if (adjustmentRow != undefined )
      adjustmentCount = adjustmentRow.name.match(/\d+/)[0]
    newAdjustmentId = adjustmentCount + 1
    pivotMonth = $('#annual_plannification_month').val()
    
    $.ajax({
      type: "GET",
      url: urls.add_virtual_adjustment_annual_planification,
      data: {new_adjustment_id: newAdjustmentId, pivot_month: pivotMonth}
    })

delete_virtual_adjustment = -> 
  $(document).on 'click', '.delete_virtual_adjustment', (e) -> 
    e.preventDefault()
    html = $(this).parent().parent().parent().remove()

change_adjustment_kind = ->
  $(document).on 'change','.adjustment_kind',(e) ->
    kind = this.value 
    position = $(this).data('position')
    if(kind == '% Base')
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_percentage').prop('readonly',false)
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_amount').prop('readonly',true)
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_percentage').parent().removeClass("percentage-annual");
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_amount').parent().addClass("amount-annual")
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_amount').val("")

    else
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_percentage').prop('readonly',true)
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_amount').prop('readonly',false)
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_percentage').parent().addClass("percentage-annual");
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_amount').parent().removeClass("amount-annual")
      $('#annual_adjustment_group_virtual_annual_adjustments_attributes_'+position+'_percentage').val("")

change_adjustment_percentage = ->
  $(document).on 'change','.percentage-annual-adjustment',(e) ->
    item_code = $('#item_table').find('tr').find('td')[0].innerText
    if item_code == "-"
      item_code = null
    annual_base_adjustment_id = $('#annual_adjustment_group_annual_base_adjustment_id').val()
    percentage = this.value
    position = $(this).data('position')
    $.ajax({
      type: "GET",
      url: urls.change_adjustment_amount_annual_planification,
      dataType: 'script',
      data: {item_code: item_code,percentage: percentage,annual_base_adjustment_id:annual_base_adjustment_id,position:position}
    })

tree_table = ->
      $("#annual-transaction-details").treeFy treeColumn: 0
      $(".quater_details_table").treeFy treeColumn: 0



process_quater_details = ->
  $(document).on 'click', 'a.annual-quarter-details',(e) ->
    e.preventDefault()
    link = $(this)

    quarter = link.data('quarter')
    select_year = $('#annual_plannification_year')
    select_month = $('#annual_plannification_month')
    select_version = $('#annual_plannification_name')

    year = select_year.val()
    month = select_month.val()
    version = select_version.val()

    subfield_name = link.data('subfield-name');
    item_code = link.data('item_code');
    if item_code == "-"
      item_code = null
    if subfield_name == "-"
      subfield_name = null
    $.ajax({
      type: "GET",
      url: urls.annual_quater_details,
      data: {quarter: quarter, year:year,pivot_month:month,name:version,subfield_name:subfield_name,item_code:item_code}
    })
control_report = ->
  $(document).on 'click', 'a.annual_control_report', (e) ->
    e.preventDefault()
    report_type = $(this).data('report')
    year = $('#annual_plannification_year').val()
    month = $('#annual_plannification_month').val()
    version = $('#annual_plannification_name').val()
    $.ajax({
      type: "GET",
      url: urls.annual_planification_control_report,
      dataType: 'script',
      data: {report_type: report_type, year: year, month: month, version: version}
    })


onInit new_annual_planification
onInit create_annual_version
onInit annual_budget_details
onInit open_modal_adjust_base
onInit change_base_type_select
onInit load_adjustments
onInit add_virtual_adjustment
onInit delete_virtual_adjustment
onInit change_adjustment_kind
onInit change_adjustment_percentage
onInit process_quater_details
onInit control_report