# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  $('#budgets-table').dataTable
    processing: true
    serverSide: true
    ajax: $('#budgets-table').data('source')
    pagingType: 'full_numbers'
    language: 'url': 'https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
    columns: [
      {data: 'item_code'}
      {data: 'year'}
      {data: 'month'}
      {data: 'amount'}
      {data: 'actions'}
    ]
