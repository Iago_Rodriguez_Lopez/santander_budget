// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery.turbolinks
//= require jquery_ujs
//= require bootstrap-sprockets
//= require dataTables/jquery.dataTables
//= require dataTables/bootstrap/3/jquery.dataTables.bootstrap
//= require Chart.min
//= require bootstrap-treefy
//= require bootstrap-treefy.min
//= require gritter
//= require_self
//= require_tree .
//= require turbolinks
// Source : https://stackoverflow.com/questions/18770517/rails-4-how-to-use-document-ready-with-turbo-links
// Answer : vedant1811

//Urls
const urls ={
    root:'/santander_budget'
}

urls.budgets = urls.root + "/budgets/";
//Reports
urls.control_report = urls.root + "/reports/control";
urls.detailed_control_report = urls.root + "/reports/detailed_control";
urls.report_consultation = urls.root + "/reports/consultation";
urls.reports_year_report = urls.root + "/reports/year_report";
urls.month_report = urls.root + "/reports/month_report";
urls.change_adjustment_amount = urls.root + "/planification/change_adjustment_amount";

//Month planification
urls.create_month_planifications = urls.root + "/month_planifications";
urls.new_version_monthly_planification = urls.root + "/planification/new_monthly_budget_version";
urls.new_item_adjustment_monthly_planification = urls.root + "/planification/new_item_adjustment";
urls.delete_adjustment_monthly_planification = urls.root + "/planification/delete_item_adjustment"
urls.open_new_version_monthly_planification_modal = urls.root + "/planification/new_monthly_budget_modal";
urls.change_version_monthly_planification = urls.root + "/planification/change_version_monthly";
urls.change_base_type_month_planification = urls.root + "/planification/change_base_type";
urls.change_analysis_version_name_monthly_planification = urls.root + "/planification/change_analysis_version_name";
urls.open_adjustments_form_modal_planification = urls.root + "/planification/open_adjustments_form_modal";
urls.open_base_adjustments_modal_planification = urls.root + "/planification/open_base_adjustments_modal";
urls.quater_details = urls.root + "/planification/process_quater_details";
urls.planification_control_report = urls.root + "/planification/control_report"

//Annual planification
urls.open_modal_new_annual_planification = urls.root + "/annual_planification/new_annual_planification";
urls.create_annual_planification = urls.root + "/annual_planification/create_annual_version";
urls.budget_details_annual_planification = urls.root + "/annual_planification/annual_budget_details";
urls.open_modal_adjust_base_annual_planification = urls.root + "/annual_planification/load_base";
urls.change_base_type_annual_planification = urls.root + "/annual_planification/change_base_type";
urls.open_modal_adjustments = urls.root + "/annual_planification/load_adjustments";
urls.add_virtual_adjustment_annual_planification = urls.root + "/annual_planification/add_virtual_adjustment";
urls.change_adjustment_amount_annual_planification = urls.root + "/annual_planification/change_adjustment_amount";
urls.annual_quater_details = urls.root + "/annual_planification/quater_details";
urls.annual_planification_control_report = urls.root + "/annual_planification/control_report";

urls.I18n_data_table_path = urls.root + "/data_table_spanish.json";

onInit = function (callback) {
  $(document).ready(callback);
  $(document).on('page:load', callback);
}
$(document).on('turbolinks:load', function () {
  var tooltip = function () {
    $('[data-toggle="tooltip"]').tooltip();
  }
  $('.table-initialization').dataTable({
    "language": {
      "url": urls.I18n_data_table_path
    }
  });
  //check if the year of the budget is in the database and alert
  $("#date_year").change(function (e) {
    var years = [];
    var year = $("select option:selected").val();
    $.getJSON(urls.budgets, function (data) {
      $(data).each(function () {
        years.push(this);
        if (String(year) == String(this)) {
          alert("El año " + year + " ya esta en la base de datos");
        }
      });
    });
  });
  //when change the file, ask if you want to override the data.
  $("#upload").change(function (e) {
    var years = [];
    var year = $("select option:selected").val();
    $.getJSON(urls.budgets, function (data) {
      $(data).each(function () {
        years.push(this);
        if (String(year) == String(this)) {
          var r = confirm("Se reemplazará los valores del año " + year);
          if (r == true) {
            $("#import_form").submit();
          } else {
            window.location.href = "/budgets/";
          }
        }
      });
    });
  });
});
removeModalFadeIn = function () {
    $('body').removeClass('modal-open');
    $('.modal-backdrop').remove();
  }
  //Source:https://stackoverflow.com/questions/13343566/set-select-option-selected-by-value
  //TODO: Change all the select that used selected by val.
var change_selected_option;
change_selected_option = function (changed_element, selected_value) {
  var option_selector;
  changed_element.find("option[selected='selected']").removeAttr("selected");
  option_selector = "option[value='" + selected_value + "']";
  return changed_element.find(option_selector).attr("selected", "selected");
};