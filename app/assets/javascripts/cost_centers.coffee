
$(document).on 'turbolinks:load', ->
  $('#cost-centers-table').dataTable
    processing: true
    serverSide: true
    ajax: $('#cost-centers-table').data('source')
    pagingType: 'full_numbers'
    language: 'url': 'https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
    columns: [
      {data: 'code'}
      {data: 'name'}
      {data: 'division'}
      {data: 'management'}
      {data: 'area'}
      {data: 'subarea'}
      {data: 'actions'}
    ]
