# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


$(document).on 'turbolinks:load', ->
  $('#items-table').dataTable
    processing: true
    serverSide: true
    ajax: $('#items-table').data('source')
    pagingType: 'full_numbers'
    language: 'url': 'https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
    columns: [
      {data: 'code'}
      {data: 'description'}
      {data: 'interpretation_ccc'}
      {data: 'code_ccc'}
      {data: 'interpretation_cargabal'}
      {data: 'code_cargabal'}
      {data: 'interpretation_ges'}
      {data: 'code_ges'}
      {data: 'subfield'}
      {data: 'format_bank'}
      {data: 'format_group'}
      {data: 'format_spain'}
      {data: 'actions'}
    ]