# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


control_report = ->
  $(document).on "click",'a[data-report-type]',(event) ->
    event.preventDefault()
    report_type = $(this).data('report-type')
    year_selected = $("#year_report option:selected").val()
    month_selected = $("#month_report option:selected").val()

    $.ajax({
      type: "POST",
      url: urls.control_report,
      dataType: 'script',
      data: {report_type:report_type ,year_report: year_selected,month_report: month_selected}
    })

detailed_report = ->
  $(document).on "click",'a[data-detailed-report-type]',(event) ->
    event.preventDefault()
    report_type = $(this).data('detailed-report-type')
    start_year = $("#start_year option:selected").val()
    end_year = $("#end_year option:selected").val()

    $.ajax({
      type: "POST",
      url: urls.detailed_control_report,
      dataType: 'script',
      data: {report_type:report_type ,start_year: start_year,end_year: end_year}
    })


consultation_report_change = ->
  $(document).on 'change','.consultation_select', ->
    consultation_report()


consultation_report_submit = ->
  $(document).on 'click','#consultation_commit',(e) ->
    e.preventDefault()
    consultation_report()

consultation_report_change_year = ->
  $(document).on 'change','#year_consultation_report',(e) ->
    consultation_report()

consultation_report = ->
  options = []
  year = $('#year_consultation_report').val()
  $('.consultation_select option:selected').each ->
    option = $(this).text()
    format_type = $(this).parent().attr('id')
    options.push({format_type: format_type, option_select:option})
  $.ajax({
    type: "POST",
    url: urls.report_consultation,
    dataType: 'script',
    data: {format_options:options,year_report:year}
  })

year_modal_consultation = ->
  $(document).on 'click','.year_modal', (e) ->
    item_index = $(this).attr("id")
    options = []
    $('[data-toggle="tooltip"]').tooltip()
    year = $('#year_consultation_report').val()
    $('.consultation_select option:selected').each ->
      option = $(this).text()
      format_type = $(this).parent().attr('id')
      options.push({format_type: format_type, option_select:option})

    $.ajax({
      type: "POST",
      url: urls.reports_year_report,
      dataType: 'script',
      data:{item_index: item_index, format_options:options , year_report:year}
      })

month_modal_consultation = ->
  $(document).on 'click', '.month_modal', (e) ->
    e.preventDefault()
    item_index = $(this).attr("id")
    item_month = $(this).attr("href")
    $('[data-toggle="tooltip"]').tooltip()
    options = []
    year = $('#year_consultation_report').val()

    $('.consultation_select option:selected').each ->
      option = $(this).text()
      format_type = $(this).parent().attr('id')
      options.push({format_type: format_type, option_select:option})

    $.ajax({
      type: "POST",
      url: urls.month_report,
      dataType: 'script',
      data: {item_index:item_index, format_options:options, year_report:year , item_month:item_month}
    })

onInit control_report
onInit consultation_report_change
onInit consultation_report_submit
onInit consultation_report_change_year
onInit year_modal_consultation
onInit month_modal_consultation
onInit detailed_report
