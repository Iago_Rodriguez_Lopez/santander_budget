# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  $('#transactions-table').dataTable
    processing: true
    serverSide: true
    ajax: $('#transactions-table').data('source')
    pagingType: 'full_numbers'
    language: 'url': 'https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
    columns: [
      {data: 'account'}
      {data: 'item'}
      {data: 'cost_center'}
      {data: 'amount'}
      {data: 'source'}
      {data: 'expired_date'}
      {data: 'document_date'}
      {data: 'comment'}
      {data: 'enterprise_code'}
      {data: 'enterprise_name'}
      {data: 'actions'}
    ]