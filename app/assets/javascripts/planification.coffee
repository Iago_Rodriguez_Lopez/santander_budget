# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/


change_analysis_period = ->
  $(document).on 'change','select.selected-period',(e) ->
    change_selected_option($(this),this.value);

    analysis_period = $('#period_analysis')
    year = $('#year').val()
    month = $('#month').val()
    month_name = $('#month')[0][month-1].text
    selected_period = $('#selected_period')
    new_period = "Periodo selecionado : " + month_name + " - " + year;
    selected_period.html(new_period);
    $.ajax({
      type: "GET",
      url: urls.change_version_monthly_planification,
      dataType: 'script',
      data: {year: year, month: month}
      })

change_analysis_version_name = ->
  $(document).on 'change','select.version-name', ->
    seleted_value = this.value;
    change_selected_option($(this),seleted_value);
    analysis_period = $('#period_analysis')
    year = $('#year').val()
    month = $('#month').val()
    alert(month)

    $.ajax({
      type: "GET",
      url: urls.change_analysis_version_name_monthly_planification,
      dataType: 'script',
      data: {version:seleted_value,year:year,month:month}
    })

open_new_monthly_version_modal = ->
  $(document).on 'click','#new_version', ->
    analysis_period = $('#period_analysis')
    year = $('#year').val()
    month = $('#month').val()
    month_name = $('#month')[0][month-1].text
    $.ajax({
      type: "GET",
      url: urls.open_new_version_monthly_planification_modal,
      dataType: 'script',
      data: { year:year,month:month ,month_name:month_name}
      })

create_new_monthly_version = ->
  new_montly_form = $('#new_monthly_version_form')
  $(document).on 'submit','#new_monthly_version_form', (e) ->
    e.preventDefault()
    analysis_period = $('#period_analysis')
    year = $('#year').val()
    month = $('#month').val()
    version = $(this).find('input[id="new_version_name"]').val();

    $.ajax({
      type: "POST",
      url: urls.create_month_planifications,
      dataType: 'script',
      data: {year: year,month: month,name:version}
    })

open_base_adjustments_modal = ->
  $(document).on 'click','.define-adjustments', ->
    subfield = $(this).data('subfield_name')
    year = $('#year').val()
    month = $('#month').val()
    version = $('#version_name').val()
    
    $.ajax({
      type: "GET",
      url: urls.open_base_adjustments_modal_planification,
      dataType: 'script',
      data: {subfield_name: subfield,version: version,year:year,month:month}
    })

open_adjustments_form_modal = ->
  $(document).on 'click','.adjustments', ->
    item_code = $(this).data('item_code')
    version = $('#version_name').val();

    $.ajax({
      type: "GET",
      url: urls.open_adjustments_form_modal_planification,
      dataType: 'script',
      data: {item_code: item_code,version:version}
    })

data_for_new_adjustment = ->
  $(document).on 'click','#new-adjustment-button',(e) ->
    e.preventDefault()
    base_adjustment_id = $(this).data('base-adjustment-id')
    item_id = $(this).data('item-id')
    $.ajax({
      type: "POST",
      url: urls.new_item_adjustment_monthly_planification,
      dataType: 'script',
      data: {base_adjustment_id: base_adjustment_id, item_id:item_id}
      })

change_amount_adjustment = ->
  $(document).on 'change','.amount-adjustment',(e) ->
    $('.edit_base_adjustment').submit()
    $('#total_control_report').click()

change_name_adjustment = ->
  $(document).on 'change','.text-adjustment',(e) ->
    $('.edit_base_adjustment').submit()
    $('#total_control_report').click()

delete_adjustment = ->
  $(document).on 'click', '.delete_adjustment',(e) ->
    e.preventDefault()
    base_adjustment_id = $(this).data('base_adjustment_id')
    theoric_adjustment_id = $(this).data('theoric_adjustment_id')
    $.ajax({
      type: "POST",
      url: urls.delete_adjustment_monthly_planification,
      dataType: 'script',
      data: {base_adjustment_id: base_adjustment_id, theoric_adjustment_id:theoric_adjustment_id}
      })

    

submit_adjustment_form = ->
  $(document).on 'click','#submit_adjustment_form-button',(e) ->
    $('#total_control_report').click()
    $('.edit_base_adjustment').submit()
    $('#budget_detail_by_item').modal("toggle")

change_adjustment_kind = ->
  $(document).on 'change','.adjustment_kind',(e) ->
    kind = this.value
    position = $(this).data('position')
    if(kind == '% Base')
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_percentage').prop('readonly',false)
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_percentage').parent().removeClass("percentage-monthly");
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_amount').prop('readonly',true)
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_amount').parent().addClass("amount-monthly");
    else
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_amount').parent().removeClass("amount-monthly");
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_percentage').prop('readonly',true)
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_amount').prop('readonly',false)
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_percentage').parent().addClass("percentage-monthly");
      $('#base_adjustment_theoric_adjustments_attributes_'+position+'_percentage').val("")
    
change_adjustment_percentage = ->
  $(document).on 'change','.percentage-adjustment',(e) ->
    item_code = $('#item_table').find('tr').find('td')[0].innerText
    percentage = this.value
    base_adjustment_id = $(this).data('base-adjustment-id')
    position = $(this).data('position')
    $.ajax({
      type: "GET",
      url: urls.change_adjustment_amount,
      dataType: 'script',
      data: {item_code: item_code,percentage: percentage,position:position,base_adjustment_id:base_adjustment_id}
    })


change_base_type_select = ->
  $(document).on 'change', '#base_adjustment_base_type',(e) ->
    seleted_value = this.value;
    analysis_period = $('#period_analysis')
    year = $('#year').val()
    month = $('#month').val()
    subfield_id = $("#base_adjustment_subfield_id").val()

    $.ajax({
      type: "GET",
      url: urls.change_base_type_month_planification,
      dataType: 'script',
      data: {seleted_value: seleted_value,year:year,month:month,subfield_id:subfield_id}
    })

data_for_base_adjustments = ->
  $(document).on 'click','.define-adjustments', ->
    row_details = $(this).closest('tr');
    columns = row_details.find('td');
    subfield = columns[0].innerText;
    budget = columns[7].innerText;

    year = $('#year').val()
    month = $('#month').val()
    version = $('#version_name').val()
    
    $.ajax({
      type: "GET",
      url: urls.adjust_monthly_base,
      dataType: 'script',
      data: {subfield_name: subfield,budget:budget,version: version,year:year,month:month}
    })

create_new_base_adjustment = ->
  $(document).on 'submit', '#adjust_base_form',(e) ->
    e.preventDefault()
    form_adjustment_control = $('#form_adjustment_control')
    analysis_period = $('#period_analysis')
    base_type = form_adjustment_control.find("select[name='base_type'] option[selected='selected']").text()
    year = $('#year').val()
    month = $('#month').val()
    base_value = form_adjustment_control.find("select[name='base_value']").val()
    $.ajax({
      type: "POST",
      url: urls.new_monthly_base,
      dataType: 'script',
      data: {base_type: base_type,base_value: base_value}
    })

tree_table = ->
  $("#transaction-details").treeFy treeColumn: 0, initState: 'collapsed'
  $(".quater_details_table").treeFy treeColumn: 0

process_quater_details = ->
  $(document).on 'click', 'a.quater-detail',(e) ->
    e.preventDefault()

    link = $(this)
    quater = link.data('quater')
    year = $('#year').val()
    month = $('#month').val()
    version = $('#version_name').val()

    data_row = link.closest('tr');
    subrubro_node = data_row.find('.subrubro');
    item_node  = data_row.find('.item:first-child');
    subrubro;
    item_code;

    if(subrubro_node.length != 0)
      subrubro = subrubro_node.data('subrubro');
    else if(item_node.length != 0)
      subrubro = item_node.data('subrubro');
      item_info = item_node.data('itemInfo');
      item_code = item_node.data('itemCode');
      item_description = item_node.data('itemDescription')
    $.ajax({
      type: "GET",
      url: urls.quater_details,
      dataType: 'script',
      data: {year:year,month:month,version:version,quarter:quater,subfield:subrubro,item_code:item_code} #{quater: quater,quarter_data:quarter_data, year:year,month:month,version:version,subrubro:subrubro,item_code:item_code,item_description:item_description}
    })


control_report = ->
  $(document).on 'click', 'a.control_report', (e) ->
    e.preventDefault()
    report_type = $(this).data('report')
    year = $('#year').val()
    month = $('#month').val()
    version = $('#version_name').val()

    $.ajax({
      type: "GET",
      url: urls.planification_control_report,
      dataType: 'script',
      data: {report_type: report_type, year: year, month: month, version: version}
    })

onInit change_analysis_period
onInit change_analysis_version_name
onInit create_new_monthly_version
onInit open_adjustments_form_modal
onInit tree_table
onInit open_base_adjustments_modal
onInit process_quater_details
onInit control_report
onInit open_new_monthly_version_modal
onInit data_for_new_adjustment
onInit change_amount_adjustment
onInit change_name_adjustment
onInit submit_adjustment_form
onInit change_base_type_select
onInit delete_adjustment
onInit change_adjustment_kind
onInit change_adjustment_percentage
