# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

$(document).on 'turbolinks:load', ->
  $('#users-table').dataTable
    processing: true
    serverSide: true
    ajax: $('#users-table').data('source')
    pagingType: 'full_numbers'
    language: 'url': 'https://cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json'
    columns: [
      {data: 'id'}
      {data: 'name'}
      {data: 'last_name'}
      {data: 'age'}
      {data: 'sex'}
      {data: 'actions'}
    ]
