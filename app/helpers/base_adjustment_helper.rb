module BaseAdjustmentHelper
  def get_theoric_adjustments_sum(subfield,item,month_planification)
    if month_planification.present?
      base_adjustment = BaseAdjustment.find_by(subfield_id: subfield.id,item_id:item.id,month_planification_id:month_planification.id)
      theoric_adjustments = get_theoric_adjustments(base_adjustment,item.id)
      sum_theoric_adjustments(theoric_adjustments)
    else
      return 0
    end
  end
  def get_base_adjustments_sum(subfield,month_planification)
    if month_planification.present?
      base_adjustments = BaseAdjustment.where(subfield_id: subfield.id,month_planification_id:month_planification.id)
      sum_base_adjustments(base_adjustments)
    else
      return 0
    end
  end
  def sum_base_adjustments(base_adjustments)
    sum= 0
    base_adjustments.each do |base_adjustment|
      theoric_adjustments = get_theoric_adjustments(base_adjustment,base_adjustment.item_id)
      sum += sum_theoric_adjustments(theoric_adjustments)
    end
    sum
  end
  def get_total_base_adjustment_sum(month_planification)
    if month_planification.present?
      base_adjustments = BaseAdjustment.where(month_planification_id:month_planification.id)
      sum_base_adjustments(base_adjustments)
    else
      return 0
    end
  end
end
