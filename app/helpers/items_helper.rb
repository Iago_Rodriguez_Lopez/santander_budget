module ItemsHelper

  def edit_item(item,icon)
    link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"), edit_item_path(item)
  end

  def delete_item(item,icon)
    link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"),item, data: { confirm: '¿Estás seguro?' }, method: :delete
  end

  def item_actions(item)
    edit_item(item,"glyphicon-pencil") + delete_item(item,"glyphicon-remove")
  end
end
