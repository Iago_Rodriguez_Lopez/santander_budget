module BudgetsHelper
    def options_months
        Budget::MONTHS.to_enum(:each_with_index).map{|m,index| [m,index+1]}
    end

    def format_month(numerical_month)
        position = numerical_month - 1
        Budget::MONTHS[position].capitalize
    end

    def edit_object_link(object,icon)
        link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"), edit_budget_path(object)
    end

    def delete_object_link(object,icon)
        link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"),object, data: { confirm: '¿Estás seguro?' }, method: :delete
    end

    def actions_links(object)
        edit_object_link(object,"glyphicon-pencil") + delete_object_link(object,"glyphicon-remove")
    end

    def currency_format(number)
        if number.is_a? Numeric
            number_in_millions = number/1000000.0
            number_to_currency(number_in_millions, unit: "$", precision:4,separator: ",", delimiter: ".",format: "%u %n")
        else
            '-'
        end
    end
end
