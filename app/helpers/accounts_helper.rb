module AccountsHelper
  def edit_object_link(object,icon)
    link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"), edit_account_path(object)
  end

  def delete_object_link(object,icon)
    link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"),object, data: { confirm: '¿Estás seguro?' }, method: :delete
  end

  def actions_links(object)
    edit_object_link(object,"glyphicon-pencil") + delete_object_link(object,"glyphicon-remove")
  end
end
