module ApplicationHelper
    def bootstrap_class_for flash_type
        case flash_type
        when "notice"
            "alert-success" # Green
        when "error"
            "alert-danger" # Red
        when "alert"
            "alert-warning" # Yellow
        when "notice"
            "alert-info" # Blue
        else
            flash_type.to_s
        end
    end

  # Método se encarga de renderear los errores que tenga un objeto ActiveRecord en un cuadro de danger de Bootstrap.
  # Si no tiene errores no renderea nada.
  # @param [ActiveRecord, model_object] Objeto que puede contener errores
  # @param [String, title] Titulo para mostrar al principio del cuadro Bootstrap
  def bootstrap_model_errors(model_object = nil, title = nil)
    bootstrap_message = ''
    unless model_object.blank? or !model_object.errors.any?
      error_list = []
      unless title.blank?
        error_list << title
      end
      error_list << '<ul>'
      model_object.errors.full_messages.each do |msg|
        error_list << '<li>'+msg+'</li>'
      end
      error_list << '</ul>'
      error_list_str = error_list.join("\n").html_safe
      bootstrap_message = content_tag(:div,
                         content_tag(:a, raw('&times;'), :class => 'close', 'data-dismiss' => 'alert') +
                             error_list_str.html_safe,:class => 'alert fade in alert-danger')
    end
    bootstrap_message.html_safe
  end

  def currency_format(number)
      if number.is_a? Numeric
        number_in_millions = number/1000000.0
        number_to_currency(number_in_millions, unit: "$", precision:4,separator: ",", delimiter: ".",format: "%u %n")
      else
        '-'
      end
  end

  def difference_format(number1,number2)
    if number1.is_a? Numeric and number2.is_a? Numeric
        currency_format(number1-number2)
    else
        '-'
    end
  end

  def percentage(number1,number2)
    if number1.is_a? Numeric and number2.is_a? Numeric
      number2 == 0 ? '-' : number_to_percentage((number1/number2.to_f)*100, precision: 0)
    else
      '-'
    end
  end
  def percentage_increase(number1,number2)
    if number1.is_a? Numeric and number2.is_a? Numeric
      number2 == 0 ? '-' : number_to_percentage(((number1/number2.to_f)*100)-100, precision: 0)
    else
      '-'
    end
  end
  def year_report_stats(before_previous_year_results,previous_year_results,current_year_result,month,index)
    month = get_month(month,true)
    anual_budget_transaction = {
      labels: [before_previous_year_results[index]["year"],previous_year_results[index]["year"],current_year_result["year"]],
      datasets: [
        {
            label: "Presupuesto",
            backgroundColor: "rgba(255, 99, 132, 0.2)",
            borderColor: "rgba(255, 99, 132, 1)",
            data: [before_previous_year_results[index]["#{month}_budget"],previous_year_results[index]["#{month}_budget"],current_year_result["#{month}_budget"]]
        },
        {
            label: "Gasto",
            backgroundColor: "rgba(151,187,205,0.2)",
            borderColor: "rgba(151,187,205,1)",
            data: [before_previous_year_results[index]["#{month}_transaction"],previous_year_results[index]["#{month}_transaction"],current_year_result["#{month}_transaction"]]
        }
      ]
    }
  end


  def to_month_year_report_stats(before_previous_year_results,previous_year_results,current_year_result,month,index)
    current_year_acomulate_budget = 0.0
    previous_year_acomulate_budget = 0.0
    before_previous_year_acomulate_budget = 0.0
    current_year_acomulate_transactions = 0.0
    previous_year_acomulate_transactions = 0.0
    before_previous_year_acomulate_transactions = 0.0

    (1..month).each do |current_month|
      month_name = get_month(current_month,true)
      puts(month_name)
      current_year_acomulate_budget+= current_year_result["#{month_name}_budget"].present? ? current_year_result["#{month_name}_budget"] :0
      current_year_acomulate_transactions += current_year_result["#{month_name}_transaction"].present? ? current_year_result["#{month_name}_transaction"] :0
      previous_year_acomulate_budget += previous_year_results[index]["#{month_name}_budget"].present? ? previous_year_results[index]["#{month_name}_budget"] :0
      previous_year_acomulate_transactions += previous_year_results[index]["#{month_name}_transaction"].present? ? previous_year_results[index]["#{month_name}_transaction"] :0
      before_previous_year_acomulate_budget += before_previous_year_results[index]["#{month_name}_budget"].present? ? before_previous_year_results[index]["#{month_name}_budget"] :0
      before_previous_year_acomulate_transactions += before_previous_year_results[index]["#{month_name}_transaction"].present? ? before_previous_year_results[index]["#{month_name}_transaction"] :0
    end
    anual_budget_transaction = {
      labels: [before_previous_year_results[index]["year"],previous_year_results[index]["year"],current_year_result["year"]],
      datasets: [
        {
            label: "Presupuesto",
            backgroundColor: "rgba(255, 99, 132, 0.2)",
            borderColor: "rgba(255, 99, 132, 1)",
            data: [before_previous_year_acomulate_budget,previous_year_acomulate_budget,current_year_acomulate_budget]
        },
        {
            label: "Gasto",
            backgroundColor: "rgba(151,187,205,0.2)",
            borderColor: "rgba(151,187,205,1)",
            data: [before_previous_year_acomulate_transactions,previous_year_acomulate_transactions,current_year_acomulate_transactions]
        }
      ]
    }
  end


  def to_month_report_stats(current_year_result,month)
    month_labels = []
    budgets_data = []
    transactions_data = []
    (1..month).each do |current_month|
      month_name = get_month(current_month,true)
      month_lable_name = get_month(current_month,false)
      month_budget_data = current_year_result["#{month_name}_budget"].present? ? current_year_result["#{month_name}_budget"] :0
      month_transaction_data = current_year_result["#{month_name}_transaction"].present? ? current_year_result["#{month_name}_transaction"] :0

      month_labels << month_lable_name
      budgets_data << month_budget_data
      transactions_data << month_transaction_data
    end
    anual_budget_transaction = {
      labels: month_labels,
      datasets: [
        {
            label: "Presupuesto",
            backgroundColor: "rgba(255, 99, 132, 0.2)",
            borderColor: "rgba(255, 99, 132, 1)",
            data: budgets_data
        },
        {
            label: "Gasto",
            backgroundColor: "rgba(151,187,205,0.2)",
            borderColor: "rgba(151,187,205,1)",
            data: transactions_data
        }
      ]
    }
  end

  def to_month_difference_report_stats(current_year_result,month)
    month_labels = []
    difference_datas = []
    (1..month).each do |current_month|
      month_name = get_month(current_month,true)
      month_lable_name = get_month(current_month,false)
      month_budget_data = current_year_result["#{month_name}_budget"].present? ? current_year_result["#{month_name}_budget"] :0
      month_transaction_data = current_year_result["#{month_name}_transaction"].present? ? current_year_result["#{month_name}_transaction"] :0
      difference_data  = month_budget_data-month_transaction_data

      month_labels << month_lable_name
      difference_datas << difference_data
    end
    difference_budget_transaction = {
      labels: month_labels,
      datasets: [
        {
            label: "Diferencia presupuesto-Gastos",
            backgroundColor: "rgba(51,153,255,0.2)",
            borderColor: "rgba(51,153,255,1)",
            data: difference_datas
        }
      ]
    }
  end


  def month_report_stats(current_year_result,month)
    past_month = get_month(month-1,true)
    current_month = get_month(month,true)
    next_month = get_month(month+1,true)
    anual_budget_transaction = {
      labels: [get_month(month-1,false),get_month(month,false),get_month(month+1,false)],
      datasets: [
        {
            label: "Presupuesto",
            backgroundColor: "rgba(255, 99, 132, 0.2)",
            borderColor: "rgba(255, 99, 132, 1)",
            data: [current_year_result["#{past_month}_budget"],current_year_result["#{current_month}_budget"],current_year_result["#{next_month}_budget"]]
        },
        {
            label: "Gasto",
            backgroundColor: "rgba(151,187,205,0.2)",
            borderColor: "rgba(151,187,205,1)",
            data: [current_year_result["#{past_month}_transaction"],current_year_result["#{current_month}_transaction"],current_year_result["#{next_month}_transaction"]]
        }
      ]
    }
  end
  def comulative_budget_transaction(current_year_result,month)
    comulative_budget = 0
    comulative_transaction = 0
    (1..month).each do |current_month|
      budget_month = get_month(current_month,true)+"_budget"
      transaction_month = get_month(current_month,true)+"_transaction"
      comulative_budget += current_year_result[budget_month].present? ? current_year_result[budget_month]:0
      comulative_transaction += current_year_result[transaction_month].present? ? current_year_result[transaction_month]:0
    end
    return comulative_budget,comulative_transaction
  end


  def get_month(index,english)
    if english
      months = ["january","february","march","april","may","june","july","august","september","october","november","december"]
    else
      months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
    end
    if index == 13
      month = months[0]
    elsif index == 0
      month = months[11]
    else
      month = months[index-1]
    end
  end

  def both_locale_humanized_months
    Budget::MONTHS.to_enum(:each_with_index).map{|m,index| [m,index+1]}
  end

  def locale_humanized_months
    Budget::MONTHS.to_enum(:each_with_index).map{|m,index| m}
  end

  def both_month_version_by_index(index)
    months = both_locale_humanized_months
    selected_month = months[index]

    humanized_month = selected_month[0]
    number_month = selected_month[1]

    return humanized_month, number_month
  end

  def month_name_by_number(number)
    months = locale_humanized_months
    index = number - 1

    months[index].capitalize
  end

  def month_index_by_number(number)
    (number.to_i - 1)
  end


  def get_report_type(model_type)
    Report::MODELS_TYPES[model_type.to_sym]
  end
end
