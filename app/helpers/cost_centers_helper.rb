module CostCentersHelper
  def load_index_translations
    @cost_center_t = t('activerecord.models.cost_center')
    @cost_center_attributes_t = t('activerecord.attributes.cost_center')
    @actions_t = t('actions')
  end

  def edit_object_link(object,icon)
    link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"), edit_cost_center_path(object)
  end

  def delete_object_link(object,icon)
    link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"),object, data: { confirm: '¿Estás seguro?' }, method: :delete
  end

  def actions_links(object)
    edit_object_link(object,"glyphicon-pencil") + delete_object_link(object,"glyphicon-remove")
  end
end
