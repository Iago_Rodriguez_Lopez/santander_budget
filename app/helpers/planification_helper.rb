module PlanificationHelper
  def options_years
    Budget.years_available.map{|year| [year,year]}
  end
  def month_planification_versions(month,year)
    MonthPlanification.get_month_names(month,year).map{|name| [name,name]}
  end
  def get_theoric_adjustments(base_adjustment_id,item_id)
    #TODO: Cambiar esto al modelo
    TheoricAdjustment.where(base_adjustment_id: base_adjustment_id, item_id: item_id)
  end

  def get_quarter(data,quarter,month)
    result = 0
    quarter = (quarter*3)-2
    (quarter..quarter+2).each do |n|
      if n <= month
        result += data[get_month(n,true)].present? ? data[get_month(n,true)] : 0
      end
    end
    result.to_i
  end
  
  def get_quarter_total(data,quarter,month)
    if data.blank?
      return 0
    end
    sum = 0
     data.each do |d|
       sum +=get_quarter(d,quarter,month)
     end
     sum.to_i
  end

  def get_total(data,month)
    if data.blank?
      return 0
    end
    sum = 0
    data.each do |d|
      sum += d["#{month}"]
    end
    sum.to_i
  end

  def get_theoric_adjustments_sum(subfield, item, month_planification)
    if month_planification.present?
      base_adjustment = BaseAdjustment.find_by(subfield_id: subfield.id, item_id: item.id, month_planification_id: month_planification.id)
      sum_base_adjustment(base_adjustment)
    else
      0
    end
  end

  def get_base_adjustments_sum(subfield, month_planification)
    if month_planification.present?
      base_adjustments = BaseAdjustment.where(subfield_id: subfield.id,month_planification_id:month_planification.id)
      sum_base_adjustments(base_adjustments)
    else
      0
    end
  end

  def sum_base_adjustments(base_adjustments)
    sum = 0
    base_adjustments.each do |base_adjustment|
      theoric_adjustments = get_theoric_adjustments(base_adjustment, base_adjustment.item_id)
      sum += base_adjustment.amount.present? ? base_adjustment.amount : 0
      sum += sum_theoric_adjustments(theoric_adjustments)
    end
    sum
  end

  def get_total_base_adjustment_sum(month_planification)
    if month_planification.present?
      base_adjustments = BaseAdjustment.where(month_planification_id: month_planification.id)
      sum_base_adjustments(base_adjustments)
    else
      0
    end
  end

  def get_subfield(subfield_name)
    Subfield.find_by(name: subfield_name)
  end

  def sum_base_adjustment(base_adjustment)
    sum = 0
    sum += base_adjustment.amount.present? ? base_adjustment.amount : 0
    sum += sum_theoric_adjustments(base_adjustment.theoric_adjustments)
    sum
  end

  def sum_theoric_adjustments(theoric_adjustments)
    sum = 0
    theoric_adjustments.each do |t|
      sum += t.amount
    end
    sum
  end

  def get_item(item_code)
    Item.search_existence(item_code)
  end
end
