module UsersHelper
  def edit_user(object,icon)
    link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"), edit_user_path(object)
  end

  def delete_user(object,icon)
    link_to content_tag(:span,"",class:"glyphicon #{icon.to_s}"),object, data: { confirm: '¿Estás seguro?' }, method: :delete
  end

  def user_actions(object)
    edit_user(object,"glyphicon-pencil") + delete_user(object,"glyphicon-remove")
  end
end
