module AnnualPlanificationHelper

  def get_base_type_name(base_type)
    AnnualPlanification::BASE_ADJUSTMENTS_TYPE[base_type.to_sym] if base_type != nil
  end
  def calculate_budgeted_columns(columns,amount_default_columns,month)
    quarter_counter = 0
    quarter_counter += columns.include?('q1')||columns.include?("q1_#{month}") ? 0 : 1
    quarter_counter += columns.include?('q2')||columns.include?("q2_#{month}") ? 0 : 1
    quarter_counter += columns.include?('q3')||columns.include?("q3_#{month}") ? 0 : 1
    quarter_counter += columns.include?('q4')||columns.include?("q4_#{month}") ? 0 : 1
    columns.size - 1 - amount_default_columns + quarter_counter
  end

  def set_node_parent(total_level,row,row_index,parents)
    row_level = total_level - row.ord.to_i

    # set new father
    parent_same_level = parents[row_level]
    parents[row_level] = row_index if parent_same_level.blank? or parent_same_level != row_index

    parent = if row_level > 0
               parents[row_level-1]
             else
               nil
             end

    return parents, parent
  end

  def quater_detail_link(amount,quarter,subfield_name,item_code)
    attributes = {}

    attributes[:quarter] = quarter
    attributes[:subfield_name] = subfield_name if subfield_name.present?
    attributes[:item_code] = item_code if item_code.present?

    link_to annual_planification_quater_details_path, class: "annual-quarter-details", data:attributes do
      "#{currency_format(amount)}"
    end
  end


end

