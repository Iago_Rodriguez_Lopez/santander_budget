# == Schema Information
#
# Table name: items
#
#  id                         :integer          not null, primary key
#  code                       :string
#  description                :string
#  interpretation_ccc_id      :integer
#  interpretation_cargabal_id :integer
#  interpretation_ge_id       :integer
#  subfield_id                :integer
#  format_bank_id             :integer
#  format_group_id            :integer
#  format_spain_id            :integer
#  created_at                 :datetime
#  updated_at                 :datetime
#

class Item < ApplicationRecord

  include ActionView::Helpers
  include ActionView::Context
  include Rails.application.routes.url_helpers
  include ItemsHelper

  belongs_to :interpretation_ccc
  belongs_to :interpretation_cargabal
  belongs_to :interpretation_ge
  belongs_to :subfield
  belongs_to :format_bank
  belongs_to :format_group
  belongs_to :format_spain

  has_many :adjustments
  has_many :estimates
  has_many :theoric_adjustments
  has_many :base_adjustments


  has_many :accounts

  has_many :annual_base_adjustments

  validates_presence_of :code,
                        :description

  validates :code, uniqueness: true

  has_one :budget

  scope :by_format_spain, ->(items,spain_ids) {items.where("format_spain_id IN (?)",spain_ids)}
  scope :by_format_bank, ->(items,bank_ids) {items.where("format_bank_id IN (?)",bank_ids)}
  scope :by_format_group, ->(items,group_ids) {items.where("format_group_id IN (?)",group_ids)}
  scope :by_interpretation_cargabal, ->(items,cargabal_ids) {items.where("interpretation_cargabal_id IN (?)",cargabal_ids)}
  scope :by_interpretation_ccc, ->(items,ccc_ids) {items.where("interpretation_ccc_id IN (?)",ccc_ids)}
  scope :by_interpretation_ge, ->(items,ge_ids) {items.where("interpretation_ge_id IN (?)",ge_ids)}
  scope :by_subfield, ->(items,subfield_ids) {items.where("subfield_id IN (?)",subfield_ids)}
  #se le pasa un arreglo


  scope :format_spain, ->{where.not(format_spain_id: nil)}
  scope :interpretation_ccc, ->{where.not(interpretation_ccc_id:nil)}
  scope :interpretation_ge, ->{where.not(interpretation_ge_id: nil)}

  #before_save :try_cast_code
  #Ask if the code could be a float number

  def try_cast_code
    cast_result = Float(self.code) rescue nil && self.code.to_i
    self.code =  cast_result.present? ? cast_result.to_i.to_s : self.code
  end

  #scope :active, where(active: true)
  #TODO: Check where is used
  def self.search_existence(code)
      Item.find_by(code: code)
  end
  def self.get_subfields_ids
    Item.select(:subfield_id).where.not(subfield_id: nil).group(:subfield_id).pluck(:subfield_id)
  end

  def self.verify_account_id(account)
    item = Item.find_by(account_id: account.id)
    if item
      return true
    else
      return false
    end
  end


  def self.format_spain_struct
    format_spain_array = []
    formats_spain = format_spain_types
    formats_spain.each do |spain_format|
      format_spain_array << OpenStruct.new(:name => spain_format,:current_total=>0,:previous_total=>0,:annual_budget_total=>0)
    end
    return format_spain_array
  end

  def actions
    item_actions(self)
  end
end
