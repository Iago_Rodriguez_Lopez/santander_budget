# == Schema Information
#
# Table name: format_banks
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class FormatBank < ApplicationRecord
  has_many :items
  scope :id_by_name, -> (name){where(name:name)}

  def self.names_availables
    FormatBank.pluck(:name)
  end
end
