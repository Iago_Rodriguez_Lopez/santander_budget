# == Schema Information
#
# Table name: format_spains
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class FormatSpain < ApplicationRecord
  has_many :items
  scope :id_by_name, -> (name){where(name:name)}
  def self.names_availables
    FormatSpain.pluck(:name)
  end
end
