class TransactionsReport < ActiveRecord::Base
  self.table_name = "transactions_report"

  protected
  def readonly?
    true
  end
end