# == Schema Information
#
# Table name: cost_centers
#
#  id          :integer          not null, primary key
#  code        :string
#  name        :string
#  division    :string
#  area        :string
#  subarea     :string
#  created_at  :datetime
#  updated_at  :datetime
#  description :string
#  management  :string
#

class CostCenter < ApplicationRecord

  include ActionView::Helpers
  include ActionView::Context
  include Rails.application.routes.url_helpers
  include CostCentersHelper

  has_many :transactions, dependent: :delete_all

  validates_presence_of :code,
                      :name

  scope :by_division, ->(cost_centers,division) {cost_centers.where(division: division)}
  scope :by_management, ->(cost_centers,management) {cost_centers.where(management: management)}
  scope :by_area, ->(cost_centers,area) {cost_centers.where(area: area)}
  scope :by_subarea, ->(cost_centers,subarea) {cost_centers.where(subarea: subarea)}

  validate  :validate_uniq_code,
            :validate_uniq_code_name,
            :validate_uniq_code_name_description,
            :validate_uniq_code_name_description_management,
            :validate_uniq_code_name_description_management_division,
            :validate_uniq_code_name_description_management_division_area,
            :validate_uniq_code_name_description_management_division_area_subarea

  def validate_uniq_code
    result = CostCenter.where(code:self.code).where(name:[nil, ""]).where(description: [nil, ""]).where(management: [nil, ""]).where(division: [nil, ""]).where(area: [nil, ""]).where(subarea: [nil, ""]).first

    if result.present?
      self.errors.add(:base,"El código #{self.code} ya está siendo utilizado.")
    end
  end

  def validate_uniq_code_name
    result = CostCenter.where(code:self.code).where(name:self.name).where(description: [nil, ""]).where(management: [nil, ""]).where(division: [nil, ""]).where(area: [nil, ""]).where(subarea: [nil, ""]).first
    if result.present?
      self.errors.add(:base,"El código #{self.code} y el nombre #{self.name} ya están siendo utilizados.")
    end
  end

  def validate_uniq_code_name_description
    result = CostCenter.where(code:self.code).where(name:self.name).where(description: self.description).where(management: [nil, ""]).where(division: [nil, ""]).where(area: [nil, ""]).where(subarea: [nil, ""]).first
    if result.present?
      self.errors.add(:base,"El código #{self.code}, el nombre #{self.name} y la descripción #{self.description} ya están siendo utilizados.")
    end
  end

  def validate_uniq_code_name_description_management
    CostCenter.where(code:self.code).where(name:self.name).where(description: self.description).where(management: self.management).where(division: [nil, ""]).where(area: [nil, ""]).where(subarea: [nil, ""]).first
  end

  def validate_uniq_code_name_description_management_division
    CostCenter.where(code:self.code).where(name:self.name).where(description: self.description).where(management: self.management).where(division: self.division).where(area: [nil, ""]).where(subarea: [nil, ""]).first
  end

  def validate_uniq_code_name_description_management_division_area
    CostCenter.where(code:self.code).where(name:self.name).where(description: self.description).where(management: self.management).where(division: self.division).where(area: self.area).where(subarea: [nil, ""]).first
  end

  def validate_uniq_code_name_description_management_division_area_subarea
    CostCenter.where(code:self.code).where(name:self.name).where(description: self.description).where(management: self.management).where(division: self.division).where(area: self.area).where(subarea: self.subarea).first
  end

  def self.division_availables
    CostCenter.pluck(:division)
  end
  def self.management_availables
    CostCenter.pluck(:management)
  end
  def self.area_availables
    CostCenter.pluck(:area)
  end
  def self.subarea_availables
    CostCenter.pluck(:subarea)
  end

  def actions
    actions_links(self)
  end
end
