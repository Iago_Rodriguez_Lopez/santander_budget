# == Schema Information
#
# Table name: interpretation_cargabals
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class InterpretationCargabal < ApplicationRecord
  has_many :items
  scope :id_by_name, -> (name){where(name:name)}
  def self.names_availables
    InterpretationCargabal.pluck(:name)
  end
end
