# == Schema Information
#
# Table name: budgets
#
#  id         :integer          not null, primary key
#  period     :date
#  month      :integer
#  year       :integer
#  created_at :datetime
#  updated_at :datetime
#  amount     :float
#  item_id    :integer
#

class Budget < ApplicationRecord

    include ActionView::Helpers
    include ActionView::Context
    include Rails.application.routes.url_helpers
    include BudgetsHelper

    MONTHS = (1..12).map {|m| I18n.l(DateTime.parse(Date::MONTHNAMES[m]), format: "%B").capitalize }
    before_save :format_period

    validates :month, numericality:{only_integer:true,greater_than_or_equal_to:0},presence:true
    validates :year, numericality:{only_integer:true,greater_than_or_equal_to:0},presence:true
    
    #validates :amount, numericality: {greater_than_or_equal_to:0},presence: true
    #

    # validate :check_item_id, :on => [:create,:update]

    belongs_to :item

    #All periods begin the first of each month
    def period_end_to_month
        self.period.end_of_month
    end

    def period_dates
        return self.period,self.transform_end_to_month
    end

    def format_period
        if self.year.is_a? Numeric and self.month.is_a? Numeric and Date.valid_date?(self.year, self.month, 1)
            self.period = Date.new(self.year, self.month, 1)
        end
    end

    def assigned_budget
        if self.year.is_a? Numeric and self.month.is_a? Numeric
            budget = Budget.find_by(year:self.year, month:self.month)
            if budget.present?
                # self.errors.add(:base,"El presupuesto para el periodo selecionado ya fue asignado.")
            end
        end
    end

    def check_item_id
      item = Item.search_existence(self.code)
      if item.blank?
        self.errors.add(:base, "El item no existe")
        return false
      end
      return true
    end

    def self.get_years
      budgets = Budget.select(:year).uniq
      years = []
      budgets.each do |budget|
        years << budget.year
      end
      years = years.uniq
    end

    def self.years_available
        Budget.select(:year).group(:year).pluck(:year)
    end

    def amount_to_currency
      currency_format(self.amount)
    end
    def actions
      actions_links(self)
    end
end
