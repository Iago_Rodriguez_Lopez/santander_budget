class AnnualPlanificationReport < ActiveRecord::Base
  self.table_name = "annual_planification_report"

  TRANSACTIONS_COLUMNS = %w(january_transactions february_transactions march_transactions april_transactions may_transactions june_transactions july_transactions august_transactions september_transactions october_transactions november_transactions december_transactions year_transaction)
  BUDGET_COLUMNS = %w(january_budget february_budget march_budget april_budget may_budget june_budget july_budget august_budget september_budget october_budget november_budget december_budget year_budget)

  BUDGETED_COLUMS = %w(january_transactions_budgeted february_transactions_budgeted march_transactions_budgeted april_transactions_budgeted may_transactions_budgeted june_transactions_budgeted july_transactions_budgeted august_transactions_budgeted september_transactions_budgeted october_transactions_budgeted november_transactions_budgeted december_transactions_budgeted)

  def self.get_transactions_report_by(year,pivot_month,name)
    default_colums = ['subfield_name','item_code','item_description','ord']
    transactions_columns = self.select_all_transaction_columns(pivot_month)
    columns = (default_colums + transactions_columns)
    select_statement = columns.join(",")
    report = AnnualPlanificationReport.where(year:year,pivot_month:pivot_month,annual_planification_name:name).select(select_statement)
    return columns, report
  end
  
  def self.select_all_transaction_columns(pivot_month)
    selected_columns = []
    unless pivot_month == 1
      index_month = pivot_month-1
      (1..index_month).each do |month|
        selected_columns << TRANSACTIONS_COLUMNS[month-1]
      end
    end
    selected_columns
  end

  def self.get_quarters_string(pivot_month)
    quarters = {}

    if pivot_month > 0
      quarters[:q1] = pivot_month >0 && pivot_month <= 3 ? "q1_#{pivot_month}" : "q1"
    end
    if pivot_month > 3
      quarters[:q2] = pivot_month >3 && pivot_month <= 6 ? "q2_#{pivot_month}" : "q2"
    end
    if pivot_month > 6
      quarters[:q3] = pivot_month >6 && pivot_month <= 9 ? "q3_#{pivot_month}" : "q3"
    end
    if pivot_month > 9
      quarters[:q4] = pivot_month >9 && pivot_month <= 12 ? "q4_#{pivot_month}" : "q4"
    end
    quarters
  end

  def self.get_report_by(year,pivot_month,name)
    quarters = self.get_quarters_string(pivot_month)
    default_colums = ['subfield_name','item_code','item_description']
    quarters.each do |key, value|
      default_colums << value
    end
    default_colums << 'ord'

    transactions_columns = self.select_transaction_columns(pivot_month)
    budget_columns = self.select_budget_columns(pivot_month)

    budgeted_columns = self.select_budgeted_colums(pivot_month) 
    columns = (default_colums + transactions_columns + budget_columns + budgeted_columns)
    select_statement = columns.join(",")

    report = AnnualPlanificationReport.where(year:year,pivot_month:pivot_month,annual_planification_name:name).select(select_statement)
    return columns, report
  end

  def self.select_transaction_columns(pivot_month)
    index_month = pivot_month - 1

    selected_columns = if index_month == 0
                         [TRANSACTIONS_COLUMNS[index_month]]
                       elsif index_month > 1
                         previous_month = TRANSACTIONS_COLUMNS[index_month - 1]
                         [previous_month]
                       else
                         []
                       end

    selected_columns
  end

  def self.select_budget_columns(pivot_month)
    [BUDGET_COLUMNS[pivot_month]]
  end

  def self.select_budgeted_colums(pivot_month)
    index_month = pivot_month == 1 ? 0 : pivot_month - 1

    BUDGETED_COLUMS[index_month..-1]
  end

  def self.quater_detail_by(year,pivot_month,annual_planification_name,quarter,subfield_name = nil,item_code=nil)
    default_colums = ['subfield_name','item_code','item_description','ord']
    columns = self.transaction_columns_by(quarter,pivot_month)

    report = AnnualPlanificationReport.where(year:year,pivot_month:pivot_month,annual_planification_name:annual_planification_name)
    columns += default_colums
    default_answer = [nil, nil]
    return default_answer unless report.present?
    report = if subfield_name.present? && item_code.present?
               report.where(subfield_name:subfield_name,item_code:item_code)
             elsif subfield_name.present?
               report.where(subfield_name:subfield_name)
             else
              report
             end


    return default_answer unless report.present?

    return columns, report.select(columns)
  end

  def self.transaction_columns_by(quarter,pivot_month)
    pivot_month = pivot_month - 1 #because the month is from january (1) and here the columns are from january(0) 
    columns = if quarter == 'q1'
                if pivot_month <= 2
                  TRANSACTIONS_COLUMNS[0..pivot_month]
                else
                  TRANSACTIONS_COLUMNS[0..2]
                end
              elsif quarter == 'q2'
                if pivot_month <= 5
                  TRANSACTIONS_COLUMNS[3..pivot_month]
                else
                  TRANSACTIONS_COLUMNS[3..5]
                end
              elsif quarter == 'q3'
                if pivot_month <= 8
                  TRANSACTIONS_COLUMNS[6..pivot_month]
                else 
                  TRANSACTIONS_COLUMNS[6..8]
                end
              elsif quarter == 'q4'
                if pivot_month <=11
                  TRANSACTIONS_COLUMNS[9..pivot_month]
                else
                  TRANSACTIONS_COLUMNS[9..11]
                end
              else
                nil
              end
  end


  protected

  def readonly?
    true
  end
end