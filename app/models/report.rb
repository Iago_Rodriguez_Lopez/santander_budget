class Report
    include ActiveModel::Model

    MODELS_TYPES = {format_spain: FormatSpain.name.downcase,format_ccc:InterpretationCcc.name.downcase,local_gp:InterpretationGe.name.downcase,format_cargabal:InterpretationCargabal.name.downcase,format_bank:FormatBank.name.downcase,format_group:FormatGroup.name.downcase,format_subfield:Subfield.name.downcase,division:'division',management:'management',area:'area',subarea:'subarea'}

    ANALYSIS_PERIOD = 1.year
    def self.control(options)
      #Process paramenters
      month = options[:month_report]
      year = options[:year_report]
      model_type = options[:report_type]

      #Periods
      budget = Budget.where(month:month.to_i,year:year.to_i).first
      reference_date = budget.period.to_date
      start_previous_year,end_previous_year,start_current_year,end_current_year = self.yearly_report_periods(reference_date)

      #Calculations by year
      current_year_result = self.calculations(start_current_year,end_current_year,model_type)
      previous_year_result = self.calculations(start_previous_year,end_previous_year,model_type)

      #Calculations by month
      start_date_previous_monthly,end_date_previous_monthly,start_monthly_date,end_monthly_date = self.monthly_report_periods(reference_date)
      month_current_year = self.calculations(start_monthly_date,end_monthly_date,model_type)
      month_last_year = self.calculations(start_date_previous_monthly,end_date_previous_monthly,model_type)

      #Previous Month
      start_date_previous_month = (start_monthly_date - 1.month).beginning_of_month
      end_date_previous_month = start_date_previous_month.end_of_month
      previous_month = self.calculations(start_date_previous_month,end_date_previous_month,model_type)

      #adjustment_results
      year_adjustment_result = self.adjustment_results(model_type,start_previous_year,end_previous_year,start_current_year,end_current_year)
      month_adjustment_result = self.adjustment_results(model_type,start_date_previous_monthly,end_date_previous_monthly,start_monthly_date,end_monthly_date)
      previous_month_adjustment_result = self.adjustment_results(model_type,start_date_previous_month,end_date_previous_month,nil,nil )

      OpenStruct.new(current_year_result:current_year_result,previous_year_result:previous_year_result,month_current_year:month_current_year,month_last_year:month_last_year,previous_month:previous_month,
      year_adjustment_result:year_adjustment_result,month_adjustment_result:month_adjustment_result, previous_month_adjustment_result:previous_month_adjustment_result)

    end
    def self.detailed_control_report(options)
      start_date, end_date = self.periods_for_detailed_report(options[:start_year],options[:end_year])
      model_type = options[:report_type]
      self.calculations_for_detailed_report(start_date,end_date,model_type)
    end



    def self.adjustment_results(model_type,start_date_previous_year,end_date_previous_year,start_date_current_year,end_date_current_year)
      items,table_name = self.items_by(model_type)
      results = if items.present?
                  result = self.sql_adjustment_with_item(start_date_previous_year,end_date_previous_year,start_date_current_year,end_date_current_year,items)
                else
                  result = self.sql_adjustment(start_date_previous_year,end_date_previous_year,start_date_current_year,end_date_current_year)
                end
    end

    def self.periods_for_detailed_report(start_year,end_year)
      start_date = Date.new(start_year.to_i,1,1)
      end_date = Date.new(end_year.to_i,12,31)

      return start_date,end_date
    end

    def self.yearly_report_periods(reference_date)
      end_current_year = self.set_end_current_year(reference_date)
      start_current_year = self.set_start_current_year(end_current_year)
      start_previous_year = self.set_start_previous_year(end_current_year)
      end_previous_year = self.set_end_previous_year(end_current_year)
      return start_previous_year,end_previous_year,start_current_year,end_current_year
    end

    def self.set_end_current_year(reference_date)
      actual_year = DateTime.now.year
      actual_month = DateTime.now.month
      end_current_year = if actual_year == reference_date.year and actual_month == reference_date.month
                            Date.new(reference_date.year,reference_date.month,DateTime.now.day)
                         else
                            Date.new(reference_date.year,reference_date.month,reference_date.end_of_month.day)
                         end
    end

    def self.set_start_current_year(reference_date)
      reference_date - 1.year + 1.day
    end

    def self.set_end_previous_year(reference_date)
      reference_date - 1.year
    end

    def self.set_start_previous_year(reference_date)
      reference_date - 2.year + 1.day
    end

    def self.monthly_report_periods(reference_date)
      previous_year = reference_date - 1.year
      previous_start_date = previous_year.beginning_of_month
      previous_end_date = previous_year.end_of_month
      current_start_date = reference_date.beginning_of_month
      current_end_date = reference_date.end_of_month

      return previous_start_date,previous_end_date,current_start_date,current_end_date
    end

    def self.calculations(start_date,end_date,model_type)
      items,table_name = self.items_by(model_type)

      results = if items.present?
                  result = self.sql_control_report(start_date,end_date,items.ids,table_name)
                else
                  total_transaction = Transaction.where(expired_date:start_date..end_date).sum(:amount)
                  total_budget = Budget.where(period:start_date..end_date).sum(:amount)

                  result = [OpenStruct.new(concept:'Total',transaction_result:total_transaction,budget:total_budget,item_id:'',item_code:'',item_description:'')]

                end
    end

    def self.calculations_for_detailed_report(start_date,end_date,model_type)

      items,table_name = self.items_by(model_type)

      results = if items.present?
                  self.sql_detailed_report(start_date,end_date,items,table_name)
                else
                  items = Item.all
                  self.sql_detailed_report_all_items(start_date,end_date,items)
                end
    end



    def self.items_by(model_type)
      items = nil
      table_name = nil
      if model_type == MODELS_TYPES[:format_spain]
        items = Item.format_spain
        table_name = FormatSpain.table_name
      elsif model_type == MODELS_TYPES[:format_ccc]
        items = Item.interpretation_ccc
        table_name = InterpretationCcc.table_name
      elsif model_type == MODELS_TYPES[:local_gp]
        items = Item.interpretation_ge
        table_name = InterpretationGe.table_name
      end

      return items,table_name
    end

    def self.set_consultation_dates(year)
      actual_start_date = Date.new(year,1,1)
      actual_end_date = Date.new(year,12,31)
      previous_start_date = actual_start_date - 1.year
      previous_end_date = actual_end_date - 1.year
      previous2_end_date = actual_start_date - 2.year
      previous2_start_date = actual_end_date - 2.year
      return actual_start_date,actual_end_date,previous_start_date,previous_end_date,previous2_start_date,previous2_end_date
    end

    def self.consultation(options)
      year = options[:year_report].to_i
      actual_start_date,actual_end_date,previous_start_date,previous_end_date,previous2_start_date,previous2_end_date = set_consultation_dates(year)
      model_types = nil
      if options[:format_options].present?
        model_types = Hash.new
        options[:format_options].each do |key,value|
          model_types["#{value[:format_type]}"] = value[:option_select]
        end
      end
      items = self.items_consultation_by(model_types)
      cost_centers = self.cost_centers_consultation_by(model_types)
      if items.present?
        actual_year_results = sql_consultation_report(items,actual_start_date,actual_end_date,cost_centers)
        previous_year_results = sql_consultation_report(items,previous_start_date,previous_end_date,cost_centers)
        previous2_year_results = sql_consultation_report(items,previous2_start_date,previous2_end_date,cost_centers)
        results = self.create_chart_data(actual_year_results,previous_year_results,previous2_year_results)
        return OpenStruct.new(current_year_results:results,previous_year_results:previous_year_results,previous2_year_results:previous2_year_results)
      end
    end



    def self.create_chart_data(results,previous_year_results,previous2_year_results)
      resultado = []
      results.each_with_index do |result,index|
        #each month budget and transaction verify
        january_budget,january_transaction = result['january_budget'].present? ? result['january_budget']:0,result['january_transaction'].present? ? result['january_transaction']:0
        february_budget,february_transaction = result['february_budget'].present? ? result['february_budget']:0, result['february_transaction'].present? ? result['february_transaction']:0
        march_budget,march_transaction= result['march_budget'].present? ? result['march_budget']:0,result['march_transaction'].present? ? result['march_transaction']:0
        april_budget,april_transaction = result['april_budget'].present? ? result['april_budget']:0,result['april_transaction'].present? ? result['april_transaction']:0
        may_budget,may_transaction= result['may_budget'].present? ? result['may_budget']:0,result['may_transaction'].present? ? result['may_transaction']:0
        june_budget,june_transaction = result['june_budget'].present? ? result['june_budget']:0,result['june_transaction'].present? ? result['june_transaction']:0
        july_budget,july_transaction = result['july_budget'].present? ? result['july_budget']:0,result['july_transaction'].present? ? result['july_transaction']:0
        august_budget,august_transaction = result['august_budget'].present? ? result['august_budget']:0,result['august_transaction'].present? ? result['august_transaction']:0
        september_budget,september_transaction = result['september_budget'].present? ? result['september_budget']:0,result['september_transaction'].present? ? result['september_transaction']:0
        october_budget,october_transaction = result['october_budget'].present? ? result['october_budget']:0,result['october_transaction'].present? ? result['october_transaction']:0
        november_budget,november_transaction = result['november_budget'].present? ? result['november_budget']:0,result['november_transaction'].present? ? result['november_transaction']:0
        december_budget,december_transaction = result['december_budget'].present? ? result['december_budget']:0,result['december_transaction'].present? ? result['december_transaction']:0
        #average information
        budget_average = average([january_budget,february_budget,march_budget,april_budget,may_budget,june_budget,july_budget,august_budget,september_budget,october_budget,november_budget,december_budget])
        transaction_average = average([january_transaction,february_transaction,march_transaction,april_transaction,may_transaction,june_transaction,july_transaction,august_transaction,september_transaction,october_transaction,november_transaction,december_transaction])

        #create the chart data
        anual_budget_transaction = {
          labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
          datasets: [
            {
                label: "Presupuesto",
                backgroundColor: "rgba(255, 99, 132, 0.2)",
                borderColor: "rgba(255, 99, 132, 1)",
                data: [january_budget,february_budget,march_budget,april_budget,may_budget,june_budget,july_budget,august_budget,
                      september_budget,october_budget,november_budget,december_budget]
            },
            {
                label: "Gasto",
                backgroundColor: "rgba(151,187,205,0.2)",
                borderColor: "rgba(151,187,205,1)",
                data: [january_transaction,february_transaction,march_transaction,april_transaction,may_transaction,june_transaction,july_transaction,
                      august_transaction,september_transaction,october_transaction,november_transaction,december_transaction]
            }
          ]
        }
        difference_budget_transaction = {
          labels: ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"],
          datasets: [
            {
                label: "Diferencia presupuesto-Gastos",
                backgroundColor: "rgba(51,153,255,0.2)",
                borderColor: "rgba(51,153,255,1)",
                data: [difference_format(january_budget,january_transaction),difference_format(february_budget,february_transaction),difference_format(march_budget,march_transaction),
                      difference_format(april_budget,april_transaction),difference_format(may_budget,may_transaction),difference_format(june_budget,june_transaction),
                      difference_format(july_budget,july_transaction),difference_format(august_budget,august_transaction),difference_format(september_budget,september_transaction),
                      difference_format(october_budget,october_transaction),difference_format(november_budget,november_transaction),difference_format(december_budget,december_transaction)]
            }
          ]
        }
        difference_year_budget_transaction  = {
          labels: [previous2_year_results[index]['year'],previous_year_results[index]['year'],result['year']],
          datasets: [
            {
                label: "Gastos",
                backgroundColor: "rgba(255, 99, 132, 0.2)",
                borderColor: "rgba(255, 99, 132, 1)",
                data: [previous2_year_results[index]['year_transaction'].present? ? previous2_year_results[index]['year_transaction']:0,
                       previous_year_results[index]['year_transaction'].present? ? previous_year_results[index]['year_transaction']:0,
                      result['year_transaction'].present? ? result['year_transaction']:0]
            },
            {
                label: "Presupuesto",
                backgroundColor: "rgba(151,187,205,0.2)",
                borderColor: "rgba(151,187,205,1)",
                data: [previous2_year_results[index]['year_budget'].present? ? previous2_year_results[index]['year_budget']:0,
                       previous_year_results[index]['year_budget'].present? ? previous_year_results[index]['year_budget']:0,
                      result['year_budget'].present? ? result['year_budget']:0]
            }
          ]
        }
        result['anual_budget_transaction'] = anual_budget_transaction
        result['difference_budget_transaction'] = difference_budget_transaction
        result['difference_year_budget_transaction'] = difference_year_budget_transaction
        result['budget_average'] = budget_average
        result['transaction_average'] = transaction_average
        resultado<< result
      end
      resultado
    end

    def self.items_consultation_by(formats)
      items = Item.all

      if formats.present?
        if formats[MODELS_TYPES[:format_bank]] != ""
          items = Item.by_format_bank(items,FormatBank.id_by_name(formats[MODELS_TYPES[:format_bank]]).ids)
        end
        if formats[MODELS_TYPES[:format_group]] != ""
          items = Item.by_format_group(items,FormatGroup.id_by_name(formats[MODELS_TYPES[:format_group]]).ids)
        end
        if formats[MODELS_TYPES[:format_spain]] != ""
          items = Item.by_format_spain(items,FormatSpain.id_by_name(formats[MODELS_TYPES[:format_spain]]).ids)
        end
        if formats[MODELS_TYPES[:format_cargabal]] != ""
          items = Item.by_interpretation_cargabal(items,InterpretationCargabal.id_by_name(formats[MODELS_TYPES[:format_cargabal]]).ids)
        end
        if formats[MODELS_TYPES[:format_ccc]] != ""
          items = Item.by_interpretation_ccc(items,InterpretationCcc.id_by_name(formats[MODELS_TYPES[:format_ccc]]).ids)
        end
        if formats[MODELS_TYPES[:local_gp]] != ""
          items = Item.by_interpretation_ge(items,InterpretationGe.id_by_name(formats[MODELS_TYPES[:local_gp]]).ids)
        end
        if formats[MODELS_TYPES[:format_subfield]] != ""
          items = Item.by_subfield(items,Subfield.id_by_name(formats[MODELS_TYPES[:format_subfield]]).ids)
        end
      end
      return items
    end

    def self.cost_centers_consultation_by(formats)
      cost_centers = CostCenter.all
      if formats.present?
        if formats[MODELS_TYPES[:division]].present?
          cost_centers = CostCenter.by_division(cost_centers,formats[MODELS_TYPES[:division]])
        end
        if formats[MODELS_TYPES[:management]].present?
          cost_centers = CostCenter.by_management(cost_centers,formats[MODELS_TYPES[:management]])
        end
        if formats[MODELS_TYPES[:area]].present?
          cost_centers = CostCenter.by_area(cost_centers,formats[MODELS_TYPES[:area]])
        end
        if formats[MODELS_TYPES[:subarea]].present?
          cost_centers = CostCenter.by_subarea(cost_centers,formats[MODELS_TYPES[:subarea]])
        end
      end
      return cost_centers
    end

    def self.sql_consultation_report(items,start_date,end_date,cost_centers)
      item_ids = items.ids.join(',')
      cost_center_ids = cost_centers.ids.join(',')
      sql_budget_amounts = self.sql_budget_amounts(start_date,end_date)
      sql_transaction_amounts = self.sql_transaction_amounts(start_date,end_date,cost_center_ids)
      sql = p %{
        SELECT items.code as code,
        items.description as name,
        #{start_date.year} as year,
        january_budget,
        january_transaction,
        february_budget,
        february_transaction,
        march_budget,
        march_transaction,
        april_budget,
        april_transaction,
        may_budget,
        may_transaction,
        june_budget,
        june_transaction,
        july_budget,
        july_transaction,
        august_budget,
        august_transaction,
        september_budget,
        september_transaction,
        october_budget,
        october_transaction,
        november_budget,
        november_transaction,
        december_budget,
        december_transaction,
        year_budget,
        year_transaction
        from items
        LEFT JOIN
        (
          #{sql_transaction_amounts}
        ) transaction_detail on (transaction_detail.code = items.code)
        LEFT JOIN(
          #{sql_budget_amounts}
        ) budget_detail on (budget_detail.code = items.code)
        WHERE items.id IN (#{item_ids})
        ORDER BY items.id
        }.gsub(/\s+/, " ").strip
      self.execute_pg_query(sql)

    end


    def self.sql_transaction_amounts(start_date,end_date,cost_center_ids)
      p %{
        SELECT
        items.code as CODE ,
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 1.0 THEN transactions.amount ELSE 0 END) AS "january_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 2.0 THEN transactions.amount ELSE 0 END) AS "february_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 3.0 THEN transactions.amount ELSE 0 END) AS "march_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 4.0 THEN transactions.amount ELSE 0 END) AS "april_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 5.0 THEN transactions.amount ELSE 0 END) AS "may_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 6.0 THEN transactions.amount ELSE 0 END) AS "june_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 7.0 THEN transactions.amount ELSE 0 END) AS "july_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 8.0 THEN transactions.amount ELSE 0 END) AS "august_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 9.0 THEN transactions.amount ELSE 0 END) AS "september_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 10.0 THEN transactions.amount ELSE 0 END) AS "october_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 11.0 THEN transactions.amount ELSE 0 END) AS "november_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 12.0 THEN transactions.amount ELSE 0 END) AS "december_transaction",
        SUM(transactions.amount) AS "year_transaction"
        FROM transactions
        INNER JOIN accounts ON accounts.id = transactions.account_id
        INNER JOIN cost_centers ON cost_centers.id = transactions.cost_center_id
        INNER JOIN items ON items.id = accounts.item_id
        WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD') AND transactions.cost_center_id IN (#{cost_center_ids})
        GROUP BY items.code
      }
    end
    def self.sql_budget_amounts(start_date,end_date)
      p %{
        SELECT
        items.code as CODE,
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 1.0 THEN budgets.amount ELSE 0 END) AS "january_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 2.0 THEN budgets.amount ELSE 0 END) AS "february_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 3.0 THEN budgets.amount ELSE 0 END) AS "march_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 4.0 THEN budgets.amount ELSE 0 END) AS "april_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 5.0 THEN budgets.amount ELSE 0 END) AS "may_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 6.0 THEN budgets.amount ELSE 0 END) AS "june_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 7.0 THEN budgets.amount ELSE 0 END) AS "july_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 8.0 THEN budgets.amount ELSE 0 END) AS "august_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 9.0 THEN budgets.amount ELSE 0 END) AS "september_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 10.0 THEN budgets.amount ELSE 0 END) AS "october_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 11.0 THEN budgets.amount ELSE 0 END) AS "november_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 12.0 THEN budgets.amount ELSE 0 END) AS "december_budget",
        SUM(budgets.amount) AS "year_budget"
        FROM budgets
        INNER JOIN items ON items.id = budgets.item_id
        WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD')
        GROUP BY items.code
      }
    end

    def self.sql_adjustment(start_date_previous_year,end_date_previous_year,start_date_current_year,end_date_current_year)
      sql = p %{
        SELECT adjustments.name as concept,
        sum(CASE WHEN to_date('#{start_date_previous_year}','YYYY-MM-DD') <= adjustments.date  AND adjustments.date <= to_date('#{end_date_previous_year}','YYYY-MM-DD') THEN adjustments.amount END) AS previous_adjustment_results,
        sum(CASE WHEN to_date('#{start_date_previous_year}','YYYY-MM-DD') <= adjustments.date AND adjustments.date <= to_date('#{end_date_previous_year}','YYYY-MM-DD') THEN estimates.amount END) AS previous_estimates_results,
        sum(CASE WHEN to_date('#{start_date_current_year}','YYYY-MM-DD') <= adjustments.date  AND adjustments.date <= to_date('#{end_date_current_year}','YYYY-MM-DD') THEN adjustments.amount END) AS current_adjustment_results,
        sum(CASE WHEN to_date('#{start_date_current_year}','YYYY-MM-DD') <= adjustments.date  AND adjustments.date <= to_date('#{end_date_current_year}','YYYY-MM-DD') THEN estimates.amount END) AS current_estimates_results,
        adjustments.adjustment_type AS adjustment_type
        from adjustments
        INNER JOIN estimates ON estimates.name = adjustments.name
        GROUP BY adjustments.name, adjustment_type
        ORDER BY adjustment_type DESC
      }.gsub(/\s+/, " ").strip
      self.execute_pg_query(sql)
    end

    def self.sql_adjustment_with_item(start_date_previous_year,end_date_previous_year,start_date_current_year,end_date_current_year,items)
      item_ids = items.ids.join(',')
      sql = p %{
        SELECT adjustments.name as concept,
        sum(CASE WHEN to_date('#{start_date_previous_year}','YYYY-MM-DD') <= adjustments.date  AND adjustments.date <= to_date('#{end_date_previous_year}','YYYY-MM-DD') THEN adjustments.amount END) AS previous_adjustment_results,
        sum(CASE WHEN to_date('#{start_date_previous_year}','YYYY-MM-DD') <= adjustments.date AND adjustments.date <= to_date('#{end_date_previous_year}','YYYY-MM-DD') THEN estimates.amount END) AS previous_estimates_results,
        sum(CASE WHEN to_date('#{start_date_current_year}','YYYY-MM-DD') <= adjustments.date  AND adjustments.date <= to_date('#{end_date_current_year}','YYYY-MM-DD') THEN adjustments.amount END) AS current_adjustment_results,
        sum(CASE WHEN to_date('#{start_date_current_year}','YYYY-MM-DD') <= adjustments.date  AND adjustments.date <= to_date('#{end_date_current_year}','YYYY-MM-DD') THEN estimates.amount END) AS current_estimates_results,
        adjustments.adjustment_type AS adjustment_type
        from adjustments
        INNER JOIN estimates ON estimates.name = adjustments.name
        INNER JOIN items ON items.id = adjustments.item_id
        WHERE items.id IN (#{item_ids})
        GROUP BY adjustments.name, adjustment_type
        ORDER BY adjustment_type DESC
      }.gsub(/\s+/, " ").strip
      self.execute_pg_query(sql)
    end

    def self.sql_control_report(start_date,end_date,item_ids,table_name)
      item_ids = item_ids.join(',')
      item_field = Item.column_names.select{ |field| field.include?(table_name.singularize)}.pop()
      sql = p %{
        SELECT #{table_name}.name as concept,sum(transaction_detail.RESULT) as transaction_result,sum(budget_detail.BUDGET) as budget
        from #{table_name}
        inner join items on items.#{item_field} = #{table_name}.id
        LEFT JOIN
        (
          SELECT items.id as ID ,sum(transactions.amount) as RESULT
          FROM transactions
          INNER JOIN accounts ON accounts.id = transactions.account_id
          INNER JOIN items ON items.id = accounts.item_id
          WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD')
          GROUP BY items.id
        ) transaction_detail on (transaction_detail.id = items.id)
        LEFT JOIN(
          SELECT items.id as ID,sum(budgets.amount) as BUDGET
          FROM budgets
          INNER JOIN items ON items.id = budgets.item_id
          WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD')
          GROUP BY items.id
        ) budget_detail on (budget_detail.id = items.id)
        WHERE items.id IN (#{item_ids})
        GROUP BY concept
        ORDER BY concept
        }.gsub(/\s+/, " ").strip
        self.execute_pg_query(sql)
    end


    def self.sql_detailed_report(start_date,end_date,items,table_name)
      item_ids = items.ids.join(',')
      item_field = Item.column_names.select{ |field| field.include?(table_name.singularize)}.pop()
      sql_transaction_detailed = self.sql_transaction_detailed(start_date,end_date)
      sql_budget_detailed = self.sql_budget_detailed(start_date,end_date)
      sql = p %{
        SELECT #{table_name}.name as concept,
        transaction_year AS year,
        sum(january_budget) as january_budget,
        sum(january_transaction)  as january_transaction,
        sum(february_budget) as february_budget,
        sum(february_transaction) as february_transaction,
        sum(march_budget) as march_budget,
        sum(march_transaction) as march_transaction,
        sum(april_budget) as april_budget,
        sum(april_transaction) as april_transaction,
        sum(may_budget) as may_budget,
        sum(may_transaction) as may_transaction,
        sum(june_budget) as june_budget,
        sum(june_transaction) as june_transaction,
        sum(july_budget) as july_budget,
        sum(july_transaction) as july_transaction,
        sum(august_budget) as august_budget,
        sum(august_transaction) as august_transaction,
        sum(september_budget) as september_budget,
        sum(september_transaction) as september_transaction,
        sum(october_budget) as october_budget,
        sum(october_transaction) as october_transaction,
        sum(november_budget) as november_budget,
        sum(november_transaction) as november_transaction,
        sum(december_budget) as december_budget,
        sum(december_transaction) as december_transaction
        from #{table_name}
        inner join items on items.#{item_field} = #{table_name}.id
        LEFT JOIN
        (
          #{sql_transaction_detailed}
        ) transaction_detail on (transaction_detail.code = items.code)
        LEFT JOIN(
          #{sql_budget_detailed}
        ) budget_detail on (budget_detail.code = items.code)
        WHERE items.id IN (#{item_ids}) And transaction_year = budget_year
        GROUP BY concept, year
        ORDER BY year
        }.gsub(/\s+/, " ").strip
        self.execute_pg_query(sql)
    end

    def self.sql_detailed_report_all_items(start_date,end_date,items)
      item_ids = items.ids.join(',')
      sql_transaction_detailed = self.sql_transaction_detailed(start_date,end_date)
      sql_budget_detailed = self.sql_budget_detailed(start_date,end_date)
      sql = p %{
        SELECT items.code as concept,
        items.description as name,
        transaction_year AS year,
        january_budget,
        january_transaction,
        february_budget,
        february_transaction,
        march_budget,
        march_transaction,
        april_budget,
        april_transaction,
        may_budget,
        may_transaction,
        june_budget,
        june_transaction,
        july_budget,
        july_transaction,
        august_budget,
        august_transaction,
        september_budget,
        september_transaction,
        october_budget,
        october_transaction,
        november_budget,
        november_transaction,
        december_budget,
        december_transaction
        from items
        LEFT JOIN
        (
          #{sql_transaction_detailed}
        ) transaction_detail on (transaction_detail.code = items.code)
        LEFT JOIN(
          #{sql_budget_detailed}
        ) budget_detail on (budget_detail.code = items.code)
        WHERE items.id IN (#{item_ids}) And transaction_year = budget_year
        ORDER BY year,items.code
        }.gsub(/\s+/, " ").strip
      self.execute_pg_query(sql)

    end

    def self.sql_transaction_detailed(start_date,end_date)
      p %{
        SELECT items.code as CODE ,
        EXTRACT(YEAR FROM transactions.expired_date) AS transaction_year,
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 1.0 THEN transactions.amount ELSE 0 END) AS "january_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 2.0 THEN transactions.amount ELSE 0 END) AS "february_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 3.0 THEN transactions.amount ELSE 0 END) AS "march_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 4.0 THEN transactions.amount ELSE 0 END) AS "april_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 5.0 THEN transactions.amount ELSE 0 END) AS "may_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 6.0 THEN transactions.amount ELSE 0 END) AS "june_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 7.0 THEN transactions.amount ELSE 0 END) AS "july_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 8.0 THEN transactions.amount ELSE 0 END) AS "august_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 9.0 THEN transactions.amount ELSE 0 END) AS "september_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 10.0 THEN transactions.amount ELSE 0 END) AS "october_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 11.0 THEN transactions.amount ELSE 0 END) AS "november_transaction",
        SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 12.0 THEN transactions.amount ELSE 0 END) AS "december_transaction"
        FROM transactions
        INNER JOIN accounts ON accounts.id = transactions.account_id
        INNER JOIN items ON items.id = accounts.item_id
        WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD')
        GROUP BY items.code, transaction_year
      }
    end

    def self.sql_budget_detailed(start_date,end_date)
      p %{
        SELECT items.code as CODE,
        EXTRACT(YEAR FROM budgets.period) AS budget_year,
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 1.0 THEN budgets.amount ELSE 0 END) AS "january_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 2.0 THEN budgets.amount ELSE 0 END) AS "february_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 3.0 THEN budgets.amount ELSE 0 END) AS "march_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 4.0 THEN budgets.amount ELSE 0 END) AS "april_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 5.0 THEN budgets.amount ELSE 0 END) AS "may_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 6.0 THEN budgets.amount ELSE 0 END) AS "june_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 7.0 THEN budgets.amount ELSE 0 END) AS "july_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 8.0 THEN budgets.amount ELSE 0 END) AS "august_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 9.0 THEN budgets.amount ELSE 0 END) AS "september_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 10.0 THEN budgets.amount ELSE 0 END) AS "october_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 11.0 THEN budgets.amount ELSE 0 END) AS "november_budget",
        SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 12.0 THEN budgets.amount ELSE 0 END) AS "december_budget"
        FROM budgets
        INNER JOIN items ON items.id = budgets.item_id
        WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD')
        GROUP BY items.code, budget_year
      }
    end


    def self.execute_pg_query(query)
      #Source: https://stackoverflow.com/questions/25331778/getting-typed-results-from-activerecord-raw-sql
      #Answer by : Ramfjord
      conn = ActiveRecord::Base.connection
      @type_map ||= PG::BasicTypeMapForResults.new(conn.raw_connection)

      res = conn.execute(query)
      res.type_map = @type_map
      res.each do |r| puts r end
    end

    def self.years_availables
      if Budget.first.present?
        first_year = Budget.order("period").first.year 
        last_year = DateTime.now.year
        amount_years = last_year-first_year+1
        amount_years.times.map do |index|
          [first_year+index,first_year+index]
        end
      end
    end
    def self.difference_format(number1,number2)
      if number1.is_a? Numeric and number2.is_a? Numeric
          (number1-number2)
      else
          '-'
      end
    end
    def self.average(array)
      average = 0
      count = 0
      array.each do |number|
        if number.is_a? Numeric
          average +=number
        else
          average +=0
        end
        count +=1
      end
      average/count
    end

end
