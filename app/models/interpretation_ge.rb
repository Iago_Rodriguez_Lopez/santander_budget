# == Schema Information
#
# Table name: interpretation_ges
#
#  id         :integer          not null, primary key
#  code       :string
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class InterpretationGe < ApplicationRecord
  has_many :items
  scope :id_by_name, -> (name){where(name:name)}
  def self.names_availables
    InterpretationGe.pluck(:name)
  end
end
