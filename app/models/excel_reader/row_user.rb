class ExcelReader::RowUser < ExcelReader::Row
  attr_accessor :rut,
                :dv,
                :name,
                :last_name,
                :cost_center,
                :department,
                :division,
                :area,
                :subarea
  validates_presence_of :rut,
                        :dv,
                        :name,
                        :last_name,
                        :cost_center,
                        :department,
                        :division,
                        :area,
                        :subarea
  def self.get_format
    {
      rut:0,
      dv:1,
      name:2,
      last_name:3,
      cost_center:4,
      department:5,
      division:6,
      area:7,
      subarea:8
    }
  end

  def user_params
    {
      rut:self.rut,
      dv:self.dv,
      name:self.name.strip,
      last_name:self.last_name.strip,
      cost_center:self.cost_center.strip,
      department:self.department.strip,
      division:self.division.strip,
      area:self.area.strip,
      subarea:self.subarea.strip

    }
  end

  def self.validate_multiple_rows(rows)
    # TODO: Retornar la row con error si existe una con error
    # En el caso de Cost Center no se requiere hacer ninguna validación a nivel agregado
    return true
  end

  def self.after_multiple_rows_validation(rows)
    # esta variable se transforma en un Row
    # si hay un error al guardar algún centro de costo y retorna la fila que tuvo problemas
    row_return_values = OpenStruct.new
    row_return_values.row_with_error = true
    row_return_values.news_counter = 0
    row_return_values.edits_counter = 0
    #Check amount of data
    check_data = check_amount_of_data(rows,row_return_values)
    if check_data.present?
      return check_data
    end
    User.transaction do
      rows.each do |row|
        user = User.find_by(rut: row.rut,dv: row.dv)
        if user.blank?
          user = User.new
          row_return_values.news_counter +=1
        else
          row_return_values.edits_counter +=1
        end
        user.attributes =row.user_params

        unless user.save
          row.copy_errors(user)
          row_return_values.row_with_error = row
          ExcelReader::RowUser.rollback
        end
      end
    end

    return row_return_values
  end



end
