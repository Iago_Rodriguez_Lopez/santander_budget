class ExcelReader::RowBudget < ExcelReader::Row
  attr_accessor   :item_code,
                  :item_name,
                  :january,
                  :february,
                  :march,
                  :april,
                  :may,
                  :june,
                  :july,
                  :august,
                  :september,
                  :october,
                  :november,
                  :december

  validates_presence_of :item_code,
                        :item_name,
                        :january,
                        :february,
                        :march,
                        :april,
                        :may,
                        :june,
                        :july,
                        :august,
                        :september,
                        :october,
                        :november,
                        :december

  def self.get_format
    {
      item_code:0,
      item_name:1,
      january:2,
      february:3,
      march:4,
      april:5,
      may:6,
      june:7,
      july:8,
      august:9,
      september:10,
      october:11,
      november:12,
      december:13
    }
  end



  def get_month(month_id)
    months = [self.january,self.february,self.march,self.april,self.may,self.june,
      self.july,self.august,self.september,self.october,self.november,self.december]
    return months[month_id-1]
  end

  def month_budget_params(year,month_id,item)

    {
        item_id: item.is_a?(Item) ? item.id : nil,
        year: year.to_i,
        month: month_id.to_i,
        period: Date.new(year.to_i,month_id.to_i,1),
        amount: self.get_month(month_id.to_i)
    }
  end
  def self.read_spreadsheet(file,year)
    spreadsheet = open_spreadsheet(file)
    rows = []
    # Run validation for each row
    (2..spreadsheet.last_row).each do |number|
      row = self.create_from_raw_row(spreadsheet.row(number), number)
      return row unless row.valid?
      rows << row
    end

    # Run multiple validation for rows
    row = self.validate_multiple_rows(rows)

    if row.is_a?(ExcelReader::Row)
      return row
    else
      return self.after_multiple_rows_validation(rows,year)
    end

  end
  def self.validate_multiple_rows(rows)
    return true
  end

  def check_item_id
    item = Item.search_existence(self.item_code)
    if item.blank?
      self.add_custom_error(:base, "El codigo #{self.item_code} no existe.")
      item = true
    end
    return item
  end
  def create_month_budget(year,month_id,row_return_values,item)
    # aca tengo que seguir
    month_budget = OpenStruct.new
    row_with_error = true
    budget = Budget.find_by(year:year.to_i, month:month_id, item_id:item.id)
    if budget.blank?
      budget = Budget.new
      row_return_values.news_counter +=1
    else
      row_return_values.edits_counter +=1
    end

    budget.attributes = month_budget_params(year,month_id,item)
    budget.save!

    month_budget.budget = budget
    month_budget.row_with_error = row_with_error
    month_budget.row_return_values = row_return_values
    return month_budget
  end

  def self.after_multiple_rows_validation(rows,year)
    row_return_values = OpenStruct.new
    row_return_values.row_with_error = true
    row_return_values.news_counter = 0
    row_return_values.edits_counter = 0

    #Check amount of data
    check_data = check_amount_of_data(rows,row_return_values)
    if check_data.present?
      return check_data
    end
    
    Budget.transaction do
      rows.each do |row|
        item = row.check_item_id

        #Create all the budget with the month
        months = []
        (1..12).each do |i|
          month = row.create_month_budget(year,i,row_return_values,item)
          row_return_values = month.row_return_values
          months << month
        end
        #Check errors
        if item == true
          row.copy_errors(months[0].budget) #choose one month to copy the errors
          row_return_values.row_with_error = row
        end
        months.each do |month|
          if month.row_with_error.is_a?(ExcelReader::Row) #check if all the months are well created
            row_return_values.row_with_error = row
            ExcelReader::RowBudget.rollback #if it finds an error, the row triggers a rollback
          end
        end

        #we found an errors
        if row_return_values.row_with_error.is_a?(ExcelReader::Row)
          ExcelReader::RowBudget.rollback
        end
      end
    end
        #Everything is ok
    return row_return_values
  end
end
