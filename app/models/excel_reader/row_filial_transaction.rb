class ExcelReader::RowFilialTransaction < ExcelReader::Row

  attr_accessor :code_account,
                :code_item,
                :amount,
                :execution_date,
                :code_cost_center,
                :description,
                :enterprise_code,
                :enterprise_name

  validates_presence_of :code_account,
                :amount,
                :execution_date,
                :code_cost_center

  validate :check_execution_date

  def check_format_amount
      unless amount.is_a? Numeric && amount < 0
        errors.add(:base, I18n.t("excel_readers.errors.filial_reader.amount",amount:amount))
      end
  end

  def check_execution_date
    check_format_date(:execution_date,self.execution_date)
  end

  def check_format_date(attribute,date)
      if !date.is_a?(Date) && !date.is_a?(DateTime) && date.is_a?(String)
        begin
          Date.strptime(date,"%d-%m-%Y %H:%M:%S")
        rescue
          self.errors.add(:base.to_sym,I18n.t("excel_readers.errors.filial_reader.#{attribute.to_s}",date:date))
        end
      end
  end

  def check_account(account,code)
    check('account',account,code)
  end

  def check_item(item,code)
    check('item',item,code)
  end

  def check_cost_center(cost_center,code)
    check('cost_center',cost_center,code)
  end

  def check(attribute,object,code)
    if object.blank?
      self.add_custom_error(:base,I18n.t("excel_readers.errors.filial_reader.#{attribute}",code:code))
      return true
    end
  end

  def check_belongs_to_account(item,account)
    if account.present?
      if account.item != item
        self.add_custom_error(:base,I18n.t("excel_readers.errors.filial_reader.item_consistency",given_item:item.code,real_item:account.item.code))
        return true
      end
    end
  end

  def self.get_format
    {
      code_account:0,
      code_item:1,
      execution_date:2,
      amount:3,
      code_cost_center:4,
      description:5,
      enterprise_code:6,
      enterprise_name:7
    }
  end

  def filial_transaction_params(account,item,cost_center)
    {
      account:account,
      expired_date:self.execution_date,#Date used for accounting calculations
      amount:self.amount.to_i,
      cost_center:cost_center,
      source: I18n.t("options.models.transaction.filial"),
      comment:self.description,
      enterprise_code: self.enterprise_code,
      enterprise_name: self.enterprise_name
    }
  end

  def self.validate_multiple_rows(rows)
    # TODO: Retornar la row con error si existe una con error
    # En el caso de Cost Center no se requiere hacer ninguna validación a nivel agregado
    return true
  end

  def self.after_multiple_rows_validation(rows)
    # esta variable se transforma en un Row
    # si hay un error al guardar algún centro de costo y retorna la fila que tuvo problemas
    row_return_values = OpenStruct.new
    row_return_values.row_with_error = true
    row_return_values.news_counter = 0
    row_return_values.edits_counter = 0
    #Check amount of data
    check_data = check_amount_of_data(rows,row_return_values)
    if check_data.present?
      return check_data
    end
    
    Transaction.transaction do
      rows.each do |row|
        #find account
        account = Account.find_by_code(row.code_account)
        if row.check_account(account,row.code_account)
          row_return_values.row_with_error = row
        end

        #Find item only if it is present
        if row.code_item.present?
          item = Item.find_by_code(row.code_item)
          if row.check_item(item,row.code_item)
            row_return_values.row_with_error = row
          end

          if item.present? && row.check_belongs_to_account(item,account)
            row_return_values.row_with_error = row
          end
        end

        #Find Cost Center
        cost_center = CostCenter.find_by_code(row.code_cost_center)
        if row.check_cost_center(cost_center,row.code_cost_center)
          row_return_values.row_with_error = row
        end

        #This allow to collect all errors together instead one by one.
        if row_return_values.row_with_error.is_a?(ExcelReader::Row)
          ExcelReader::RowBankTransaction.rollback
        end

        #Everything is ok, you can save now the transaction.
        # transaction = Transaction.find_by(account: account, cost_center: cost_center)
        transaction = nil
        if transaction.blank?
          transaction = Transaction.new
          row_return_values.news_counter += 1
        else
          row_return_values.edits_counter += 1
        end
        transaction.attributes = row.filial_transaction_params(account,item,cost_center)
        unless transaction.save
          row.copy_errors(transaction)
          row_return_values.row_with_error = row
          ExcelReader::RowCostCenter.rollback
        end
      end
    end

    return row_return_values
  end

end
