class ExcelReader::RowAccount < ExcelReader::Row
  attr_accessor :name,
                :code,
                :description,
                :source,
                :item_code

  validates_presence_of :name,
                        :code,
                        :description,
                        :source,
                        :item_code

  def self.get_format
    {
      name:0,
      code:1,
      description:2,
      source:3,
      item_code:4
    }
  end

  def account_params(item)
    {
      name:self.name.strip,
      code:self.code,
      description:self.description.strip,
      source:self.source.strip,
      item_code:self.item_code.strip,
      item: item
    }
  end

  def check_item
    item = Item.search_existence(self.item_code)
    if item.blank?
      # equal_account_id = Item.verify_account_id(account)
      # if equal_account_id
      #   self.add_custom_error(:base,"El codigo #{self.item_code} no coincide con el de la cuenta #{self.code}")
      #   item = false
      # else
      item = Item.create!(code: self.item_code,description: self.item_code)
      # end
    end
    return item
    # TODO: Check if it's necesary to create a new item
  end


  def self.validate_multiple_rows(rows)
    return true
  end

  def self.after_multiple_rows_validation(rows)
    row_return_values = OpenStruct.new
    row_return_values.row_with_error = true
    row_return_values.news_counter = 0
    row_return_values.edits_counter = 0

    #Check amount of data
    check_data = check_amount_of_data(rows,row_return_values)
    if check_data.present?
      return check_data
    end

    Account.transaction do
      rows.each do |row|
        account = Account.find_by(code: row.code)
        if account.blank?
          account = Account.new
          row_return_values.news_counter += 1
        else
          row_return_values.edits_counter += 1
        end
        item = row.check_item
        account.attributes = row.account_params(item)
        unless account.save
          row.copy_errors(account)
          row_return_values.row_with_error = row
          ExcelReader::RowAccount.rollback
        end


        #Check for errors
        if item == false
          row.copy_errors(account)
          row_return_values.row_with_error = row
          ExcelReader::RowAccount.rollback
        else
          account.item_code = item.code
          account.item_id = item.id
        end
      end
    end
    return row_return_values
  end
end
