class ExcelReader::RowAdjustment < ExcelReader::Row
  attr_accessor :name,
                :adjustment_type,
                :amount,
                :date,
                :item_code,
                :account_code

  validates_presence_of :name,
                :adjustment_type,
                :amount,
                :date



  def self.get_format
    {
      adjustment_type:0,
      name:1,
      amount:2,
      date:3,
      item_code:4,
      account_code:5
    }
  end
  def adjustment_params(item,account)
    {
      adjustment_type:self.adjustment_type.strip,
      name:self.name.strip,
      amount:self.amount.strip,
      date:self.date.strip,
      item:item,
      account:account
    }
  end

  def check_valid_item
    item = Item.search_existence(self.item_code)
    if item.blank?
      item = true
      self.add_custom_error(:base,"El codigo de item #{self.item_code} no existe.")
    end
    return item
  end

  def check_valid_account
    account = Account.search_existence(self.account_code)
    if account.blank?
      account = true
      self.add_custom_error(:base, "El codigo de cuenta #{self.account_code} no existe.")
    end
    return account
  end

  def self.validate_multiple_rows(rows)
    return true
  end

  def self.after_multiple_rows_validation(rows)
    row_return_values = OpenStruct.new
    row_return_values.row_with_error = true
    row_return_values.news_counter = 0
    row_return_values.edits_counter = 0
    #Check amount of data
    check_data = check_amount_of_data(rows,row_return_values)
    if check_data.present?
      return check_data
    end
    
    Adjustment.transaction do
      rows.each do |row|
        #check item code
        item = nil
        if row.item_code.present?
          item = row.check_valid_item
        end
        #check account code
        account = nil
        if row.account_code.present?
          account = row.check_valid_account
        end
        #create adjustment
        adjustment = Adjustment.find_by(adjustment_type: row.adjustment_type,name: row.name)
        if adjustment.blank?
          adjustment = Adjustment.new
          row_return_values.news_counter +=1
        else
          row_return_values.edits_counter +=1
        end
        adjustment.attributes = row.adjustment_params(item,account)

        #check errors
        if item == true
          row.copy_errors(adjustment)
          row_return_values.row_with_error = row
        end

        #we found an error
        if row_return_values.row_with_error.is_a?(ExcelReader::Row)
          ExcelReader::RowAdjustment.rollback
        end

        #save the adjustment
        unless adjustment.save
          row.copy_errors(adjustment)
          row_return_values.row_with_error = row
          ExcelReader::RowAdjustment.rollback
        end
      end
    end
    return row_return_values
  end


end
