class ExcelReader::Row

  include ActiveModel::Model

  attr_accessor :row_number
  FORMAT_DATE = /\d{1,2}-\d{2}-\d{4}/
  
  def copy_errors(model)
    model.errors.each do |key, value|
      self.errors.add(key.to_sym,value)
    end
  end

  def add_custom_error(attribute,message)
    self.errors.add(attribute,message)
  end

  def self.open_spreadsheet(file)
    case File.extname(file.original_filename)
      when ".xls" then Roo::Excel.new(file.path)
      when ".xlsx" then Roo::Excelx.new(file.path)
      else false
    end
  end

  def self.parse_raw_row(row)
    parsed_row = {}
    self.get_format().each_pair { |key, col| parsed_row[key] = row[col] }
    parsed_row
  end

  def self.create_from_raw_row(raw_row, number)
    row = self.new(self.parse_raw_row(raw_row))
    row.row_number = number
    row
  end

  def self.get_format
  end


  def self.read_spreadsheet(file)
    spreadsheet = open_spreadsheet(file)
    rows = []
    # Run validation for each row
    (2..spreadsheet.last_row).each do |number|
      row = self.create_from_raw_row(spreadsheet.row(number), number)
      return row unless row.valid?
      rows << row
    end

    # Run multiple validation for rows
    row = self.validate_multiple_rows(rows)

    if row.is_a?(ExcelReader::Row)
      return row
    else
      return self.after_multiple_rows_validation(rows)
    end

  end

  def self.validate_multiple_rows(rows)
    # Override: return true if rows is valid
    # return a instance of SpreadsheetRow with errors if not
    true
  end

  def self.after_multiple_rows_validation(rows)
    # Override: return true if everything is OK
    # return a instance of SpreadsheetRow with errors if not
    return true
  end

  def self.array_to_hash(arr)
    # Transform an array [a0, a1, a2, ...] to a Hash { a0 => 0, a1 => 1, etc }
    # which is the format requires by the self.get_format method
    Hash[[*arr.map.with_index]]
  end

  def self.rollback
    raise ActiveRecord::Rollback
  end
  def self.check_amount_of_data(rows,row_return_values)
    #Check amount of data
    if rows.count > 5000
      row_return_values.row_with_error = rows[0]
      row_return_values.row_with_error.errors.add(:base,:blank,message: "La cantidad de datos es superior a 5000")
      return row_return_values
    else
      return nil
    end
  end


end
