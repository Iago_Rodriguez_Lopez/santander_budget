class ExcelReader::RowCostCenter < ExcelReader::Row

  attr_accessor :code,
                :name,
                :description,
                :division,
                :management,
                :area,
                :subarea

  validates_presence_of :code,
                        :name
                       

  def self.get_format
    {
    code:0,
    name:1,
    description:2,
    division:3,
    management:4,
    area:5,
    subarea:6
    }
  end

  def cost_center_params
    {
      code:self.code,
      name:self.name.strip,
      description:self.description,
      division:self.division.strip,
      management: self.management.strip,
      area:self.area.strip,
      subarea:self.subarea
    }
  end

  def self.validate_multiple_rows(rows)
    # TODO: Retornar la row con error si existe una con error
    # En el caso de Cost Center no se requiere hacer ninguna validación a nivel agregado
    return true
  end

  def self.after_multiple_rows_validation(rows)
    # esta variable se transforma en un Row
    # si hay un error al guardar algún centro de costo y retorna la fila que tuvo problemas
    row_return_values = OpenStruct.new
    row_return_values.row_with_error = true
    row_return_values.news_counter = 0
    row_return_values.edits_counter = 0

    #Check amount of data
    check_data = check_amount_of_data(rows,row_return_values)
    if check_data.present?
      return check_data
    end
    
    CostCenter.transaction do
      rows.each do |row|
        cost_center = CostCenter.new(row.cost_center_params)
        row_return_values.news_counter += 1
        unless cost_center.save
          row.copy_errors(cost_center)
          row_return_values.row_with_error = row
          ExcelReader::RowCostCenter.rollback
        end
      end
    end

    return row_return_values
  end

end
