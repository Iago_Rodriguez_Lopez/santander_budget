class ExcelReader::RowItem < ExcelReader::Row
   attr_accessor  :code,
                  :name,
                  :subfield,
                  :ccc_code,
                  :ccc_name,
                  :ges_mod,
                  :ges_mod2,
                  :cargabal_id,
                  :cargabal_name,
                  :format_spain,
                  :format_group,
                  :format_bank,
                  :account_code,
                  :account_name

  validates_presence_of :code,
                         :name,
                         :subfield,
                         :ccc_code,
                         :ccc_name,
                         :ges_mod,
                         :ges_mod2,
                         :cargabal_id,
                         :cargabal_name,
                         :format_spain,
                         :format_group,
                         :format_bank

  def self.get_format
    {
      code:0,
      name:1,
      subfield:2,
      ccc_code:3,
      ccc_name:4,
      ges_mod:5,
      ges_mod2:6,
      cargabal_id:7,
      cargabal_name:8,
      format_spain:9,
      format_group:10,
      format_bank:11,
      account_code:12,
      account_name:13
    }
  end


  def format_bank_params
    {
      name:self.format_bank.strip
    }
  end

  def format_group_params
    {
      name:self.format_group.strip
    }
  end

  def format_spain_params
    {
      name:self.format_spain.strip
    }
  end

  def interpretation_cargabal_params
    {
      name:self.cargabal_name.strip,
      code:self.cargabal_id
    }
  end

  def interpretation_ccc_params
    {
      name:self.ccc_name.strip,
      code:self.ccc_code
    }
  end

  def interpretation_ges_params
    {
      code:self.ges_mod,
      name:self.ges_mod2
    }
  end

  def subfield_params
    {
      name:self.subfield.strip
    }
  end

  def create_format_bank
    #Create FormatBank
    format_bank = FormatBank.find_by(name: self.format_bank)||FormatBank.new
    format_bank.attributes = self.format_bank_params
    format_bank.save
    return format_bank
  end
  def create_format_group
    #Create FormatGroup
    format_group = FormatGroup.find_by(name: self.format_group)||FormatGroup.new
    format_group.attributes = self.format_group_params
    format_group.save
    return format_group
  end
  def create_format_spain
    #Create FormatSpain
    format_spain = FormatSpain.find_by(name: self.format_spain)||FormatSpain.new
    format_spain.attributes = self.format_spain_params
    format_spain.save
    return format_spain
  end
  def create_interpretation_cargabal
    #Create InterpretationCargabal
    interpretation_cargabal = InterpretationCargabal.find_by(code: self.cargabal_id)||InterpretationCargabal.new
    interpretation_cargabal.attributes = self.interpretation_cargabal_params
    interpretation_cargabal.save
    return interpretation_cargabal
  end
  def create_interpretation_ccc
    #Create InterpretationCcc
    interpretation_ccc = InterpretationCcc.find_by(code: self.ccc_code)||InterpretationCcc.new
    interpretation_ccc.attributes = self.interpretation_ccc_params
    interpretation_ccc.save
    return interpretation_ccc
  end
  def create_interpretation_ges
    #Create InterpretationGes
    interpretation_ges = InterpretationGe.find_by(code: self.ges_mod)||InterpretationGe.new
    interpretation_ges.attributes = self.interpretation_ges_params
    interpretation_ges.save
    return interpretation_ges
  end
  def create_subfield
    #Create FormatBank
    subfield = Subfield.find_by(name: self.subfield)||Subfield.new
    subfield.attributes = self.subfield_params
    subfield.save
    return subfield
  end

  # def check_valid_account
  #
  #   account = Account.find_by(code: self.account_code.to_i.to_s)
  #   #TODO: Make sure all the accounts and items save integer codes without decimals
  #
  #   if account.blank?
  #     account = false
  #     self.add_custom_error(:base,"El codigo de cuentas #{self.account_code}  no existe.")
  #   else
  #     item = Item.find_by(account_id: account.id)
  #     unless item.blank?
  #       account = true
  #       self.add_custom_error(:base,"El codigo #{self.account_code} ya esta tomado en el item #{item.code}")
  #     end
  #   end
  #   return account
  # end

  def self.validate_multiple_rows(rows)
    return true
  end

  def self.after_multiple_rows_validation(rows)
    row_return_values = OpenStruct.new
    row_return_values.row_with_error = true
    row_return_values.news_counter = 0
    row_return_values.edits_counter = 0  
    
    #Check amount of data
    check_data = check_amount_of_data(rows,row_return_values)
    if check_data.present?
      return check_data
    end
    Item.transaction do
      rows.each do |row|
        #Create all the other models
        format_bank = row.create_format_bank
        format_group = row.create_format_group
        format_spain = row.create_format_spain
        interpretation_cargabal = row.create_interpretation_cargabal
        interpretation_ccc = row.create_interpretation_ccc
        interpretation_ges = row.create_interpretation_ges
        subfield = row.create_subfield
        # account = row.check_valid_account

        #CreateItem
        new_item = true
        item = Item.find_by(code: row.code)
        if item.blank?
          item = Item.new
          row_return_values.news_counter += 1
        else
          new_item = false
          row_return_values.edits_counter += 1
        end
        item.attributes = {
          code:row.code.strip,
          description:row.name.strip,
          format_bank_id:format_bank.id,
          format_group_id:format_group.id,
          format_spain_id:format_spain.id,
          interpretation_ccc_id: interpretation_ccc.id,
          interpretation_cargabal_id: interpretation_cargabal.id,
          interpretation_ge_id: interpretation_ges.id,
          subfield_id: subfield.id
        }
        #check errors
        # if account == true && !new_item
        #   account = Account.find_by(code: row.account_code)
        # elsif account == false
        #   row.copy_errors(item)
        #   row_return_values.row_with_error = row
        # end
        #we found an error
        if row_return_values.row_with_error.is_a?(ExcelReader::Row)
          ExcelReader::RowItem.rollback
        end
        #give the item account
        # item.account_id = account.id
        #everything is Ok

        unless item.save
          row.copy_errors(item)
          row_return_values.row_with_error = row
          ExcelReader::RowItem.rollback
        end
      end
    end
    return row_return_values
  end

end
