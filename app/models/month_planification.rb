class MonthPlanification < ApplicationRecord
	has_many :base_adjustments
	after_save :create_base_adjustments
	scope :base_adjustments_by_subfield, -> (subfield) { where(base_adjustments:{subfield_id:subfield.id}) }
	validates :name , presence: true, allow_blank: false

	extend BaseCalculations

  BASE_TYPES = {
    #TODO: Unify all the type of base adjustments in the model BaseAdjustment
    #Ask to project manager what is calculations for this criteria
    #monthly_budgeted_expenditure: "Gasto mensual presupuestado",
    spent_previous_month:'Gasto mes anterior',
    budget_previous_month: 'Presupuesto mes anterior',
    average_monthly_spent: 'Promedio mensual de gasto'}

	def self.get_month_names(month,year)
		MonthPlanification.select(:name).where(month: month,year: year).group(:name).pluck(:name)
	end
	def self.search_by_name(name)
		MonthPlanification.find_by(name: name)
	end

	def create_base_adjustments
		subfields_ids = Item.get_subfields_ids

		subfields_ids.each do |index|
			items = Item.where(subfield_id: index)
			items.each do |item|
        #TODO: See how nil alternative for BaseAdjust affect the rest of the code
				BaseAdjustment.create!(month_planification_id: self.id,subfield_id: index,item_id:item.id)

			end
    end

	end

	def self.get_items(format_type,name)
		model_types = Report::MODELS_TYPES
		if format_type == model_types[:format_spain]
			format_spain = FormatSpain.find_by(name:name)
			Item.where(format_spain_id:format_spain.id)
		elsif format_type == model_types[:local_gp]
			interpretation_ge = InterpretationGe.find_by(name:name)
			Item.where(interpretation_ge_id:interpretation_ge.id)
		else
			Item.all
		end
	end

	def self.total_month_planification(control_report_detail,month,year,version,report_type)
		total_month_planification = []
		current_year = control_report_detail.current_year_result
		current_year.each_with_index do |control_report,index|
			current_month_planification = current_month_planification_sum(month,year,version,report_type,control_report["concept"])
			total_month_planification << OpenStruct.new(concept:control_report["concept"],value:current_month_planification)
    end
		total_month_planification
	end

	def self.current_month_planification_sum(month,year,version,format_type,name)
		month_planification= MonthPlanification.find_by(name:version)
		items = get_items(format_type,name)
		sum_base_adjustment(items,month_planification)
  end


	def self.sum_base_adjustment(items,month_planification)
		total_base_adjustment = 0
		items.each do |item|
			base_adjustment = BaseAdjustment.find_by(month_planification_id:month_planification.id,item_id:item.id)
			if base_adjustment.present?
				theoric_adjustments = get_theoric_adjustments(base_adjustment,item.id)
	      total_base_adjustment += sum_theoric_adjustments(theoric_adjustments)
			end
		end
		total_base_adjustment
  end

	def self.get_theoric_adjustments(base_adjustment_id,item_id)
		TheoricAdjustment.where(base_adjustment_id:base_adjustment_id,item_id:item_id)
	end

	def self.sum_theoric_adjustments(theoric_adjustments)
    sum = 0
    theoric_adjustments.each do |t|
      sum += t.amount
    end
    return sum
	end

	def self.get_version_info(month,year,base_type,subfield)
    #TODO: Use method for this
    new_month = if month == 1
              month
            else
              month - 1
            end

		if base_type == BASE_TYPES[:spent_previous_month]
			data = get_month_spending(new_month,year,subfield)
			amount = data[0]["amount"]
		elsif base_type == BASE_TYPES[:budget_previous_month]
			data = get_month_budget(new_month,year,subfield)
			amount = data[0]["amount"]
    	elsif base_type == BASE_TYPES[:average_monthly_spent]
			amount = monthtly_average_expenditure_for_subfield(year,month,subfield)
    	else
			#Default option
			# data = get_month_spending(new_month,year,subfield)
			# amount = data[0]["amount"]
			amount = 0
    	end

		return amount
  end

	def self.get_month_spending(month,year,subfield)
		start_date = Date.new(year,month,1)
		end_date = start_date.end_of_month

		sql = p %{
			SELECT SUM(transactions.amount) AS amount
			FROM transactions
			INNER JOIN accounts ON accounts.id = transactions.account_id
			INNER JOIN items ON items.id = accounts.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
		}.gsub(/\s+/, " ").strip

		self.execute_pg_query(sql)
	end

	def self.get_month_budget(month,year,subfield)
		start_date = Date.new(year,month,1)
		end_date = start_date.end_of_month
		sql = p %{
			SELECT SUM(budgets.amount) AS amount
			FROM budgets
			INNER JOIN items ON items.id = budgets.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
		}.gsub(/\s+/, " ").strip
		self.execute_pg_query(sql)
	end

	def self.get_average_budget(year,subfield)
		year_data = sql_average_budget(year,subfield)
		data = year_data[0]
		sum = 0
		(1..12).each do |n|
			sum+= data["#{n}"].present? ? data["#{n}"] : 0
		end
		average = (sum/12)
		average
	end

	def self.sql_average_budget(year,subfield)
		start_date = Date.new(year,1,1)
		end_date = start_date.end_of_year
		sql = p %{
			SELECT SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 1.0 THEN budgets.amount ELSE 0 END) AS "1",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 2.0 THEN budgets.amount ELSE 0 END) AS "2",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 3.0 THEN budgets.amount ELSE 0 END) AS "3",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 4.0 THEN budgets.amount ELSE 0 END) AS "4",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 5.0 THEN budgets.amount ELSE 0 END) AS "5",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 6.0 THEN budgets.amount ELSE 0 END) AS "6",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 7.0 THEN budgets.amount ELSE 0 END) AS "7",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 8.0 THEN budgets.amount ELSE 0 END) AS "8",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 9.0 THEN budgets.amount ELSE 0 END) AS "9",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 10.0 THEN budgets.amount ELSE 0 END) AS "10",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 11.0 THEN budgets.amount ELSE 0 END) AS "11",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 12.0 THEN budgets.amount ELSE 0 END) AS "12"
			FROM budgets
			INNER JOIN items ON items.id = budgets.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
			}.gsub(/\s+/, " ").strip
			self.execute_pg_query(sql)
	end
	def self.subfield_monthly_consultation(month,year,base_adjustments)
		start_date = Date.new(year.to_i,1,1)
		end_date = Date.new(year.to_i,12,31)
		base_adjustment_subfields_id = ""
		if base_adjustments.blank?
			base_adjustments = BaseAdjustment.all
		end
		if base_adjustments.blank?
			return nil
		else
			base_adjustment_subfields_id =base_adjustments.group(:subfield_id).pluck(:subfield_id).join(',')
		end
		budget_subfield_sql = self.budget_subfield_sql(month,start_date,end_date)
		transaction_subfield_sql = self.transaction_subfield_sql(month,start_date,end_date)
		sql = p %{
			SELECT subfields.name as name,
			january, february, march, april, may, june, july,
			august, september, october, november, december, 
			current_month, last_month, actual_budget
			FROM subfields
			LEFT JOIN(
				#{transaction_subfield_sql}
			) transaction_detail on (transaction_detail.name = subfields.name)
			LEFT JOIN(
				#{budget_subfield_sql}
			)budget_detail on (budget_detail.name = subfields.name)
			WHERE subfields.id IN (#{base_adjustment_subfields_id})
			}.gsub(/\s+/, " ").strip
			self.execute_pg_query(sql)
	end

	def self.transaction_subfield_sql(month,start_date,end_date)
		p %{
			SELECT subfields.name as name,
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 1.0 THEN transactions.amount ELSE 0 END) AS "january",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 2.0 THEN transactions.amount ELSE 0 END) AS "february",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 3.0 THEN transactions.amount ELSE 0 END) AS "march",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 4.0 THEN transactions.amount ELSE 0 END) AS "april",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 5.0 THEN transactions.amount ELSE 0 END) AS "may",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 6.0 THEN transactions.amount ELSE 0 END) AS "june",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 7.0 THEN transactions.amount ELSE 0 END) AS "july",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 8.0 THEN transactions.amount ELSE 0 END) AS "august",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 9.0 THEN transactions.amount ELSE 0 END) AS "september",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 10.0 THEN transactions.amount ELSE 0 END) AS "october",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 11.0 THEN transactions.amount ELSE 0 END) AS "november",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 12.0 THEN transactions.amount ELSE 0 END) AS "december",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN #{month} THEN transactions.amount ELSE 0 END) AS "current_month",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN #{month-1} THEN transactions.amount ELSE 0 END) AS "last_month"
			FROM transactions
			INNER JOIN accounts ON accounts.id = transactions.account_id
			INNER JOIN items ON items.id = accounts.item_id
			INNER JOIN subfields ON items.subfield_id = subfields.id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD')
			GROUP BY subfields.name
			}
	end

	def self.budget_subfield_sql(month,start_date,end_date)
		p %{
			SELECT
			subfields.name as name,
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN #{month} THEN budgets.amount ELSE 0 END) AS "actual_budget",
			SUM(budgets.amount) AS "year_budget"
			FROM budgets
			INNER JOIN items ON items.id = budgets.item_id
			INNER JOIN subfields ON items.subfield_id = subfields.id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD')
			GROUP BY subfields.name
		}
	end



	def self.item_monthly_consultation(month,year,base_adjustments)
		items = Item.where.not(subfield_id:nil)
		item_ids = items.ids.join(',')
		#TODO: Fix when the month is january
		start_date = Date.new(year.to_i,1,1)
		end_date = Date.new(year.to_i,12,31)
		base_adjustment_items_id = ""
		if base_adjustments.blank?
			base_adjustments = BaseAdjustment.all
		end
		if base_adjustments.blank?
			return nil
		end
		base_adjustment_items_id =base_adjustments.group(:item_id).pluck(:item_id).join(',')
		transaction_sql = self.transaction_item_sql(month,start_date,end_date,base_adjustment_items_id)
		budget_sql = self.budget_item_sql(month,start_date,end_date)
		sql = p %{
			SELECT items.code as CODE,
			items.description as description,
			january, february, march, april, may, june,
			july,	august,	september, october,	november,	december,
			current_month, last_month, actual_budget
			FROM items
			LEFT JOIN(
				#{transaction_sql}
			) transaction_detail on (transaction_detail.code = items.code)
			LEFT JOIN(
				#{budget_sql}
			)budget_detail on (budget_detail.code = items.code)
			WHERE items.id IN (#{base_adjustment_items_id})
			}.gsub(/\s+/, " ").strip
			self.execute_pg_query(sql)
  end

	def self.transaction_item_sql(month,start_date,end_date,base_adjustment_items_id)
		p %{
			SELECT items.code as CODE , items.description as description,
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 1.0 THEN transactions.amount ELSE 0 END) AS "january",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 2.0 THEN transactions.amount ELSE 0 END) AS "february",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 3.0 THEN transactions.amount ELSE 0 END) AS "march",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 4.0 THEN transactions.amount ELSE 0 END) AS "april",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 5.0 THEN transactions.amount ELSE 0 END) AS "may",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 6.0 THEN transactions.amount ELSE 0 END) AS "june",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 7.0 THEN transactions.amount ELSE 0 END) AS "july",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 8.0 THEN transactions.amount ELSE 0 END) AS "august",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 9.0 THEN transactions.amount ELSE 0 END) AS "september",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 10.0 THEN transactions.amount ELSE 0 END) AS "october",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 11.0 THEN transactions.amount ELSE 0 END) AS "november",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN 12.0 THEN transactions.amount ELSE 0 END) AS "december",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN #{month} THEN transactions.amount ELSE 0 END) AS "current_month",
			SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN #{month-1} THEN transactions.amount ELSE 0 END) AS "last_month"
			FROM transactions
			INNER JOIN accounts ON accounts.id = transactions.account_id
			INNER JOIN items ON items.id = accounts.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD')
			GROUP BY items.code, items.description
			}
		end

	def self.budget_item_sql(month,start_date,end_date)
		p %{
			SELECT
			items.code as CODE, items.description,
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN #{month} THEN budgets.amount ELSE 0 END) AS "actual_budget",
			SUM(budgets.amount) AS "year_budget"
			FROM budgets
			INNER JOIN items ON items.id = budgets.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD')
			GROUP BY items.code, items.description
		}
	end


  def self.update_all_bases(month_planification,subfield,base_type)
    year = month_planification.year
    month = month_planification.month
	
    values_by_item = if base_type == BASE_TYPES[:spent_previous_month]
                       self.previous_spending(year,month,subfield)
                     elsif base_type == BASE_TYPES[:budget_previous_month]
                       self.previous_budget(year,month,subfield)
                     elsif base_type == BASE_TYPES[:average_monthly_spent]
					   self.monthly_average_expenditure(year,month,subfield)
					 else
						#default
						base_adjustments = BaseAdjustment.where(month_planification_id:month_planification.id,subfield_id:subfield.id)
						values = []
						base_adjustments.each do |base|
							values << OpenStruct.new(amount:0,item_id: base.item_id)
						end
						values
                     end
    begin
      MonthPlanification.transaction do
        values_by_item.each do |new_base_data|
          amount  = new_base_data['amount']
          base = BaseAdjustment.find_by!(subfield_id:subfield.id,month_planification_id:month_planification.id,item_id:new_base_data['item_id'])
          base.base_type = base_type
          base.amount = amount
          base.save!
        end
      end
    rescue => e
      raise e
    end

    true
  end

	def self.execute_pg_query(query)
		#Source: https://stackoverflow.com/questions/25331778/getting-typed-results-from-activerecord-raw-sql
		#Answer by : Ramfjord
		conn = ActiveRecord::Base.connection
		@type_map ||= PG::BasicTypeMapForResults.new(conn.raw_connection)

		res = conn.execute(query)
		res.type_map = @type_map
		res.each do |r| puts r end
	end

end
