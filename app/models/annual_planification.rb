class AnnualPlanification < ActiveRecord::Base
  has_many :annual_base_adjustments, :dependent => :destroy

  after_save :create_base_adjustments

  validates_presence_of :name, :year, :pivot_month

	extend BaseCalculations

  BASE_ADJUSTMENTS_TYPE = { #monthly_budgeted_expenditure: "Gasto mensual presupuestado",
  spending_previous_month: "Gasto mes anterior",
  budget_previous_month: "Presupuesto mes anterior",
  expenditure_average: "Promedio mensual de gasto"}



  def self.search_by_name(name)
    AnnualPlanification.find_by(name: name)
  end

  def create_base_adjustments
    subfields_ids = Item.get_subfields_ids

    subfields_ids.each do |index|
      items = Item.where(subfield_id: index)
      items.each do |item|
        AnnualBaseAdjustment.create!(amount: 0 ,base_type: nil, annual_planification_id: self.id, subfield_id: index, item_id:item.id)
      end
    end
  end

	def self.get_base_amount(month,year,base_type,subfield)
		
    if base_type == BASE_ADJUSTMENTS_TYPE[:spending_previous_month]
      data = get_month_spending(month-1,year,subfield)
      amount = data[0]['amount']
    elsif base_type == BASE_ADJUSTMENTS_TYPE[:budget_previous_month]
      data = get_month_budget(month-1,year,subfield)
      amount = data[0]['amount']
    elsif base_type == BASE_ADJUSTMENTS_TYPE[:expenditure_average]
      amount = get_average_budget(year, subfield)
    else
			amount = 0
    end
    amount
  end

	def self.get_items(format_type,name)
		model_types = Report::MODELS_TYPES
		if format_type == model_types[:format_spain]
			format_spain = FormatSpain.find_by(name:name)
			Item.where(format_spain_id:format_spain.id)
		elsif format_type == model_types[:local_gp]
			interpretation_ge = InterpretationGe.find_by(name:name)
			Item.where(interpretation_ge_id:interpretation_ge.id)
		else
			Item.all
		end
	end

	def self.total_annual_planification(control_report_detail,month,year,version,report_type)
		total_annual_planification = []
		current_year = control_report_detail.current_year_result
		current_year.each_with_index do |control_report,index|
			current_annual_planification = current_annual_planification_sum(month,year,version,report_type,control_report["concept"])
			total_annual_planification << OpenStruct.new(concept:control_report["concept"],value:current_annual_planification)
		end
		total_annual_planification
	end

	def self.current_annual_planification_sum(month,year,version,format_type,name)
		annual_planification= AnnualPlanification.find_by(name:version)
		items = get_items(format_type,name) 
		sum_annual_base_adjustment(items,annual_planification)
	end

	def self.sum_annual_base_adjustment(items,annual_planification)
		total_annual_base_adjustment = 0
		items.each do |item|
			annual_base_adjustment = AnnualBaseAdjustment.where(annual_planification_id:annual_planification.id,item_id:item.id).first
			if annual_base_adjustment.present?
				theoric_annual_adjustments = get_theoric_annual_adjustments(annual_base_adjustment)
				total_annual_base_adjustment += annual_base_adjustment.amount
	      total_annual_base_adjustment += sum_theoric_annual_adjustments(theoric_annual_adjustments)
			end
		end
		total_annual_base_adjustment
  end

	def self.get_theoric_annual_adjustments(annual_base_adjustment)
		theoric_annual_adjustments = TheoricAnnualAdjustment.where(annual_base_adjustment_id:annual_base_adjustment.id)
	end

	def self.sum_theoric_annual_adjustments(theoric_annual_adjustments)
    sum = 0
		theoric_annual_adjustments.each do |t|
      sum += t.amount
    end
    return sum
	end


  def self.get_month_spending(month, year, subfield)
    #TODO: Change to use the view , you should reload the view, so the value it will loaded automatically :)
    start_date = Date.new(year, month, 1)
    end_date = start_date.end_of_month
		sql = p %{
			SELECT SUM(transactions.amount) AS amount
			FROM transactions
			INNER JOIN accounts ON accounts.id = transactions.account_id
			INNER JOIN items ON items.id = accounts.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
		}.gsub(/\s+/, " ").strip
		self.execute_pg_query(sql)
  end

  def self.get_month_budget(month,year,subfield)
		start_date = Date.new(year,month,1)
		end_date = start_date.end_of_month
		sql = p %{
			SELECT SUM(budgets.amount) AS amount
			FROM budgets
			INNER JOIN items ON items.id = budgets.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
		}.gsub(/\s+/, " ").strip
		self.execute_pg_query(sql)
  end
  
  def self.get_average_budget(year,subfield)
		year_data = sql_average_budget(year,subfield)
		data = year_data[0]
		sum = 0
		(1..12).each do |n|
			sum+= data["#{n}"].present? ? data["#{n}"] : 0
		end
		average = (sum/12)
		average
	end

  def self.sql_average_budget(year,subfield)
		start_date = Date.new(year,1,1)
		end_date = start_date.end_of_year
		sql = p %{
			SELECT SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 1.0 THEN budgets.amount ELSE 0 END) AS "1",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 2.0 THEN budgets.amount ELSE 0 END) AS "2",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 3.0 THEN budgets.amount ELSE 0 END) AS "3",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 4.0 THEN budgets.amount ELSE 0 END) AS "4",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 5.0 THEN budgets.amount ELSE 0 END) AS "5",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 6.0 THEN budgets.amount ELSE 0 END) AS "6",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 7.0 THEN budgets.amount ELSE 0 END) AS "7",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 8.0 THEN budgets.amount ELSE 0 END) AS "8",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 9.0 THEN budgets.amount ELSE 0 END) AS "9",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 10.0 THEN budgets.amount ELSE 0 END) AS "10",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 11.0 THEN budgets.amount ELSE 0 END) AS "11",
			SUM(CASE EXTRACT(MONTH FROM budgets.period) WHEN 12.0 THEN budgets.amount ELSE 0 END) AS "12"
			FROM budgets
			INNER JOIN items ON items.id = budgets.item_id
			WHERE to_date('#{start_date}','YYYY-MM-DD') <= budgets.period AND budgets.period <= to_date('#{end_date}','YYYY-MM-DD') AND items.subfield_id = #{subfield.id}
			}.gsub(/\s+/, " ").strip
			self.execute_pg_query(sql)
	end


	def self.update_all_bases(annual_planification,subfield,base_type)
		year = annual_planification.year
		month = annual_planification.pivot_month
		values_by_item = if base_type == :spending_previous_month.to_s
											 self.previous_spending(year,month,subfield)
										 elsif base_type == :budget_previous_month.to_s
											 self.previous_budget(year,month,subfield)
										 elsif base_type == :expenditure_average.to_s
											 self.monthly_average_expenditure(year,month,subfield)
										 else
											#default
											 annual_adjustments = AnnualBaseAdjustment.where(annual_planification_id:annual_planification.id,subfield_id:subfield.id)
											 values = []
											 annual_adjustments.each do |base|
											 	values << OpenStruct.new(amount:0,item_id: base.item_id)
											 end
											 values
										 end
		begin
			AnnualPlanification.transaction do
				values_by_item.each do |new_base_data|
					amount  = new_base_data['amount']
					base = AnnualBaseAdjustment.find_by!(subfield_id:subfield.id,annual_planification_id:annual_planification.id,item_id:new_base_data['item_id'])
					base.base_type = base_type
					base.amount = amount

					base.save!
				end
			end
		rescue => e
			raise e
		end

		true
	end

  def self.execute_pg_query(query)
		#Source: https://stackoverflow.com/questions/25331778/getting-typed-results-from-activerecord-raw-sql
		#Answer by : Ramfjord
		conn = ActiveRecord::Base.connection
		@type_map ||= PG::BasicTypeMapForResults.new(conn.raw_connection)

		res = conn.execute(query)
		res.type_map = @type_map
		res.each do |r| puts r end
  end


  
end
