class AnnualPlanificationSubfieldsDetail < ActiveRecord::Base
  self.table_name = "annual_planification_subfields_detail"

  protected

  def readonly?
    true
  end
end