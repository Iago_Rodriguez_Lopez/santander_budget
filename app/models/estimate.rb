# == Schema Information
#
# Table name: estimates
#
#  id             :integer          not null, primary key
#  estimates_type :string
#  name           :string
#  amount         :float
#  date           :date
#  item_id        :integer
#  account_id     :integer
#  created_at     :datetime
#  updated_at     :datetime
#

class Estimate < ApplicationRecord
  belongs_to :item
  belongs_to :account
  validates_presence_of :name,
                        :amount,
                        :date
  validates :amount, numericality: {greater_than_or_equal_to: 0}
end
