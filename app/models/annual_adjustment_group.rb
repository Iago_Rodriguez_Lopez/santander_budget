class AnnualAdjustmentGroup
  include ActiveModel::Model

  attr_accessor :virtual_annual_adjustments
  #accepts_nested_attributes_for :virtual_annual_adjustments

  attr_accessor :annual_base_adjustment

  attr_accessor :annual_base_adjustment_id

  def load_annual_adjustments
    @virtual_annual_adjustments ||=[]

    group_categories = @annual_base_adjustment.group_categories

    group_categories.each do |group_category|
      @virtual_annual_adjustments.push(annual_base_adjustment.virtual_annual_adjustment_by_group(group_category))
    end
  end

  def process_annual_adjustments_params(adjustments_attributes)
    load_annual_adjustments

    adjustments_attributes.each_with_index do |adjustments_attribute,index|
      @virtual_annual_adjustments.push(VirtualAnnualAdjustment.new(adjustments_attribute))
    end
  end

  def self.process_annual_adjustment_group(annual_adjustment_group_params,virtual_annual_adjustments_params)
    virtual_annual_parameters = virtual_annual_adjustments_params[:virtual_annual_adjustments_attributes]
    result = nil
    AnnualBaseAdjustment.transaction do
      begin
        base = AnnualBaseAdjustment.find(annual_adjustment_group_params[:annual_base_adjustment_id])
        base.theoric_annual_adjustments.delete_all
        base.create_theorical_adjustments(virtual_annual_parameters)
        result = true
      rescue => e
        puts e.message
      end
    end

    result
  end

  def self.get_annual_adjustment_group(annual_planification,subfield,item)

    return nil if annual_planification.blank? || subfield.blank? || item.blank?

    base = AnnualBaseAdjustment.where(subfield_id:subfield.id,item_id:item.id,annual_planification_id:annual_planification.id).first

    @annual_adjustment_group = AnnualAdjustmentGroup.new()
    @annual_adjustment_group.annual_base_adjustment = base
    @annual_adjustment_group.load_annual_adjustments

    return base,@annual_adjustment_group
  end

  def virtual_annual_adjustments_attributes=(attributes)
    # Process the attributes hash
  end

end