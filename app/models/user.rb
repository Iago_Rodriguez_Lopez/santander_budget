# == Schema Information
#
# Table name: users
#
#  id          :integer          not null, primary key
#  name        :string
#  last_name   :string
#  created_at  :datetime
#  updated_at  :datetime
#  age         :integer
#  sex         :string
#  rut         :integer
#  dv          :string
#  cost_center :string
#  department  :string
#  division    :string
#  area        :string
#  subarea     :string
#

class User < ApplicationRecord
  attr_accessor :rut_completo

  validates_presence_of :rut,
                        :dv,
                        :name,
                        :last_name
                        # :cost_center,
                        # :department,
                        # :division,
                        # :area,
                        # :subarea
  # validates :rut, numericality: { only_integer: true}
  #TODO: Modify rut_completo validation
  #validates :rut_completo, rut: true
  validate :validate_uniqueness_rut, :on => :create
  # validate :validate_correct_rut

  include ActionView::Helpers
  include ActionView::Context
  include Rails.application.routes.url_helpers
  include UsersHelper

  def rut_completo
    self.rut.to_s+"-"+self.dv
  end

  def validate_uniqueness_rut

    user = User.find_by(rut: self.rut,dv: self.dv)
    if user
      self.errors.add(:base,"El rut #{self.rut}-#{self.dv} esta tomado")
    end

  end

  def actions
    user_actions(self)
  end
end
