class AnnualPlanificationTotalsReport < ActiveRecord::Base
  self.table_name = "annual_planification_totals_report"

  protected
  def readonly?
    true
  end
end