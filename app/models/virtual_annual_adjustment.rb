class VirtualAnnualAdjustment
  include ActiveModel::Model

  # Maybe use Virtus to specify kind of data
  # gem 'virtus'

  attr_accessor :name
  attr_accessor :annual_base_adjustment_id
  attr_accessor :start_month
  attr_accessor :end_month
  attr_accessor :kind
  attr_accessor :amount
  attr_accessor :percentage
  attr_accessor :group

end