class BudgetPlanning
  include ActiveModel::Model

  def self.transaction_details(year,month)
    beginning_of_the_year, last_day_current_month = self.calculate_period(year,month)

    details = self.transactions_by_quarter(current_month,first_day_of_year)
  end



  def self.calculate_period(year,month)
    first_day_current_month = Date.new(year,month,1)
    last_day_current_month = first_day_current_month.end_of_month

    first_day_of_year = last_day_current_month.beginning_of_year
    return first_day_of_year,last_day_current_month
  end

  def self.transactions_by_quarter(current_month,first_day_of_year)
    
  end
  
  def self.sql_transactions_by_quater(beginning_of_the_year,current_month,previous_month)
    p %{
      SELECT
      items.code as CODE ,
      SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN IN (1.0, 2.0, 3.0) THEN transactions.amount ELSE 0 END) AS "first_quarter",
      SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN IN (4.0, 5.0, 6.0) THEN transactions.amount ELSE 0 END) AS "second_quarter",
      SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN IN (7.0, 8.0, 9.0) THEN transactions.amount ELSE 0 END) AS "third_quarter",
      SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN IN (10.0, 11.0, 12.0) THEN transactions.amount ELSE 0 END) AS "fourth_quarter",
      SUM(CASE EXTRACT(MONTH FROM transactions.expired_date) WHEN #{previous_month} THEN transactions.amount ELSE 0 END) AS "previous_month"
      FROM transactions
      INNER JOIN accounts ON accounts.id = transactions.account_id
      INNER JOIN items ON items.account_id = accounts.id
      WHERE to_date('#{beginning_of_the_year}','YYYY-MM-DD') <= transactions.expired_date AND transactions.expired_date <= to_date('#{current_month}','YYYY-MM-DD')
      GROUP BY items.code
    }
  end


  def self.execute_pg_query(query)
    #Source: https://stackoverflow.com/questions/25331778/getting-typed-results-from-activerecord-raw-sql
    #Answer by : Ramfjord
    conn = ActiveRecord::Base.connection
    @type_map ||= PG::BasicTypeMapForResults.new(conn.raw_connection)

    res = conn.execute(query)
    res.type_map = @type_map

    res
  end

end