class TheoricAdjustment < ApplicationRecord
	belongs_to :item
	belongs_to :base_adjustment, inverse_of: :theoric_adjustments
end
