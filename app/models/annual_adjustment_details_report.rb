class AnnualAdjustmentDetailsReport < ActiveRecord::Base
  self.table_name = "annual_adjustment_details_report"

  protected

  def readonly?
    true
  end
end