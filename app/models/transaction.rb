# == Schema Information
#
# Table name: transactions
#
#  id              :integer          not null, primary key
#  cost_center_id  :integer
#  amount          :float
#  source          :string
#  created_at      :datetime
#  updated_at      :datetime
#  expired_date    :datetime
#  document_date   :datetime
#  comment         :text
#  account_id      :integer
#  enterprise_code :string
#  enterprise_name :string
#

class Transaction < ApplicationRecord
  belongs_to :account
  belongs_to :cost_center

  include ActionView::Helpers
  include ActionView::Context
  include Rails.application.routes.url_helpers
  include TransactionsHelper

  paginates_per 10

  SOURCES = %w(bank filial)

  #TODO: Ask with the cliente if the transactions can be negative
  #validates :amount, numericality: {greater_than_or_equal_to: 0}

  def actions
    actions_links(self)
  end
  def amount_to_currency
    currency_format(self.amount)
  end
end
