class TheoricAnnualAdjustment < ActiveRecord::Base
  belongs_to :annual_base_adjustment, inverse_of: :theoric_annual_adjustments

  attr_accessor :recurrence_number
  attr_accessor :start_month
  attr_accessor :end_month

end
