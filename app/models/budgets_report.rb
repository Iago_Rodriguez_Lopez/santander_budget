class BudgetsReport < ActiveRecord::Base
  self.table_name = "budgets_report"

  protected

  # The budgets_report relation is a SQL view,
  # so there is no need to try to edit its records ever.
  # Doing otherwise, will result in an exception being thrown
  # by the database connection.
  # Source : https://rietta.com/blog/2013/11/28/rails-and-sql-views-for-a-report/
  def readonly?
    true
  end
end