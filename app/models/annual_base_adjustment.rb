class AnnualBaseAdjustment < ActiveRecord::Base
  belongs_to :annual_planification
  belongs_to :subfield
  belongs_to :item

  has_many :theoric_annual_adjustments, inverse_of: :annual_base_adjustment, dependent: :destroy
  accepts_nested_attributes_for :theoric_annual_adjustments, reject_if: proc { |attributes| attributes[:amount].blank?},allow_destroy: true

  attr_accessor :start_month,:end_month

  BASE_ADJUSTMENTS_TYPE = { #monthly_budgeted_expenditure: "Gasto mensual presupuestado",
                            spending_previous_month: "Gasto mes anterior",
                            budget_previous_month: "Presupuesto mes anterior",
                            expenditure_average: "Promedio mensual de gasto"}

  after_create :create_default_theorical_adjustment

  def self.update_all_bases(base)
  	annual_base_adjustments = AnnualBaseAdjustment.where(subfield_id: base.subfield_id)
  	annual_base_adjustments.each do |annual_base_adjustment|
  		if annual_base_adjustment.id != base.id
  			annual_base_adjustment.update(base_type: base.base_type, amount: base.amount)
  		end
  	end
  end

  def create_default_theorical_adjustment
    year = self.annual_planification.year.to_i
    month = self.annual_planification.pivot_month.to_i

    forescasted_date = DateTime.new(year,month,1)
    #TODO: Change the kind for internal variable
    TheoricAnnualAdjustment.create!(name: "Ajuste de ejemplo", amount: 0,annual_base_adjustment_id: self.id, kind: "Monto",forescat_date: forescasted_date,group:1)
  end

  def group_categories
    self.theoric_annual_adjustments.pluck(:group).uniq
  end


  def virtual_annual_adjustment_by_group(group)
    adjustments = TheoricAnnualAdjustment.where(annual_base_adjustment_id:self.id,group:group).order(forescat_date: :asc)
    first_adjustment = adjustments.first
    start_month = first_adjustment.forescat_date.month
    end_month = adjustments.last.forescat_date.month

    VirtualAnnualAdjustment.new(name:first_adjustment.name,start_month:start_month,end_month:end_month,kind:first_adjustment.kind,percentage:first_adjustment.percentage,group:group,amount:first_adjustment.amount)
  end

  def create_theorical_adjustment_by_group(group,name,start_month,end_month,kind,amount,percentage)
    begin

      planification = self.annual_planification
      year = planification.year
      month = planification.pivot_month
      (end_month.to_i-start_month.to_i + 1).times do |index|
        current_month = month + index
        forescat_date = DateTime.new(year,current_month,1)
        TheoricAnnualAdjustment.create!(annual_base_adjustment_id:self.id,name: name, amount: amount,percentage:percentage, kind: kind,forescat_date: forescat_date,group:group)
      end
    rescue => e
      raise e
    end
  end

  def create_theorical_adjustments(virtual_adjustments)
    begin
      virtual_adjustments.each_with_index do |(key,value),index|
        group = index + 1
        virtual_adjustment = value
        begin
          self.create_theorical_adjustment_by_group(group,virtual_adjustment[:name],virtual_adjustment[:start_month],virtual_adjustment[:end_month],virtual_adjustment[:kind] ,virtual_adjustment[:amount],virtual_adjustment[:percentage])
        rescue => e
          raise e
        end
      end
    rescue => e
      raise e
    end
  end

end
