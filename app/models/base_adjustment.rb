class BaseAdjustment < ApplicationRecord
  has_many :theoric_adjustments, inverse_of: :base_adjustment, dependent: :destroy
  belongs_to :subfield
  belongs_to :month_planification
  belongs_to :item
  accepts_nested_attributes_for :theoric_adjustments, reject_if: proc { |attributes| attributes[:amount].blank?},allow_destroy: true

  BASE_TYPES = { spent_previous_month:'Gasto mes anterior',
                 budget_previous_month: 'Presupuesto mes anterior',
                 average_monthly_spent: 'Promedio mensual de gasto'}

  class BaseTypeRequirement < StandardError; end

  validates_numericality_of :amount, on: :update

  def save_amount(base_type)
    amount = calculate_amount(base_type)

    self.base_type = base_type.to_s
    self.amount = amount

    self.save!
  end

  def calculate_amount(base_type)
    raise BaseAdjustment::BaseTypeRequirement, "La base indicada no pertenece a las bases del sistema" unless BASE_TYPES.values.include?(base_type)

    100
  end

  def self.search_by_name(name)
  	BaseAdjustment.find_by(name: name)
  end

  def self.update_all_bases(base)
  	base_adjustments = BaseAdjustment.where(subfield_id:base.subfield_id)
  	base_adjustments.each do |base_adjustment|
  		if base_adjustment.id != base.id
  			base_adjustment.update(base_type: base.base_type,amount: base.amount)
  		end
  	end
  end


  def self.update_bases_by(params)
    errors = {}
    monthly_bases = nil

    begin
      month_planification = MonthPlanification.find(params[:month_planification_id])
      base_type = params[:base_type]
      monthly_bases = month_planification.base_adjustments

      subfield = Subfield.find(params[:subfield_id])

      #Process everything
      self.transaction do
        monthly_bases.each do |monthly_base|
          monthly_base.save_amount(base_type)
        end
      end

    rescue => e
      if e.is_a?(ActiveRecord::RecordNotFound)
        errors[:not_found] = e.message
      elsif e.is_a?(BaseAdjustment::BaseTypeRequirement)
        errors[:base_type] = e.message
      end
    end

    errors
  end

end
