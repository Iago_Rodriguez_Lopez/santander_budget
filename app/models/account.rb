# == Schema Information
#
# Table name: accounts
#
#  id          :integer          not null, primary key
#  name        :string
#  code        :string
#  description :string
#  created_at  :datetime
#  updated_at  :datetime
#  source      :string
#  item_code   :string
#  item_id     :integer
#

class Account < ApplicationRecord

  include ActionView::Helpers
  include ActionView::Context
  include Rails.application.routes.url_helpers
  include AccountsHelper

  has_many :transactions
  has_many :adjustments
  has_many :estimates
  belongs_to :item

  validates_presence_of :name,
                        :code,
                        :description,
                        :source

  validates_uniqueness_of :code, :on => :create

  validate :validate_source

  validates_uniqueness_of :code



  #validate :validate_item_code
  #TODO: Decide what is really information case or confirm the data given by client is correct.

  #before_save :try_cast_code
  #Ask if the code could be a float number
  def try_cast_code
    cast_result = Float(self.code) rescue nil && self.code.to_i
    self.code =  cast_result.present? ? cast_result.to_i.to_s : self.code
  end

  def self.search_existence(code)
      Account.find_by(code: code)
  end
  
  #TODO: Use downcase letters.
  def validate_source
    if self.source != "Filial" && self.source != "Banco"
      self.errors.add(:base,"La fuente #{self.source} no es correcta. Opciones son: Filial o Banco")
    end
  end

  def validate_item_code
    banco_account = Account.find_by(item_code: self.item_code,source: "Banco")
    filial_account = Account.find_by(item_code: self.item_code,source: "Filial")
    if banco_account && filial_account
      unless banco_account.id == self.id || filial_account.id == self.id
        self.errors.add(:base,"El codigo del item #{self.item_code} ya existe con la fuente Banco y Filial")
      end
    elsif banco_account && self.source=="Banco"
      unless banco_account.id == self.id
        self.errors.add(:base,"El codigo del item #{self.item_code} ya existe con la fuente #{self.source}")
      end
    elsif filial_account && self.source=="Filial"
      unless filial_account.id == self.id
        self.errors.add(:base,"El codigo del item #{self.item_code} ya existe con la fuente #{self.source}")
      end
    end
  end

  def actions
    actions_links(self)
  end


end
