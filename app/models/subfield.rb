# == Schema Information
#
# Table name: subfields
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime
#  updated_at :datetime
#

class Subfield < ApplicationRecord
  has_many :items
  has_many :base_adjustments

  has_many :annual_base_adjustments

  scope :id_by_name, -> (name){where(name:name)}
  
  def self.names_availables
    Subfield.pluck(:name)
  end

  def self.search_by_name(name)
    Subfield.find_by(name: name)
  end
end
