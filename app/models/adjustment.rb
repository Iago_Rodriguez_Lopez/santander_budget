# == Schema Information
#
# Table name: adjustments
#
#  id              :integer          not null, primary key
#  adjustment_type :string
#  name            :string
#  amount          :float
#  date            :datetime
#  item_id         :integer
#  account_id      :integer
#  created_at      :datetime
#  updated_at      :datetime
#

class Adjustment < ApplicationRecord
  include ActionView::Helpers
  include ActionView::Context
  include Rails.application.routes.url_helpers
  include AdjustmentsHelper

  belongs_to :item
  belongs_to :account

  validates_presence_of :name,
                        :amount,
                        :date

  #validates :amount, numericality: {greater_than_or_equal_to: 0}
  def actions
    actions_links(self)
  end
end
