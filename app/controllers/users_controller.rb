class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  before_action :load_internationalization, only:[:index]
  before_action :set_users, only: [:import]
  # before_action :validate_uniqueness_rut,only:[:create]
  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  def import
    if params[:file]
      row_param = ExcelReader::RowUser.read_spreadsheet(params[:file])
      if row_param.is_a?(ExcelReader::Row)
        @row_with_error = row_param
        render 'index'
      else
        row_with_error = row_param.row_with_error
        if row_with_error.is_a?(ExcelReader::Row)
          @row_with_error = row_with_error
          render 'index'
        else
          flash[:notice] = "Se han agregado #{row_param.news_counter}  y se han editado #{row_param.edits_counter} usuarios correctamente."
          redirect_to root_url
        end
      end
    else
      redirect_to root_url
    end
  end

  def excel_example
      send_file Rails.root.join('public','ejemplo_usuarios.xlsx').to_s, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)
    respond_to do |format|
      if @user.save
        format.html { redirect_to @user, notice: 'Se creo el usuario correctamente' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        format.html { redirect_to @user, notice: 'Usuario fue editado.' }
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    respond_to do |format|
      format.html { redirect_to users_url, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def source
    respond_to do |format|
      format.html
      format.json { render json: UserDatatable.new(view_context) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:rut,:dv,:name, :last_name,:cost_center,:department,:division,:area,:subarea)
    end

    def set_users
      @users = User.all
    end
    def load_internationalization
      @user_translation = t('activerecord.models.user')
      @attributes_translation = t('activerecord.attributes.user')
      @actions_translation = t('actions')
    end

end
