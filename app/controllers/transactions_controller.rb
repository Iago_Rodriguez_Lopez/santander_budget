class TransactionsController < ApplicationController
  before_action :set_transaction, only:[:show, :edit, :update, :destroy]
  before_action :load_internationalization, only:[:index,:import]
  before_action :set_transactions, only:[:import]

  # GET /transactions
  # GET /transactions.json
  def index
    @transactions = Transaction.includes(account:[:item]).includes(:cost_center).page(params[:page])
  end

  # GET /transactions/1
  # GET /transactions/1.json
  def show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(transaction_params)

    respond_to do |format|
      if @transaction.save
        format.html { redirect_to @transaction, notice: 'Transacción creada.' }
        format.json { render :show, status: :created, location: @transaction }
      else
        format.html { render :new }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /transactions/1
  # PATCH/PUT /transactions/1.json
  def update
    respond_to do |format|
      if @transaction.update(transaction_params)
        format.html { redirect_to @transaction, notice: 'Transacción actualizada.' }
        format.json { render :show, status: :ok, location: @transaction }
      else
        format.html { render :edit }
        format.json { render json: @transaction.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /transactions/1
  # DELETE /transactions/1.json
  def destroy
    @transaction.destroy
    respond_to do |format|
      format.html { redirect_to transactions_url, notice: 'Transacción eliminada.' }
      format.json { head :no_content }
    end
  end

  def bank_excel_example
    send_file Rails.root.join('public','ejemplo_banco.xlsx').to_s, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end

  def filial_excel_example
    send_file Rails.root.join('public','ejemplo_filiales.xlsx').to_s, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end

  def import

    if check_source
      flash[:error] = 'Favor especificar el tipo de origen de los datos: Banco o Filial'
      render 'index'
    else
      process_file
      if @row_param.is_a?(ExcelReader::Row)
        @row_with_error = @row_param
        render 'index'
      else
        if @row_param.row_with_error.blank? || @row_param.row_with_error.is_a?(ExcelReader::Row)
          @row_with_error = @row_param.row_with_error
          render 'index'
        else
          flash[:notice] = "Se han agregado #{@row_param.news_counter}  y se han editado #{@row_param.edits_counter} transacciones correctamente."
          redirect_to transactions_path
        end
      end
    end
  end

  def source
    respond_to do |format|
      format.html
      format.json { render json: TransactionDatatable.new(view_context) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    def set_transactions
      @transactions = Transaction.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def transaction_params
      params.require(:transaction).permit(:item_id, :cost_center_id, :amount, :kind,:enterprise_code, :enterprise_name,:source,:account_id,:expired_date)
    end

    def load_internationalization
      @transaction_t = t('activerecord.models.transaction')
      @transaction_attributes_t = t('activerecord.attributes.transaction')
      @actions_t = t('actions')
    end

    def check_source
      !params.has_key?(:source) || !(Transaction::SOURCES.include?(params[:source]))
    end

    def process_file
      @row_param = if params[:source] == Transaction::SOURCES[0]
                ExcelReader::RowBankTransaction.read_spreadsheet(params[:file])
             elsif params[:source] == Transaction::SOURCES[1]
                ExcelReader::RowFilialTransaction.read_spreadsheet(params[:file])
             end
    end

end
