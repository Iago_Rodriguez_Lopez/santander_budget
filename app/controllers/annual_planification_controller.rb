class AnnualPlanificationController < ApplicationController

  before_action :get_annual_planification, only: [:update_annual_version]
  before_action :set_annual_base_adjustment, only: [:adjust_base]

  def planification
    #by default we load the last version created or new one for the current period (year, month)
    set_default_planification
    set_annual_planification_report

    #TODO: Verify this load a exiting annual planification
    load_base_adjustments
    
  end


  def new_annual_planification
    @month = params[:pivot_month].to_i
    @year = params[:year].to_i

    @new_annual_planification = AnnualPlanification.new()

    respond_to do |format|
      format.js{}
    end
  end
  def create_annual_version
    @annual_planification = AnnualPlanification.create!(annual_planification_params)
    annual_params = params[:annual_planification]
    year = annual_params[:year]
    month = annual_params[:pivot_month]
    respond_to do |format|
      if @annual_planification.valid?
        format.js { redirect_to controller: 'annual_planification',action:'planification',year:year,pivot_month:month , notice: 'Planificación creada.' }
        flash[:notice] = "Planificación anual creada"
      else
        format.json { render json:{ errors:@annual_planification.errors.full_messages }, status: :unprocessable_entity }
      end
    end
  end

  def update_annual_version
    if @annual_planification.update_attributes(annual_planification_params)
      flash[:success] = "Los parámetros fueron actualizados sin problemas"
      render 'planification'
    else
      render 'planification'
    end
  end

  def annual_budget_details
    annual_params = annual_planification_params
    @year = annual_params[:year].to_i
    @month = annual_params[:pivot_month].to_i
    name = annual_params[:name]
    
    if name.present?
      @annual_planification = AnnualPlanification.where(year:@year,pivot_month:@month,name:name).first
    else
      @annual_planification = AnnualPlanification.where(year:@year,pivot_month:@month).first
    end
    set_annual_planification
    set_annual_planification_report
    load_base_adjustments
    respond_to do |format|
      format.js {}
    end
  end

  def load_base
    annual_planification = AnnualPlanification.where(year:params[:year].to_i, pivot_month: params[:pivot_month].to_i,name: params[:name]).first
    subfield = Subfield.where(name: params[:subfield_name]).first

    @annual_base_adjustment = AnnualBaseAdjustment.where(subfield_id: subfield.try(:id), annual_planification_id: annual_planification.try(:id)).first

    if @annual_base_adjustment.present?
      base_type = @annual_base_adjustment.base_type
      @base_type_options = AnnualPlanification::BASE_ADJUSTMENTS_TYPE

      @base_amount = if base_type.present?
                       AnnualPlanification.get_base_amount(params[:pivot_month].to_i,params[:year].to_i,base_type,subfield)
                     else
                       AnnualPlanification.get_base_amount(params[:pivot_month].to_i,params[:year].to_i,AnnualPlanification::BASE_ADJUSTMENTS_TYPE[:monthly_budgeted_expenditure],subfield)
                     end

      respond_to do |format|
        format.js {}
      end
    else
      render json:{errors: "La información no pudo ser encontrada"}, status: :not_found
    end
  end

  def change_base_type
    subfield = Subfield.where(id:params[:subfield_id].to_i).first

    base_type = AnnualPlanification::BASE_ADJUSTMENTS_TYPE[params[:selected_base_type].to_sym]
    
    @base_amount = AnnualPlanification.get_base_amount(params[:pivot_month].to_i,params[:year].to_i,base_type,subfield)

    respond_to do |format|
      format.js {}
    end
  end

  def adjust_base
    @annual_planification = @annual_base_adjustment.annual_planification
    base_type = params[:annual_base_adjustment]['base_type']
    @subfield = Subfield.find(params[:annual_base_adjustment]['subfield_id'])

    AnnualPlanification.update_all_bases(@annual_planification,@subfield,base_type)

    set_annual_planification
    set_annual_planification_report
    load_base_adjustments

    flash.now[:success] = "Las bases fueron actualizadas sin problemas"
    respond_to do |format|
      format.js {}
    end
  end

  def load_adjustments

    @year = params[:year]
    @pivot_month = params[:pivot_month]
    @name = params[:name]

    @subfield = Subfield.where(name:params[:subfield_name]).first
    @item = Item.where(code:params[:item_code]).first

    annual_planification = AnnualPlanification.where(name:@name,year:@year,pivot_month:@pivot_month).first
    
  
    @annual_base_adjustment,@annual_adjustment_group = AnnualAdjustmentGroup.get_annual_adjustment_group(annual_planification,@subfield,@item)
    @base_adjustments = annual_planification.annual_base_adjustments
    load_base_adjustments

    respond_to do |format|
      if @annual_adjustment_group.present?
        format.js {}
      else
        format.json { render json:{ errors: "La información no se pudo encontrar, favor verificar parámetros" }, status: :unprocessable_entity }
      end
    end
  end    

  def add_virtual_adjustment
    @new_adjustment_id = params[:new_adjustment_id].to_i
    @pivot_month = params[:pivot_month].to_i-1
    @virtual_adjustment_count = @new_adjustment_id
    respond_to do |format|
      format.js {}
    end
  end

  def process_adjustments
    result = AnnualAdjustmentGroup.process_annual_adjustment_group(annual_adjustment_group_params,virtual_annual_adjustments_params)
    respond_to do |format|
      if result.present?
        annual_base_planification = AnnualBaseAdjustment.find(annual_adjustment_group_params[:annual_base_adjustment_id])
        @annual_planification = annual_base_planification.annual_planification

        @year = @annual_planification.year
        @month = @annual_planification.pivot_month

        set_annual_planification_report
        load_base_adjustments
        flash.now[:success] = "Los ajustes fueron actualizados de forma correcta"

        format.js {}
      else
        format.json { render json:{ errors: "La información no se pudo encontrar, favor verificar parámetros" }, status: :unprocessable_entity }
      end
    end
  end

  def change_adjustment_amount
    annual_base_adjustment_id= params[:annual_base_adjustment_id].to_i
    item = Item.find_by(code: params[:item_code])
    percentage = params[:percentage]
    @position = params[:position]
    annual_base_adjustment = AnnualBaseAdjustment.find_by(id: annual_base_adjustment_id)
    @amount = ((percentage.to_i * annual_base_adjustment.amount.to_i) / 100)
    respond_to do |format|
      format.js {}
    end
  end

  def quater_details
    
    @quarter = params[:quarter]

    @year = params[:year].to_i
    @month = params[:pivot_month].to_i
    annual_planification_name = params[:name]

    subfield_name = params[:subfield_name]
    item_code = params[:item_code]
    
    @selected_columns, @annual_planification_report  = AnnualPlanificationReport.quater_detail_by(@year,@month,annual_planification_name,@quarter,subfield_name,item_code)
    respond_to do |format|
      if @annual_planification_report.present?
        format.js {}
      else
        format.json { render json:{ errors: "La información no se pudo encontrar, favor verificar parámetros" }, status: :unprocessable_entity }
      end
    end
  end

  def control_report
    @month = params[:month].to_i
    @year = params[:year].to_i
    @report_type = params[:report_type]
    @version = params[:version]
    @control_report_detail = Report.control(control_report_params)
    @total_planification = AnnualPlanification.total_annual_planification(@control_report_detail,@month,@year,@version,@report_type)
    
    @alpha_month = months[@month-1]
    respond_to do |format|
      format.js {}
    end
  end




  private
  def set_default_planification
    current_date = DateTime.now

    @year = params[:year] || current_date.year
    @month = params[:pivot_month] || current_date.month

    @annual_planification = AnnualPlanification.where(year:@year,pivot_month:@month).order(:created_at).last
    

    @default_version = @annual_planification.try(:name)
    
    @versions = if @annual_planification.present?
                  AnnualPlanification.where(year:@year,pivot_month:@month).order(:created_at).pluck(:name)
                else
                  []
                end
  end

  #TODO: Change to load a annual planification specified by year and pivot_month from params
  def set_annual_planification
    # current_date = DateTime.now
    @year = @annual_planification.present? ? @annual_planification.year : @year
    @month = @annual_planification.present? ? @annual_planification.pivot_month : @month
    @default_version = @annual_planification.try(:name)

    @versions = if @annual_planification.present?
                  AnnualPlanification.where(year:@year,pivot_month:@month).order(:created_at).pluck(:name)
                else
                  []
                end
  end

  def set_annual_planification_report

    @selected_columns, @annual_planification_report = if @annual_planification.present? and @annual_planification.persisted?
                                     year = @annual_planification.year
                                     pivot_month = @annual_planification.pivot_month
                                     annual_planification_name = @annual_planification.name
                                     AnnualPlanificationReport.get_report_by(year,pivot_month,annual_planification_name)
                                   else
                                     [nil, nil]
                                   end
  end

  def get_annual_planification
    @annual_planification = AnnualPlanification.find(params[:annual_planification_id])
  end

  def annual_base_adjustment_params
    base = params.require(:annual_base_adjustment).permit(:id,:name,:base_type,:amount,:item_id,:subfield_id,:annual_planification_id)
  end

  def annual_planification_params
    params.require(:annual_planification).permit(:id,:name, :year, :pivot_month)
  end

  def annual_adjustment_group_params
    params.require(:annual_adjustment_group).permit(:annual_base_adjustment_id)
  end

  def virtual_annual_adjustments_params
    params.require(:annual_adjustment_group).permit(virtual_annual_adjustments_attributes:[:kind,:percentage,:amount,:start_month,:end_month,:name])
  end

  def set_annual_base_adjustment
    @annual_base_adjustment = AnnualBaseAdjustment.find(params.require(:annual_base_adjustment)[:id])
  end

  def load_base_adjustments
    @new_annual_planification ||= AnnualPlanification.new()

    @base_adjustments = if @annual_planification.present?
                         @annual_planification.annual_base_adjustments
                       else
                         @new_annual_planification.annual_base_adjustments
                       end
  end

  def control_report_params
    params.permit(:month,:year,:report_type,:version)
    {month_report:params[:month].to_i,year_report:params[:year].to_i,report_type: params[:report_type]}
  end
end
