class TheoricAnnualAdjustmentsController < ApplicationController
  before_action :set_theoric_annual_adjustment, only: [:update, :destroy]

  def update
    respond_to do |format|
      if @theoric_annual_adjustment.update(theoric_adjustment_params)
        @item = @theoric_annual_adjustment.item
        @annual_base_adjustment = @theoric_annual_adjustment.annual_base_adjustment
        format.js {}
      else
        format.html {redirect_to annual_planification_planification, notice: 'Ajuste teórico no editado.'}
        format.json { render json: @theoric_annual_adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @item = @theoric_annual_adjustment.item
    @annual_base_adjustment = @theoric_annual_adjustment.base_adjustment
    @theoric_annual_adjustment.destroy
    respond_to do |format|
      format.js {}
    end
  end

  private
    def theoric_annual_adjustment_params
      params.require(:theoric_annual_adjustment).permit(:name, :amount, :annual_base_adjustment_id, :item_id, :kind, :percentage,:forescat_date)
    end
    def set_theoric_annual_adjustment
      @theoric_annual_adjustment = TheoricAnnualAdjustment.find(params[:id])
    end
end
