class BudgetsController < ApplicationController
  before_action :set_budget, only: [:show, :edit, :update, :destroy]
  before_action :load_internationalization, only:[:index]
  before_action :set_budgets, only:[:import]
  before_action :set_years, only:[:index,:check_year]

  # GET /budgets
  # GET /budgets.json
  def index
    @budgets = Budget.includes(:item).all
    #para hacer la llamada a ajax
    respond_to do |format|
      format.html
      format.json {render :json => @years.to_json}
    end
  end

  # GET /budgets/1
  # GET /budgets/1.json
  def show
  end

  # GET /budgets/new
  def new
    @budget = Budget.new
  end

  # GET /budgets/1/edit
  def edit
  end

  # POST /budgets
  # POST /budgets.json
  def create
    @budget = Budget.new(budget_params)

    respond_to do |format|
      if @budget.save
        format.html { redirect_to @budget, notice: 'Presupuesto creado.' }
        format.json { render :show, status: :created, location: @budget }
      else
        format.html { render :new }
        format.json { render json: @budget.errors, status: :unprocessable_entity }
      end
    end
  end

  def check_year

    render json: true

  end

  def import
    if params[:file] && params[:date]
      row_param = ExcelReader::RowBudget.read_spreadsheet(params[:file],params[:date][:year])
      if row_param.is_a?(ExcelReader::Row)
        @row_with_error = row_param
        render 'index'
      else
        row_with_error = row_param.row_with_error
        if row_with_error.is_a?(ExcelReader::Row)
          @row_with_error = row_with_error
          render 'index'
        else
          flash[:notice] = "Se han agregado #{row_param.news_counter}  y se han editado #{row_param.edits_counter} presupuestos correctamente."
          redirect_to budgets_path
        end
      end
    else
      redirect_to check_year_budgets_path
    end
  end


  def excel_example
    send_file Rails.root.join('public','ejemplo_presupuestos 2016.xlsx').to_s, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end

  # PATCH/PUT /budgets/1
  # PATCH/PUT /budgets/1.json
  def update
    respond_to do |format|
      if @budget.update(budget_params)
        format.html { redirect_to @budget, notice: 'Presupuesto actualizado.' }
        format.json { render :show, status: :ok, location: @budget }
      else
        format.html { render :edit }
        format.json { render json: @budget.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /budgets/1
  # DELETE /budgets/1.json
  def destroy
    @budget.destroy
    respond_to do |format|
      format.html { redirect_to budgets_url, notice: 'Presupuesto eliminado.' }
      format.json { head :no_content }
    end
  end

  def source
    respond_to do |format|
      format.html
      format.json { render json: BudgetDatatable.new(view_context) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_budget
      @budget = Budget.find(params[:id])
    end

    def set_budgets
      @budgets = Budget.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def budget_params
      params.require(:budget).permit(:item_id, :month, :year, :amount)
    end

    def set_years
      @years=Budget.get_years
    end

    def load_internationalization
      @budget_t = t('activerecord.models.budget')
      @budget_attributes_t = t('activerecord.attributes.budget')
      @actions_t = t('actions')
    end
end
