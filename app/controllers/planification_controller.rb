class PlanificationController < ApplicationController

  def monthly
    if params[:notice] == "Creada" #When we create a new month planification
      flash.now[:success] = "Planificación mensual creada"
      @notification = true
    end
    @year = params[:year].present? ? params[:year].to_i : DateTime.now.year
    @month = params[:month].present? ? params[:month].to_i : DateTime.now.month
    @month_planification = MonthPlanification.where(year:@year,month:@month).last
    @month_planification_name = @month_planification.present? ? @month_planification.name : ""

    base_adjustment = BaseAdjustment.where(month_planification_id:@month_planification.present? ? @month_planification.id : "")
    @subfields_info = MonthPlanification.subfield_monthly_consultation(@month,@year,base_adjustment)
    @items_info = MonthPlanification.item_monthly_consultation(@month,@year,base_adjustment)

  end

  def new_monthly_budget_version
    @year = params[:year]
    @month = params[:format_month]
    @version = params[:version]

    respond_to do |format|
      format.js {}
    end
  end

  def new_monthly_budget_modal
    @year = params[:year].to_i
    @month = params[:month].to_i
    @month_name = params[:month_name]
    respond_to do |format|
      format.js{}
    end
  end

  def change_version_monthly
    
    @year = params[:year].to_i
    @month = params[:month].to_i
    @month_planification = MonthPlanification.find_by(year:@year,month:@month)
    @month_planification_name= @month_planification.present? ? @month_planification.name : ""
    base_adjustment = BaseAdjustment.where(month_planification_id:@month_planification.present? ? @month_planification.id : "")
    @subfields_info = MonthPlanification.subfield_monthly_consultation(@month,@year,base_adjustment)
    @items_info = MonthPlanification.item_monthly_consultation(@month,@year,base_adjustment)
    
    respond_to do |format|
      format.js{}
    end
  end

  def change_base_type
    year = params[:year].to_i
    month = get_index_month(params[:month])
    base_type = params[:seleted_value]
    subfield = Subfield.find_by_id(params[:subfield_id].to_i)
    @amount = MonthPlanification.get_version_info(month,year,base_type,subfield)
    respond_to do |format|
      format.js { }
    end
  end

  def change_analysis_version_name
    @version = params[:version]
    @month = params[:month].to_i
    @year = params[:year].to_i
    @month_planification = MonthPlanification.find_by(year:@year,month:@month,name:@version)
    base_adjustment = BaseAdjustment.where(month_planification_id:@month_planification.id)
    @subfields_info = MonthPlanification.subfield_monthly_consultation(@month,@year,base_adjustment)
    @items_info = MonthPlanification.item_monthly_consultation(@month,@year,base_adjustment)

    respond_to do |format|
      format.js {}
    end
  end

  def open_adjustments_form_modal
    @item =  Item.find_by(code: params[:item_code].strip)
    month_planification = MonthPlanification.search_by_name(params[:version].strip)
    @base_adjustment = BaseAdjustment.find_by(month_planification_id:month_planification.id, subfield_id: @item.subfield_id,item_id:@item.id)
    @base_badget = params[:base_budget]

    respond_to do |format|
      format.js{}
    end
  end

  def new_monthly_base
    @base_type = params[:base_type]
    @base_value = params[:base_value]

    respond_to do |format|
      format.js {}
    end
  end

  def open_base_adjustments_modal
    @subfield = Subfield.search_by_name(params[:subfield_name].strip)
    month_planification = MonthPlanification.search_by_name(params[:version].strip)
    @base_adjustment = BaseAdjustment.find_by(month_planification_id:month_planification.id, subfield_id: @subfield.id)
    @base_budget = params[:base_budget]
    year = params[:year].to_i
    month = params[:month].to_i
    @amount = MonthPlanification.get_version_info(month,year,@base_adjustment.base_type,@subfield)

    respond_to do |format|
      format.js {}
    end
  end

  def process_base_adjustment
    #TODO: Just recieved subfield_id, base_type_criteria

    parameters = base_adjustment_params
    @month_planification = MonthPlanification.find(params[:month_planification_id])
    @subfield = Subfield.find(parameters[:subfield_id])
    @base_type = parameters[:base_type]
    
    @result = MonthPlanification.update_all_bases(@month_planification,@subfield,@base_type)

    respond_to do |format|
      @year = @month_planification.year
      @month = @month_planification.month

      @base_adjustments = @month_planification.base_adjustments

      @subfields_info = MonthPlanification.subfield_monthly_consultation(@month,@year,@base_adjustments)
      @items_info = MonthPlanification.item_monthly_consultation(@month,@year,@base_adjustments)

      flash.now[:success] = "Las bases fueron actualizadas sin problemas"

      format.js {}
    end
  end
  
  def control_report
    @month = params[:month].to_i
    @year = params[:year].to_i
    @report_type = params[:report_type]
    @version = params[:version]
    @control_report_detail = Report.control(control_report_params)
    @total_planification = MonthPlanification.total_month_planification(@control_report_detail,@month,@year,@version,@report_type)
    
    @alpha_month = months[@month-1]

    respond_to do |format|
      format.js {}
    end
  end

  def change_adjustment_amount
    item = Item.find_by(code: params[:item_code])
    percentage = params[:percentage]
    @position = params[:position]
    base_adjustment = BaseAdjustment.find_by(id:params[:base_adjustment_id].to_i)
    @amount = ((percentage.to_i * base_adjustment.amount.to_i) / 100)
    respond_to do |format|
      format.js {}
    end
  end

  def new_item_adjustment
    @base_adjustment = BaseAdjustment.find_by_id(params[:base_adjustment_id])
    @item = Item.find_by_id(params[:item_id])
    @subfield = Subfield.find_by_id(@base_adjustment.subfield_id)
    @month_planification = MonthPlanification.find_by_id(@base_adjustment.month_planification_id)
    TheoricAdjustment.create!(name: "Nuevo ajuste", amount: 0,base_adjustment_id: @base_adjustment.id, item_id:@item.id, kind: "Monto")

    respond_to do |format|
      format.js {}
    end
  end

  def delete_item_adjustment
    @base_adjustment = BaseAdjustment.find_by_id(params[:base_adjustment_id])
    @item = Item.find_by_id(@base_adjustment.item_id)
    @subfield = Subfield.find_by_id(@base_adjustment.subfield_id)
    @month_planification = MonthPlanification.find_by_id(@base_adjustment.month_planification_id)
    
    @theoric_adjustment = TheoricAdjustment.find_by(id: params[:theoric_adjustment_id].to_i)
    @theoric_adjustment.destroy

    respond_to do |format|
      format.js {}
    end
  end

  def create_budget_base
    respond_to do |format|
      if @budget.save
        format.html { redirect_to @budget, notice: 'Budget was successfully created.' }
        format.json { render :show, status: :created, location: @budget }
      else
        format.html { render :new }
        format.json { render json: @budget.errors, status: :unprocessable_entity }
      end
    end
  end

  def process_quater_details

    @version = params[:version]
    @month = params[:month].to_i
    @year = params[:year].to_i
    @quarter = params[:quarter].to_i
    @subfield_name = params[:subfield]
    @item_code = params[:item_code]

    @month_planification = MonthPlanification.find_by(year:@year,month:@month,name:@version)
    base_adjustment = BaseAdjustment.where(month_planification_id:@month_planification.present? ? @month_planification.id : "")
    @subfields_info = MonthPlanification.subfield_monthly_consultation(@month,@year,base_adjustment)
    @items_info = MonthPlanification.item_monthly_consultation(@month,@year,base_adjustment)

    respond_to do |format|
      format.js {}
    end
  end
  private
  def control_report_params
    params.permit(:month,:year,:report_type,:version)
    {month_report:params[:month].to_i,year_report:params[:year].to_i,report_type: params[:report_type]}
  end

  def base_adjustment_params
    params.require(:base_adjustment).permit(:name,:amount,:base_type, :month_planification_id,:item_id, :subfield_id,theoric_adjustments_attributes: [:id,:name,:amount,:_destroy,:base_adjustment_id, :item_id, :kind, :percentage])
  end
end
