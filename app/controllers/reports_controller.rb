class ReportsController < ApplicationController
  def control
    @month = control_report_params[:month_report].to_i
    @year = control_report_params[:year_report].to_i
    @control_report_detail = Report.control(control_report_params)
    @alpha_month = I18n.l(DateTime.parse(Date::MONTHNAMES[@month]), format: "%B").capitalize
    @total_planification = nil #Only != to nil when rendering from planification

    respond_to do |format|
      format.html
      format.js {}
    end
  end

  def consultation
    @year = consultation_report_params[:year_report]
    @consultation_report_options = consultation_report_params[:format_options]
    @consultation_report_detail = Report.consultation(consultation_report_params)


    respond_to do |format|
      format.html
      format.js{}
    end
  end
  def detailed_control
    @start_year = control_report_params[:start_year].to_i
    @end_year = control_report_params[:end_year].to_i

    @detailed_control_data = Report.detailed_control_report(detailed_report_params)
    respond_to do |format|
      format.html
      format.js{}
    end
  end

  def year_report
    @year = consultation_report_params[:year_report]
    @consultation_report_options = consultation_report_params[:format_options]
    @consultation_report_detail = Report.consultation(consultation_report_params)
    @item_index = consultation_report_params[:item_index]

    respond_to do |format|
      format.html
      format.js{}
    end
  end

  def month_report
    @year = consultation_report_params[:year_report]
    @consultation_report_options = consultation_report_params[:format_options]
    @consultation_report_detail = Report.consultation(consultation_report_params)
    @item_index = consultation_report_params[:item_index]
    @item_month = consultation_report_params[:item_month]
    respond_to do |format|
      format.html
      format.js{}
    end
  end


  private
  def consultation_report_params
    if params[:format_options].present? and params[:year_report].present?
      params.permit({format_options: [:format_type,:option_select]},:year_report,:item_index,:item_month)
    else
      {format_options:nil, year_report:DateTime.now.year}
    end
  end

  def control_report_params
    if params[:month_report].present? and params[:year_report].present?
      params.permit(:month_report,:year_report,:report_type)
    else
       {month_report:DateTime.now.month,year_report:DateTime.now.year,report_type: nil}
    end
  end
  
  def detailed_report_params
    if params[:start_year].present? and params[:end_year].present?
      params.permit(:start_year,:end_year,:report_type)

    else
      current_year = DateTime.now.year
      {start_year:current_year,end_year:current_year,report_type: nil}
    end
  end


end
