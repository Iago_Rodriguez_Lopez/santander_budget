class MonthPlanificationsController < ApplicationController
  def index
    @month_planifications = MonthPlanification.all
  end

  def new
    @month_planification = MonthPlanification.new
  end

  def create
    month = params[:month].to_i
    @month_planification = MonthPlanification.new(name:params[:name].strip,year: params[:year], month: month )
    respond_to do |format|
      if @month_planification.save
        format.html { redirect_to controller: 'planification',action:'monthly',year:params[:year],month:month , notice: 'Creada' }
        format.json { render :show, status: :created, location: @month_planification }
      else
        format.html{}
        format.json { render json: @month_planification.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end

  def edit
  end

  def update
    respond_to do |format|
      if @month_planification.update(month_planification_params)
        format.html {redirect_to @month_planification, notice: 'Planificación mensual creada.' }
        format.json {render :show, status: :ok, location: @month_planification}
      else
        format.html {render :edit }
        format.json { render json: @month_planification.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @month_planification.destroy
    respond_to do |format|
      format.html {redirect_to month_planifications_url, notice: 'Planificación mensual eliminada.'}
      format.json {head :no_content}
    end
  end

  private
    def month_planification_params
      params[:year,:name,:month]
    end

end
