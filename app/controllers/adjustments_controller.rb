class AdjustmentsController < ApplicationController
  before_action :set_adjustment, only:[:show, :edit, :update, :destroy]
  before_action :load_internationalization, only:[:index,:import]
  before_action :set_adjustments, only:[:import]

  # GET /adjustments
  # GET /adjustments.json
  def index
    @adjustments = Adjustment.all
  end

  # GET /adjustments/1
  # GET /adjustments/1.json
  def show
  end

  # GET /adjustments/new
  def new
    @adjustment = Adjustment.new
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /adjustments
  # POST /adjustments.json
  def create
    @adjustment = Adjustment.new(adjustment_params)

    respond_to do |format|
      if @adjustment.save
        format.html { redirect_to @adjustment, notice: 'Ajuste creado.' }
        format.json { render :show, status: :created, location: @adjustment }
      else
        format.html { render :new }
        format.json { render json: @adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /adjustments/1
  # PATCH/PUT /adjustments/1.json
  def update
    respond_to do |format|
      if @adjustment.update(adjustment_params)
        format.html { redirect_to @adjustment, notice: 'Ajuste actualizado.' }
        format.json { render :show, status: :ok, location: @adjustment }
      else
        format.html { render :edit }
        format.json { render json: @adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /adjustments/1
  # DELETE /adjustments/1.json
  def destroy
    @adjustment.destroy
    respond_to do |format|
      format.html { redirect_to adjustments_url, notice: 'Ajuste eliminado.' }
      format.json { head :no_content }
    end
  end

  def excel_example
    send_file Rails.root.join('public','ejemplo_ajustes.xlsx').to_s, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end

  def import
    if params[:file]
      row_param = ExcelReader::RowAdjustment.read_spreadsheet(params[:file])
      if row_param.is_a?(ExcelReader::Row)
        @row_with_error = row_param
        render 'index'
      else
        row_with_error = row_param.row_with_error
        if row_with_error.is_a?(ExcelReader::Row)
          @row_with_error = row_with_error
          render 'index'
        else
          flash[:notice] = "Se han agregado #{row_param.news_counter}  y se han editado #{row_param.edits_counter} ajustes correctamente."
          redirect_to adjustments_path
        end
      end
    else
      redirect_to adjustments_path
    end
  end

  def source
    respond_to do |format|
      format.html
      format.json { render json: AdjustmentDatatable.new(view_context) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_adjustment
      @adjustment = Adjustment.find(params[:id])
    end

    def set_adjustments
      @adjustments = Adjustment.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def adjustment_params
      params.require(:adjustment).permit(:item_id, :account_id, :amount, :adjustment_type,:date, :name)
    end

    def load_internationalization
      @adjustment_t = t('activerecord.models.adjustment')
      @adjustment_attributes_t = t('activerecord.attributes.adjustment')
      @actions_t = t('actions')
    end


end
