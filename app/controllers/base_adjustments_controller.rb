class BaseAdjustmentsController < ApplicationController
  before_action :set_base_adjustment, only: [:update]

  def index
    @base_adjustments = BaseAdjustment.all
  end

  def new
    @base_adjustment = BaseAdjustment.new
  end

  def create
    @base_adjustment = BaseAdjustment.new(base_adjustment_params)

    respond_to do |format|
      if @base_adjustment.save
        format.html { redirect_to @base_adjustment, notice: 'Ajuste base creado.' }
        format.json { render :show, status: :created, location: @base_adjustment }
      else
        format.html { render :new }
        format.json { render json: @base_adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end
  def edit
  end

  def update

    respond_to do |format|
      if @base_adjustment.update(base_adjustment_params)
        @month_planification = MonthPlanification.find_by_id(@base_adjustment.month_planification_id)
        @item = Item.find_by_id(@base_adjustment.item_id)
        @month = @month_planification.month
        @year = @month_planification.year

        base_adjustment = BaseAdjustment.where(month_planification_id:@month_planification.id)

        @subfields_info = MonthPlanification.subfield_monthly_consultation(@month,@year,base_adjustment)
        @items_info = MonthPlanification.item_monthly_consultation(@month,@year,base_adjustment)

        format.js {}

        format.html {redirect_to planification_monthly_path, notice: 'Ajuste base editada.'}
        format.json {render :show, status: :ok, location: @base_adjustment}

      else
        format.html {render :edit }
        format.json { render json: @base_adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @base_adjustment.destroy
    respond_to do |format|
      format.html {redirect_to base_adjustments_url, notice: 'Ajuste base eliminada.'}
      format.json {head :no_content}
    end
  end

  private
    def base_adjustment_params
      params.require(:base_adjustment).permit(:name,:amount,:base_type, :month_planification_id,:item_id, :subfield_id,theoric_adjustments_attributes: [:id,:name,:amount,:_destroy,:base_adjustment_id, :item_id, :kind, :percentage])
    end

    def set_base_adjustment
      @base_adjustment = BaseAdjustment.find(params[:id])
    end

end
