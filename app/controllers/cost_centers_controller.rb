class CostCentersController < ApplicationController
  before_action :set_cost_center, only: [:show, :edit, :update, :destroy]
  before_action :set_cost_centers, only: [:import]
  # GET /cost_centers
  # GET /cost_centers.json
  def index
    @cost_centers = CostCenter.all
  end

  # GET /cost_centers/1
  # GET /cost_centers/1.json
  def show

  end

  # GET /cost_centers/new
  def new
    @cost_center = CostCenter.new
  end

  # GET /cost_centers/1/edit
  def edit
  end

  # POST /cost_centers
  # POST /cost_centers.json
  def create
    @cost_center = CostCenter.new(cost_center_params)

    respond_to do |format|
      if @cost_center.save
        format.html { redirect_to @cost_center, notice: 'Centro de costo creado.' }
        format.json { render :show, status: :created, location: @cost_center }
      else
        format.html { render :new }
        format.json { render json: @cost_center.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /cost_centers/1
  # PATCH/PUT /cost_centers/1.json
  def update
    respond_to do |format|
      if @cost_center.update(cost_center_params)
        format.html { redirect_to @cost_center, notice: 'Centro de costo actualizado.' }
        format.json { render :show, status: :ok, location: @cost_center }
      else
        format.html { render :edit }
        format.json { render json: @cost_center.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /cost_centers/1
  # DELETE /cost_centers/1.json
  def destroy
    @cost_center.destroy
    respond_to do |format|
      format.html { redirect_to cost_centers_url, notice: 'Centro de costo eliminado.' }
      format.json { head :no_content }
    end
  end

  def import_excel_example
      send_file Rails.root.join('public','ejemplo_centro_costo.xlsx').to_s, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end

  def import
    #TODO: Refactor this code.
    if params[:file]
      row_param = ExcelReader::RowCostCenter.read_spreadsheet(params[:file])

      if row_param.is_a?(ExcelReader::Row)
        @row_with_error = row_param
        render 'index'
      else
        row_with_error = row_param.row_with_error
        if row_with_error.is_a?(ExcelReader::Row)
          @row_with_error = row_with_error
          render 'index'
        else
          flash[:notice] = "Se han agregado #{row_param.news_counter} centros de costos correctamente."
          redirect_to cost_centers_path
        end
      end
    else
      redirect_to cost_centers_path
    end
  end

  def source
    respond_to do |format|
      format.html
      format.json { render json: CostCenterDatatable.new(view_context) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_cost_center
      @cost_center = CostCenter.find(params[:id])
    end

    def set_cost_centers
      @cost_centers = CostCenter.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def cost_center_params
      params.require(:cost_center).permit(:code,:name,:division,:area,:subarea,:description,:management)
    end
end
