class EstimatesController < ApplicationController
  before_action :set_estimate, only:[:show, :edit, :update, :destroy]
  before_action :load_internationalization, only:[:index,:import]
  before_action :set_estimates, only:[:import]

  # GET /estimates
  # GET /estimates.json
  def index
    @estimates = Estimate.all
  end

  # GET /estimates/1
  # GET /estimates/1.json
  def show
  end

  # GET /estimates/new
  def new
    @estimate = Estimate.new
  end

  # GET /estimates/1/edit
  def edit
  end

  # POST /estimates
  # POST /estimates.json
  def create
    @estimate = Estimate.new(estimate_params)

    respond_to do |format|
      if @estimate.save
        format.html { redirect_to @estimate, notice: 'Estimación creada.' }
        format.json { render :show, status: :created, location: @estimate }
      else
        format.html { render :new }
        format.json { render json: @estimate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /estimates/1
  # PATCH/PUT /estimates/1.json
  def update
    respond_to do |format|
      if @estimate.update(estimate_params)
        format.html { redirect_to @estimate, notice: 'Estimación actualizada.' }
        format.json { render :show, status: :ok, location: @estimate }
      else
        format.html { render :edit }
        format.json { render json: @estimate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /estimates/1
  # DELETE /estimates/1.json
  def destroy
    @estimate.destroy
    respond_to do |format|
      format.html { redirect_to estimates_url, notice: 'Estimación eliminada.' }
      format.json { head :no_content }
    end
  end

  def excel_example
    send_file Rails.root.join('public','ejemplo_estimaciones.xlsx').to_s, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end

  def import
    if params[:file]
      row_param = ExcelReader::RowEstimate.read_spreadsheet(params[:file])
      if row_param.is_a?(ExcelReader::Row)
        @row_with_error = row_param
        render 'index'
      else
        row_with_error = row_param.row_with_error
        if row_with_error.is_a?(ExcelReader::Row)
          @row_with_error = row_with_error
          render 'index'
        else
          flash[:notice] = "Se han agregado #{row_param.news_counter}  y se han editado #{row_param.edits_counter} ajustes correctamente."
          redirect_to estimates_path
        end
      end
    else
      redirect_to estimates_path
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_estimate
      @estimate = Estimate.find(params[:id])
    end

    def set_estimates
      @estimates = Estimate.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def estimate_params
      params.require(:estimate).permit(:item_id, :account_id, :amount, :estimates_type,:date, :name)
    end

    def load_internationalization
      @estimate_t = t('activerecord.models.estimate')
      @estimate_attributes_t = t('activerecord.attributes.estimate')
      @actions_t = t('actions')
    end


end
