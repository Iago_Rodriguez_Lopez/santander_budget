class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  before_action :set_locale

  def set_locale
    I18n.locale = params[:locale] || I18n.default_locale
  end

  def months
    ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"]
  end

  def get_index_month(month)
    months.each_with_index do |item,index|
      if (item == month)
        return index+1
      end
    end
    return 0
  end

  def locale_months
    (1..12).map {|m| I18n.l(DateTime.parse(Date::MONTHNAMES[m]), format: "%B")}
  end

  def locale_month_by_number(number)
    locale_months[number]
  end

  def get_month_name(month)
    if month == 0
      return months[month]
    end
    months[month-1]
  end

  private
    def load_internationalization
      #Each controller can override this method to load specific I18n translation for use in the views.
    end
end
