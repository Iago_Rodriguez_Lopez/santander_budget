class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy]
  before_action :set_accounts, only: [:import]


  # GET /accounts
  # GET /accounts.json
  def index
    @accounts = Account.all
  end

  # GET /accounts/1
  # GET /accounts/1.json
  def show
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  # POST /accounts.json
  def create
    @account = Account.new(account_params)
    respond_to do |format|
      if @account.save
        format.html { redirect_to @account, notice: 'Cuenta creada' }
        format.json { render :show, status: :created, location: @account }
      else
        format.html { render :new }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /accounts/1
  # PATCH/PUT /accounts/1.json
  def update
    respond_to do |format|
      if @account.update(account_params)
        format.html { redirect_to @account, notice: 'Cuenta actualizada' }
        format.json { render :show, status: :ok, location: @account }
      else
        format.html { render :edit }
        format.json { render json: @account.errors, status: :unprocessable_entity }
      end
    end
  end
  def import
    if params[:file]
      row_param = ExcelReader::RowAccount.read_spreadsheet(params[:file])
      if row_param.is_a?(ExcelReader::Row)
        @row_with_error = row_param
        render 'index'
      else
        row_with_error = row_param.row_with_error
        if row_with_error.is_a?(ExcelReader::Row)
          @row_with_error = row_with_error
          render 'index'
        else
          flash[:notice] = "Se han agregado #{row_param.news_counter}  y se han editado #{row_param.edits_counter} cuentas correctamente."
          redirect_to accounts_url
        end
      end
    else
      redirect_to accounts_url
    end
  end


  def excel_example
      send_file Rails.root.join('public','ejemplo_cuentas.xlsx').to_s, type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
  end
  # DELETE /accounts/1
  # DELETE /accounts/1.json
  def destroy
    @account.destroy
    respond_to do |format|
      format.html { redirect_to accounts_url, notice: 'Cuenta eliminada' }
      format.json { head :no_content }
    end
  end

  def source
    respond_to do |format|
      format.html
      format.json { render json: AccountDatatable.new(view_context) }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end
    def set_accounts
      @accounts = Account.all
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params.require(:account).permit(:name, :code, :description,:source,:item_code,:item_id)
    end

    def load_internationalization
      @account_t = t('activerecord.models.account')
      @account_attributes_t = t('activerecord.attributes.account')
      @actions_t = t('actions')
    end
end
