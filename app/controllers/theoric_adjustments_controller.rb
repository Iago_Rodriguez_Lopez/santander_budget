class TheoricAdjustmentsController < ApplicationController
  before_action :set_theoric_adjustment, only: [:update,:destroy]
  def index
    @theoric_adjustments = TheoricAdjustment.all
  end

  def new
    @theoric_adjustment = TheoricAdjustment.new
  end

  def create
    @theoric_adjustment = TheoricAdjustment.new(base_adjustment_params)

    respond_to do |format|
      if @theoric_adjustment.save
        format.html { redirect_to @theoric_adjustment, notice: 'Ajuste teórico creado.' }
        format.json { render :show, status: :created, location: @theoric_adjustment }
      else
        format.html { render :new }
        format.json { render json: @theoric_adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  def show
  end
  def edit
  end

  def update
    respond_to do |format|
      if @theoric_adjustment.update(theoric_adjustment_params)
        @item = @theoric_adjustment.item
        @base_adjustment = @theoric_adjustment.base_adjustment
        format.js {}
      else
        format.html {redirect_to planification_monthly_path, notice: 'Ajuste teórico no editado.'}
        format.json { render json: @theoric_adjustment.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @item = @theoric_adjustment.item
    @base_adjustment = @theoric_adjustment.base_adjustment
    @theoric_adjustment.destroy
    respond_to do |format|
      format.js {}
    end
  end

  private
    def theoric_adjustment_params
      params.require(:theoric_adjustment).permit(:name, :amount, :base_adjustment_id, :item_id, :kind, :percentage)
    end
    def set_theoric_adjustment
      @theoric_adjustment = TheoricAdjustment.find(params[:id])
    end

end
