class ItemDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      code: { source: "Item.code", cond: :like, searchable: true, orderable: true },
      description: { source: "Item.description", cond: :like, searchable: true, orderable: true },
      interpretation_ccc: { source: "interpretation_cccs.name", cond: :like, searchable: true, orderable: true },
      code_ccc: { source: "interpretation_cccs.code", cond: :like, searchable: true, orderable: true },
      interpretation_cargabal:{source:"interpretation_cargabals.name", cond: :like, searchable: true, orderable: true  },
      code_cargabal:{source:"interpretation_cargabals.code", cond: :like, searchable: true, orderable: true},
      interpretation_ges:{source:"interpretation_ges.name", cond: :like, searchable: true, orderable: true},
      code_ges:{source:"interpretation_ges.code", cond: :like, searchable: true, orderable: true},
      subfield:{source:"subfields.name", cond: :like, searchable: true, orderable: true },
      format_bank: {source:"format_banks.name", cond: :like, searchable: true, orderable: true},
      format_group: {source:"format_groups.name", cond: :like, searchable: true, orderable: true},
      format_spain: {source:"format_spains.name", cond: :like, searchable: true, orderable: true},
      actions: {source: "Item.id", cond: :eq, searchable: false, orderable: false}
    }
  end

  def data
    records.map do |record|
      {
        code: record.code,
        description: record.description,
        interpretation_ccc: record.interpretation_ccc.present? ? record.interpretation_ccc.name : '-',
        code_ccc: record.interpretation_ccc.code.present? ? record.interpretation_ccc.code : '-',
        interpretation_cargabal: record.interpretation_cargabal.present? ? record.interpretation_cargabal.name : '-',
        code_cargabal: record.interpretation_cargabal.present? ? record.interpretation_cargabal.code : '-',
        interpretation_ges: record.interpretation_ge.present? ? record.interpretation_ge.name : '-',
        code_ges: record.interpretation_ge.present? ? record.interpretation_ge.code : '-',
        subfield: record.subfield.present? ? record.subfield.name : '-',
        format_bank: record.format_bank.present? ? record.format_bank.name : '-',
        format_group: record.format_group.present? ? record.format_group.name : '-',
        format_spain: record.format_spain.present? ? record.format_spain.name : '-',
        actions: record.actions.present? ? record.actions : '-'
      }
    end
  end

  private

  def get_raw_records
    Item.all
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
