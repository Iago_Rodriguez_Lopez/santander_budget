class TransactionDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      account:{source:"Account.name", cond: :like, searchable: true, orderable: true},
      item:{source:"items.code", cond: :like, searchable: true, orderable: true},
      cost_center:{source:"CostCenter.name", cond: :like, searchable: true, orderable: true},
      amount:{source:"Transaction.amount", cond: :like, searchable: true, orderable: true},
      source:{source:"Transaction.source", cond: :like, searchable: true, orderable: true},
      expired_date:{source:"Transaction.expired_date", cond: :like, searchable: true, orderable: true},
      document_date:{source:"Transaction.document_date", cond: :like, searchable: true, orderable: true},
      comment:{source:"Transaction.comment", cond: :like, searchable: true, orderable: true},
      enterprise_code:{source:"Transaction.enterprise_code", cond: :like, searchable: true, orderable: true},
      enterprise_name:{source:"Transaction.enterprise_name", cond: :like, searchable: true, orderable: true},
      actions:{source:"Transaction.id", cond: :eq, searchable: false, orderable: false}
    }
  end

  def data
    records.map do |record|
      {
        account: record.account.name,
        item: record.account.try(:item).try(:code),
        cost_center: record.try(:cost_center).try(:code),
        amount: record.amount_to_currency,
        source: record.source,
        expired_date: record.expired_date,
        document_date: record.document_date,
        comment: record.comment,
        enterprise_code: record.enterprise_code,
        enterprise_name: record.enterprise_name,
        actions: record.actions
      }
    end
  end

  private

  def get_raw_records
    # insert query here
    Transaction.joins(account: :item).includes(:account).includes(:cost_center)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
