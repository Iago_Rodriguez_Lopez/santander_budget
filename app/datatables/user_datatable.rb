class UserDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      id:{source: "User.id", cond: :like, searchable: true, orderable: true },
      name: {source: "User.name", cond: :like, searchable: true, orderable: true },
      last_name: {source:"User.last_name" , cond: :like, searchable: true, orderable: true },
      age: {source:"User.age" , cond: :like, searchable: true, orderable: true },
      sex: {source:"User.sex" , cond: :like, searchable: true, orderable: true },
      actions:{source:"User.id" , cond: :eq, searchable: false, orderable: false }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        id: record.id,
        name: record.name,
        last_name: record.last_name,
        age: record.age,
        sex: record.sex,
        actions: record.actions
      }
    end
  end

  private

  def get_raw_records
    User.all
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
