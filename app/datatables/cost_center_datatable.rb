class CostCenterDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      code:{source: "CostCenter.code", cond: :like, searchable: true, orderable: true },
      name:{source: "CostCenter.name", cond: :like, searchable: true, orderable: true },
      division:{source: "CostCenter.division", cond: :like, searchable: true, orderable: true },
      management:{source: "CostCenter.management", cond: :like, searchable: true, orderable: true },
      area:{source:"CostCenter.area", cond: :like, searchable: true, orderable: true },
      subarea:{source: "CostCenter.subarea", cond: :like, searchable: true, orderable: true},
      actions:{source: "CostCenter.id", cond: :eq, searchable: false, orderable: false}
    }
  end

  def data
    records.map do |record|
      {
        code: record.code,
        name: record.name,
        division: record.division,
        management: record.management,
        area: record.area,
        subarea: record.subarea,
        actions: record.actions
      }
    end
  end

  private

  def get_raw_records
    CostCenter.all
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
