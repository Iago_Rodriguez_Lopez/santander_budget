class BudgetDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
        # id: { source: "User.id", cond: :eq },
        # name: { source: "User.name", cond: :like }
        item_code:{source:"Item.code",cond: :like, searchable: true, orderable: true },
        year:{source:"Budget.year",cond: :like, searchable: true, orderable: true},
        month:{source:"Budget.month", cond: :like, searchable: true, orderable: true},
        amount:{source:"Budget.amount",cond: :like, searchable: true, orderable: true},
        actions:{source:"Budget.id",cond: :eq, searchable: false, orderable: false}
    }
  end

  def data
    records.map do |record|
      {
          # example:
          # id: record.id,
          # name: record.name
          item_code: record.try(:item).try(:code),
          year: record.year,
          month: record.month,
          amount: record.amount_to_currency,
          actions: record.actions
      }
    end
  end

  private

  def get_raw_records
    # insert query here
    Budget.includes(:item)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
