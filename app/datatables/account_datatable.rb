class AccountDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      # id: { source: "User.id", cond: :eq },
      # name: { source: "User.name", cond: :like }
      name:{source: "Account.name", cond: :like, searchable: true, orderable: true },
      code:{source: "Account.code", cond: :like, searchable: true, orderable: true },
      description:{source: "Account.description", cond: :like, searchable: true, orderable: true },
      source:{source: "items.code", cond: :like, searchable: true, orderable: true  },
      item_code:{source: "items.code", cond: :like, searchable: true, orderable: true  },
      item_description:{source: "items.description", cond: :like, searchable: true, orderable: true  },
      actions:{source: "Account.id", cond: :like, searchable: false, orderable: false  }
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
          code: record.code,
          name: record.name,
          description: record.description,
          source: record.source,
          item_code: record.item.code,
          item_description: record.item.description,
          actions: record.actions
      }
    end
  end

  private

  def get_raw_records
    Account.includes(:item)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
