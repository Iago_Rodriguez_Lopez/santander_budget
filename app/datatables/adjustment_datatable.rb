class AdjustmentDatatable < AjaxDatatablesRails::Base

  def view_columns
    # Declare strings in this format: ModelName.column_name
    # or in aliased_join_table.column_name format
    @view_columns ||= {
      concept:{source:"Adjustment.name",cond: :like, searchable: true, orderable: true },
      adjustment_type:{source:"Adjustment.adjustment_type",cond: :like, searchable: true, orderable: true},
      amount:{source:"Adjustment.amount",cond: :like, searchable: true, orderable: true},
      date:{source:"Adjustment.date",cond: :like, searchable: true, orderable: true},
      item:{source:"Item.code",cond: :like, searchable: true, orderable: true},
      account:{source:"Account.name",cond: :like, searchable: true, orderable: true},
      actions:{source:"Adjustment.id.",cond: :eq, searchable: false, orderable: false}
    }
  end

  def data
    records.map do |record|
      {
        # example:
        # id: record.id,
        # name: record.name
        concept: record.name,
        adjustment_type: record.adjustment_type,
        amount: record.amount,
        date: record.date,
        item: record.try(:item).try(:code),
        account: record.try(:account).try(:name)
      }
    end
  end

  private

  def get_raw_records
    # insert query here
    Adjustment.includes(:item).includes(:account)
  end

  # ==== These methods represent the basic operations to perform on records
  # and feel free to override them

  # def filter_records(records)
  # end

  # def sort_records(records)
  # end

  # def paginate_records(records)
  # end

  # ==== Insert 'presenter'-like methods below if necessary
end
