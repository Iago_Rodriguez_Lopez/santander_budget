# This file is used by Rack-based servers to start the application.
require ::File.expand_path('../config/environment',  __FILE__)


puts "Production Mode : " + (Rails.env.production?).to_s

if Rails.env.production?

	Rails.application.config.relative_url_root = '/santander_budget'

	map Rails.application.config.relative_url_root || "/santander_budget" do
		run Rails.application
	end

else

	Rails.application.config.relative_url_root = '/'

	map Rails.application.config.relative_url_root || "/" do
		run Rails.application
	end

	require ::File.expand_path('../config/environment',  __FILE__)
	run Rails.application
end